-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16-10-23 16:59
-- 서버 버전: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;



CREATE TABLE IF NOT EXISTS `admin` (
`adminID` int(10) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `joindate` datetime NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;


INSERT INTO `admin` (`adminID`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `joindate`, `type`) VALUES
(2, 'adminkt', '6b3a55e0261b0304143f805a24924d0c', '15cec5205c9e34355287acbb4d096a08', 'Kristi ', 'Turman', 'kristi@gmail.com', '2014-01-01 09:00:00', 0),
(3, 'adminst', '10e39d44af853554767db9b1e7396bea8c97681eb56d85d8818addbb7e3b7053', 'abd1018f0e08056ff208df5de039809e', 'Scott', ' Turman', 'turman@telstra.com.au', '2014-01-20 14:30:00', 0),
(4, 'adminrw', '8a35fd6de1e692dfa8277c405f93bba34926176285c85a13634e54e051b576f3', '09d1fe391935b75f798053f866ee5052', 'Richard', ' Weathers', 'richo@gmail.com', '2014-01-20 16:00:00', 0),
(5, 'adminnc', 'd54dc8e24b12ba4805777d6b6eac977094b73962a15e04f227ec40eb6ec56432', '4d069acd30e4b0c6eb5e5f36c01d1482', 'Nicholas', ' Cutter', 'nicholas.cutter@gmail.com', '2014-01-22 13:00:00', 0),
(6, 'admintest', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', '', 'John', 'ad', 'ff', '2016-09-06 00:00:00', 0),
(7, 'committee', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', '', 'dfd', '', 'ji5656@gmail.com', '0000-00-00 00:00:00', 3),
(8, 'com2', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', '', 'cris', 'lee', 'abo@adidd.com', '0000-00-00 00:00:00', 3),
(9, 'president', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', '', 'JIYOUNG', 'CHOI', '', '0000-00-00 00:00:00', 4);


CREATE TABLE IF NOT EXISTS `category` (
`categoryID` int(11) NOT NULL,
  `category` varchar(40) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;


INSERT INTO `category` (`categoryID`, `category`, `description`) VALUES
(1, 'charity', 'charity'),
(2, 'activity', 'activity');


CREATE TABLE IF NOT EXISTS `donation` (
`donationID` int(11) NOT NULL,
  `donationTitle` varchar(40) NOT NULL,
  `donationDes` longtext NOT NULL,
  `eventID` int(11) NOT NULL,
  `goalMoney` int(11) NOT NULL,
  `img` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;


INSERT INTO `donation` (`donationID`, `donationTitle`, `donationDes`, `eventID`, `goalMoney`, `img`) VALUES
(1, 'Twelve-year-old Mitchell', 'So much so that they have often worried if their 42-year-old father, Matt - who was diagnosed with his progressive form of multiple sclerosis in July 2016, is going to die.\n\nWhy do they do this? Because, like many young children growing up in families living with multiple sclerosis, Mitchell and Hannah face a myriad of fears and misconceptions about their Dad''s disease on a daily basis.', 3, 48515, 'donation1'),
(2, 'Kathleen''s Dream for Kaelan', 'For Kaelan, age 6, who lost his mummy too soon. Kathleen died unexpectedly at 38 and had no life insurance. Her dream was for him to graduate at the school he attends. Home looks different now, let''s keep his school, his routine and his friends the same to support him through his life.', 5, 24000, 'dontaion2.jpg');


CREATE TABLE IF NOT EXISTS `eventexpense` (
`eventplanID` int(40) NOT NULL,
  `type` int(11) NOT NULL,
  `eventID` int(11) NOT NULL,
  `budget` int(11) NOT NULL,
  `venuecost` int(40) NOT NULL,
  `FoodCatering` int(11) NOT NULL,
  `AudioVisual` int(11) NOT NULL,
  `ThirdPartyVendors` int(11) NOT NULL,
  `EventRentals` int(11) NOT NULL,
  `marketingRegistration` int(11) NOT NULL,
  `planningOrganization` int(11) NOT NULL,
  `administrativeExpenses` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;


INSERT INTO `eventexpense` (`eventplanID`, `type`, `eventID`, `budget`, `venuecost`, `FoodCatering`, `AudioVisual`, `ThirdPartyVendors`, `EventRentals`, `marketingRegistration`, `planningOrganization`, `administrativeExpenses`) VALUES
(1, 0, 3, 5000, 500, 500, 150, 50, 250, 200, 150, 300),
(2, 0, 4, 4500, 450, 400, 230, 100, 250, 200, 150, 240),
(3, 0, 5, 4300, 450, 500, 230, 200, 250, 200, 150, 240),
(4, 0, 6, 2500, 600, 400, 400, 500, 200, 100, 300, 100),
(5, 1, 6, 2500, 600, 300, 200, 300, 500, 500, 200, 140),
(6, 1, 3, 5000, 700, 400, 80, 200, 50, 100, 100, 200),
(7, 1, 5, 4300, 30, 600, 600, 150, 200, 180, 100, 125),
(8, 1, 4, 4500, 300, 250, 500, 200, 100, 250, 200, 200);


CREATE TABLE IF NOT EXISTS `eventincome` (
  `eventincomeID` int(11) NOT NULL,
  `RaffleTickets` int(11) NOT NULL,
  `EntryFees` int(11) NOT NULL,
  `CashDonation` int(11) NOT NULL,
  `GoodsServiceSold` int(11) NOT NULL,
  `eventID` int(11) NOT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `eventincome` (`eventincomeID`, `RaffleTickets`, `EntryFees`, `CashDonation`, `GoodsServiceSold`, `eventID`, `type`) VALUES
(0, 1500, 2000, 2000, 700, 3, 0),
(1, 2000, 1500, 1500, 700, 4, 0),
(3, 2000, 1500, 3000, 600, 5, 0),
(4, 1500, 2000, 650, 600, 6, 0),
(5, 2000, 2500, 400, 600, 6, 1),
(6, 2500, 200, 800, 600, 4, 1),
(7, 600, 2500, 3000, 2000, 3, 1),
(9, 500, 1200, 2000, 100, 5, 1);


CREATE TABLE IF NOT EXISTS `events` (
`eventID` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `donationID` int(11) NOT NULL,
  `budget` int(40) NOT NULL,
  `categoryID` int(10) NOT NULL,
  `adminID` int(10) NOT NULL,
  `eventdate` datetime NOT NULL,
  `venue` varchar(40) NOT NULL,
  `eventimg` varchar(200) DEFAULT NULL,
  `eventGoal` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;


INSERT INTO `events` (`eventID`, `title`, `content`, `donationID`, `budget`, `categoryID`, `adminID`, `eventdate`, `venue`, `eventimg`, `eventGoal`) VALUES
(3, 'Healing Through Art', '                                          Indulge in the essence of art and help the ones in need. Being held at the O Block in QUT gardens point, visit the art exhibition. And all the earnings will be donated to the local charity.\r\n', 2, 3000, 1, 7, '2016-11-30 10:00:00', 'O BLock Podium O205', 'test1.jpg', 5000),
(4, 'Water for Life', 'Water For Life''s goal is to help the people in Africa, who are in need of water. Certain regions in Africa suffer from scarcity of water in their area. Water for life''s mission is to help communities in need develop safe and (able to last/helping the planet) water sources. Come join us at the Botanic gardens and help us reach out to the communities that are in need of water. ', 2, 2000, 2, 8, '2016-11-24 13:00:00', 'Botanic gardens', 'test4.jpg', 4000),
(5, 'STOP Bullying !!', 'Bullying can affect you in many ways. You may lose sleep or feel sick. You may want to skip school. You may even be thinking about suicide. This event is being held to appreciate the communities that volunteer to help the ones being bullied. Help us in appreciating their fantastic work and raise some funds foe their proper functioning and betterment.\n\n', 1, 3000, 1, 7, '2016-11-26 09:00:00', 'Anzac Square', 'test2.jpg', 4000),
(6, 'Education for Gypsies', 'The mental age of an average adult Gypsy is thought to be about that of a child of 10," said the 1959 edition of the Encyclopaedia Britannica, 14 years after the end of the Nazi genocide of Romany Gypsies. This week new analysis of the 2011 census has been released by the Office for National Statistics. It revealed that of the 58,000 people who identified themselves as being of Gypsy/Traveller ethnicity, 60% had no formal qualifications whatsoever. This is almost three times higher than the figure for England and Wales as a whole, which is 23%.', 1, 4000, 1, 8, '2016-11-22 14:00:00', 'The Brisbane City Hall', 'test3.jpg', 4000);


CREATE TABLE IF NOT EXISTS `fundedmoney` (
`fundedID` int(11) NOT NULL,
  `donationID` int(11) NOT NULL,
  `memberID` int(11) NOT NULL,
  `donationMoney` int(11) NOT NULL,
  `donationdate` date NOT NULL,
  `eventID` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;


INSERT INTO `fundedmoney` (`fundedID`, `donationID`, `memberID`, `donationMoney`, `donationdate`, `eventID`) VALUES
(6, 1, 1, 1000, '2016-09-04', 0),
(7, 1, 2, 5000, '2016-09-04', 0),
(8, 1, 4, 200, '2016-09-02', 0),
(9, 2, 7, 500, '2016-09-04', 5),
(10, 1, 7, 100, '2016-08-15', 6),
(11, 0, 7, 60, '2016-10-22', 3),
(12, 0, 7, 60, '2016-10-22', 4);


CREATE TABLE IF NOT EXISTS `invoice` (
`invoiceID` int(11) NOT NULL,
  `memberID` int(11) NOT NULL,
  `dateTime` datetime NOT NULL,
  `invoicecol` varchar(45) DEFAULT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;


INSERT INTO `invoice` (`invoiceID`, `memberID`, `dateTime`, `invoicecol`, `total`) VALUES
(23, 7, '2016-10-05 05:26:36', NULL, 0),
(24, 7, '2016-10-05 05:27:56', NULL, 0),
(25, 7, '2016-10-05 05:39:09', NULL, 0),
(26, 0, '2016-10-13 23:30:49', NULL, 0),
(27, 0, '2016-10-15 05:39:19', NULL, 0),
(28, 0, '2016-10-15 05:50:05', NULL, 0),
(29, 0, '2016-10-18 13:35:25', NULL, 0),
(30, 7, '2016-10-19 01:01:38', NULL, 0),
(31, 7, '2016-10-23 08:48:51', NULL, 0),
(32, 7, '2016-10-23 08:58:12', NULL, 0),
(33, 7, '2016-10-23 09:17:38', NULL, 0),
(34, 7, '2016-10-23 09:20:00', NULL, 0),
(35, 7, '2016-10-23 09:20:50', NULL, 0),
(36, 7, '2016-10-23 09:22:02', NULL, 0),
(37, 7, '2016-10-23 09:22:58', NULL, 0),
(38, 7, '2016-10-23 09:23:10', NULL, 0),
(39, 7, '2016-10-23 09:23:57', NULL, 0),
(40, 7, '2016-10-23 09:25:17', NULL, 0),
(41, 7, '2016-10-23 09:25:31', NULL, 0),
(42, 7, '2016-10-23 09:25:45', NULL, 0),
(43, 7, '2016-10-23 09:26:55', NULL, 0),
(44, 7, '2016-10-23 09:29:12', NULL, 201),
(45, 7, '2016-10-23 09:31:50', NULL, 0),
(46, 7, '2016-10-23 10:22:45', NULL, 10);


CREATE TABLE IF NOT EXISTS `member` (
`memberID` int(10) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `streetnum` int(11) DEFAULT NULL,
  `streetname` varchar(45) DEFAULT NULL,
  `suburb` varchar(20) DEFAULT NULL,
  `state` enum('ACT','QLD','WA','VIC','TAS','NSW') DEFAULT NULL,
  `postcode` varchar(45) DEFAULT NULL,
  `country` varchar(45) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `gender` enum('Female','Male') NOT NULL,
  `date` datetime NOT NULL,
  `newsletter` enum('YES','NO') DEFAULT NO,
  `image` varchar(200) DEFAULT NULL,
  `contribution` int(11) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `memberdelete` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- 테이블의 덤프 데이터 `member`
--

INSERT INTO `member` (`memberID`, `username`, `password`, `salt`, `firstname`, `lastname`, `streetnum`, `streetname`, `suburb`, `state`, `postcode`, `country`, `phone`, `mobile`, `email`, `gender`, `date`, `newsletter`, `image`, `contribution`, `type`, `memberdelete`) VALUES
(1, 'natalie', '9d26a3c61c0e2ce5c980ad8c7fff13c08cf898b6f3786930c73b68e3b88ad779', '002640cb549ba08ee90cd79aee89cce7', 'ji', 'choi', 0, 'QLD', 'Brisbane', 'ACT', '4000', 'Australia', '413019203', '12334', 'ji5656@hotmail.com', 'Female', '2014-01-16 14:00:00', 'NO', 'nataliegoddard.png', NULL, 1, 0),
(4, 'member1', '4b374124e81c0f96572313a7e57d95d68c7ea2ac38c65c8b962a11db66608e51', '3c75ae7840aaf4da8cdf9e6443905034', 'ji', 'choi', 0, 'QLD', 'Brisbane', 'ACT', '4000', 'Australia', '413019203', '413019203', '', 'Female', '2016-09-05 19:33:22', 'NO', '2693_girl-with-hat-in-sunglass_large.jpg', NULL, 1, 0),
(5, 'test1', 'fc551e3412e3f8122b570828692720f7f77839184eed225b6baa21cc61957608', '181a9751748c7572bde26800f12929b8', 'ji', 'choi', 0, 'QLD', 'Brisbane', 'ACT', '4000', 'Australia', '413019203', '4130192032', 'tess.nash@connect.qut.edu.au', 'Female', '2016-09-12 14:23:49', 'NO', '', NULL, 1, 0),
(6, 'test2', '2670b9911660337a14701072722d536c80b455dd28b6a6f47220535f7f2acf8a', '9505fa9bab0114388784ccd08dafd722', 'ji', 'choi', 0, 'QLD', 'Brisbane', 'QLD', '4000', 'Australia', '413019203', '413019325d', 'cjy5108@gmail.com', 'Female', '2016-09-17 16:20:25', 'NO', '3710_2693_girl-with-hat-in-sunglass_large.jpg', NULL, 1, 0),
(7, 'test3', '11aeacaee6f58022850987c0caa34616f852bf18d5696b84d61932fa9851bc79', '2908f7071c253f22678de695ac9dda63', 'jihye', 'choi', 28, 'abc street', 'Rochedale', 'QLD', '1234', 'au', '', '1234567', 'ji5656@hotmail.com', 'Female', '2016-09-20 20:05:29', 'NO', 'user.jpg', NULL, 1, 0),
(8, 'playtest', 'ee9e2ea0db97497479a20d1d657e83efcb1839fcee7f9f287d5385a2de4afdc6', 'bf2188607424227e9f058dbf1a8b8c2f', 'James', 'Woods', 123, 'Fake street', 'Springfield', 'QLD', '1234', 'America', '', '', 'compugolbalhypermeganet@homer.com', 'Male', '2016-10-13 23:26:47', 'NO', '9232_mcgdbzb.jpg', NULL, 1, 0);


CREATE TABLE IF NOT EXISTS `memberattendee` (
  `eventID` int(11) NOT NULL,
  `memberID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `memberattendee` (`eventID`, `memberID`) VALUES
(0, 7),
(3, 1),
(3, 4),
(3, 5),
(3, 7),
(4, 5),
(4, 7),
(5, 7),
(6, 1),
(6, 6),
(6, 7);


CREATE TABLE IF NOT EXISTS `product` (
`productID` int(11) NOT NULL,
  `productPrice` decimal(10,2) NOT NULL,
  `productDescription` text NOT NULL,
  `productName` varchar(40) NOT NULL,
  `productImage` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;


INSERT INTO `product` (`productID`, `productPrice`, `productDescription`, `productName`, `productImage`) VALUES
(1, '10.00', 'Clock </br>\nBrand New: A spic-and-span, unused, unopened, unmutilated item in its original packaging (where packaging is applicable). Packaging ought to be an equivalent as what''s obtainable in an exceedingly mercantile establishment, unless the item was packaged by the manufacturer in non-retail packaging, like Associate in Nursing unprinted box or bag', 'Clock', 'clock.jpg'),
(2, '200.50', 'Brand New: A spick-and-span, unused, unopened, unimpaired item in its original packaging (where packaging is applicable). Packaging ought to be identical as what''s accessible in an exceedingly place of business, unless the item was prepackaged by the manufacturer in non-retail packaging, like AN unprinted box or bag.', 'Camera', 'camera.jpg'),
(3, '33.90', 'New with tags: A bran-new, unused, and uneroded item within the original packaging (such because the original box or bag) and/or with the initial tags hooked up', 'Sunglasses', 'sunglass.jpeg'),
(4, '18.60', '	\nNew with tags: A brand-new, unused, and unworn item in the original packaging (such as the original box or bag) and/or with the original tags attached', 'Sweater', 'knit.jpeg'),
(5, '50.50', 'New with tags: A bran-new, unused, and uneroded item within the original packaging (such because the original box or bag) and/or with the initial tags hooked up', 'Glasses', 'glass.jpeg'),
(6, '100.50', '	\nNew with tags: A brand-new, unused, and unworn item in the original packaging (such as the original box or bag) and/or with the original tags attached', 'Pocket Watch', 'clock2.jpg'),
(7, '10.00', '	\nNew with tags: A brand-new, unused, and unworn item in the original packaging (such as the original box or bag) and/or with the original tags attached', 'Digital Watch', 'clock.jpg'),
(8, '200.50', 'Brand New: A spick-and-span, unused, unopened, unimpaired item in its original packaging (where packaging is applicable). Packaging ought to be identical as what''s accessible in an exceedingly place of business, unless the item was prepackaged by the manufacturer in non-retail packaging, like AN unprinted box or bag.', 'Panoramic Camera ', 'camera.jpg');


CREATE TABLE IF NOT EXISTS `product_invoice` (
  `invoiceID` int(11) NOT NULL,
  `productID` int(11) NOT NULL,
  `quantitiy` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `product_invoice` (`invoiceID`, `productID`, `quantitiy`) VALUES
(23, 1, 1),
(25, 1, 1),
(25, 2, 1),
(25, 4, 1),
(26, 1, 999),
(26, 2, 999),
(26, 3, 999),
(26, 4, 999),
(26, 5, 999),
(26, 6, 999),
(26, 7, 999),
(26, 8, 999),
(27, 6, 1),
(28, 2, 1),
(29, 5, 2),
(30, 1, 1),
(31, 1, 1),
(31, 2, 1),
(32, 1, 1),
(33, 2, 1),
(36, 1, 1),
(39, 3, 1),
(43, 2, 1),
(44, 2, 1),
(46, 1, 1);


CREATE TABLE IF NOT EXISTS `type` (
  `typeID` tinyint(1) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `type` (`typeID`, `type`) VALUES
(0, 'admin'),
(1, 'member'),
(2, 'volunteer'),
(3, 'committee'),
(4, 'president');


CREATE TABLE IF NOT EXISTS `volunteer` (
`volunteerID` int(10) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `gender` enum('Female','Male') NOT NULL,
  `date` datetime NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '2',
  `voldelete` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 테이블의 덤프 데이터 `volunteer`
--

INSERT INTO `volunteer` (`volunteerID`, `username`, `password`, `salt`, `firstname`, `lastname`, `phone`, `mobile`, `email`, `gender`, `date`, `type`, `voldelete`) VALUES
(1, 'vol1', '9a12632d6c05e66b2cf195c8c9c769352d78c5ee74fc9ad98dfb11ba50fbe6bc', '1fda3f81a70263e4cde999e72edf179f', 'JI YOUNG', 'Choii', '413019203', '413019203', 'ji5656@hotmail.com', 'Female', '2016-09-15 22:16:21', 2, 1),
(2, 'JESUSH.CHR', 'ffe5f045c01f7dcb5922ca3776ba446f3820a524d012fbe11297209c290eaa4d', '43239fba96808bd2039029b1dd1a864f', 'JI YOUNG', 'Choi', '413019203', '413019203', 'ji5656@hotmail.com', 'Female', '2016-10-13 23:18:37', 2, 0),
(3, 'JESUSH.CHR', 'ca48ba1e9858ed16111c35ae94d55e2f86208931a69874f5c2d6f2fa065bf476', '249918aa7a24083ef9f81432af57871c', 'JI YOUNG', 'Choi', '413019203', '413019203', 'ji5656@hotmail.com', 'Female', '2016-10-13 23:20:12', 2, 0),
(4, 'testuser2', '7f98534db7c3cb4c1763c041c383043264204f8796a15673307e328ce1d27ca5', 'aff011ab5428024e9d3eb709d01dcb35', 'JI YOUNG', 'Choi', '413019203', '413019203', 'ji5656@hotmail.com', 'Female', '2016-10-17 10:54:46', 2, 0),
(5, 'volun1', '67ee9643f6c90fec87c2e60512ebd9abe4c34bf7bdcfd5f41d96bf48fd112dc1', '315df00451843cd51368c9a1acf5c24a', 'JI YOUNG', 'Choi', '413019203', '413019203', 'ji5656@hotmail.com', 'Female', '2016-10-17 11:44:11', 2, 0);


CREATE TABLE IF NOT EXISTS `vol_attendee` (
  `eventID` int(11) NOT NULL,
  `volID` int(11) NOT NULL,
  `position` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `vol_attendee` (`eventID`, `volID`, `position`) VALUES
(3, 0, 'dd'),
(3, 1, 'IT support'),
(3, 2, 'test'),
(4, 1, 'kitched han'),
(4, 3, 'it'),
(4, 4, 'IT');

CREATE TABLE IF NOT EXISTS `comments` (
  `commentID` int(12) NOT NULL AUTO_INCREMENT,
  `parentCommentID` int(12) NULL DEFAULT NULL,
  `comment` text,
  `memberID` int(10) NOT NULL,
  `eventID` int(11) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`commentID`),
  KEY `memberID` (`memberID`),
  KEY `eventID` (`eventID`),
  FOREIGN KEY (eventID) REFERENCES events(eventID),
  FOREIGN KEY (memberID) REFERENCES member(memberID)
  
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27;

-- Chat table

CREATE TABLE IF NOT EXISTS `chat`(
  `chatID` int(12) NOT NULL,
  `messageID` int(12) NOT NULL DEFAULT 0,
  `chatter` int(10) NOT NULL,
  `chattee` int(10) NOT NULL,
  `message` text,
  `sentOn` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `block` int(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`chatID`, `messageID`),
  KEY `messageID` (`messageID`),
  KEY `chatter` (`chatter`),
  KEY `chattee` (`chattee`),
  FOREIGN KEY (chatter) REFERENCES member(memberID),
  FOREIGN KEY (chattee) REFERENCES member(memberID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT = 27;

INSERT INTO comments (`commentID`,`parentCommentID`,`comment`,`memberID`,`eventID`) VALUES
("1",NULL,"this is a cool event! :)","5","4"),
("2","1","I agree", "1","4"),
("3",NULL,"harambe is a dead meme","6","4"),
("4","3","harambe lives on","4","4"),
("5","3","lol","1","4"),
("6",NULL,"Not harambe please","5","4"),
("7","6","Yes harambe please","6","4");


ALTER TABLE `admin`
 ADD PRIMARY KEY (`adminID`), ADD UNIQUE KEY `adminID_UNIQUE` (`adminID`), ADD KEY `fk_admin_type1_idx` (`type`);

ALTER TABLE `category`
 ADD PRIMARY KEY (`categoryID`), ADD UNIQUE KEY `categoryID_UNIQUE` (`categoryID`);

ALTER TABLE `donation`
 ADD PRIMARY KEY (`donationID`);

ALTER TABLE `eventexpense`
 ADD PRIMARY KEY (`eventplanID`);

ALTER TABLE `eventincome`
 ADD PRIMARY KEY (`eventincomeID`);

ALTER TABLE `events`
 ADD PRIMARY KEY (`eventID`), ADD UNIQUE KEY `reviewID_UNIQUE` (`eventID`,`categoryID`), ADD KEY `fk_review_admin1_idx` (`adminID`), ADD KEY `fk_review_category1_idx` (`categoryID`);

ALTER TABLE `fundedmoney`
 ADD PRIMARY KEY (`fundedID`);

ALTER TABLE `invoice`
 ADD PRIMARY KEY (`invoiceID`), ADD KEY `fk_invoice_user_idx` (`memberID`);

ALTER TABLE `member`
 ADD PRIMARY KEY (`memberID`), ADD KEY `fk_member_type1_idx` (`type`);

ALTER TABLE `memberattendee`
 ADD PRIMARY KEY (`eventID`,`memberID`);

ALTER TABLE `product`
 ADD PRIMARY KEY (`productID`);

ALTER TABLE `product_invoice`
 ADD PRIMARY KEY (`invoiceID`,`productID`), ADD KEY `fk_product_invoice_product1_idx` (`productID`);

ALTER TABLE `type`
 ADD PRIMARY KEY (`typeID`);

ALTER TABLE `volunteer`
 ADD PRIMARY KEY (`volunteerID`), ADD KEY `fk_member_type1_idx` (`type`);

ALTER TABLE `vol_attendee`
 ADD PRIMARY KEY (`eventID`,`volID`), ADD KEY `fk_product_invoice_product1_idx` (`volID`);


ALTER TABLE `admin`
MODIFY `adminID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;

ALTER TABLE `category`
MODIFY `categoryID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;

ALTER TABLE `donation`
MODIFY `donationID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;

ALTER TABLE `eventexpense`
MODIFY `eventplanID` int(40) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;

ALTER TABLE `events`
MODIFY `eventID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;

ALTER TABLE `fundedmoney`
MODIFY `fundedID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;

ALTER TABLE `invoice`
MODIFY `invoiceID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;

ALTER TABLE `member`
MODIFY `memberID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;

ALTER TABLE `product`
MODIFY `productID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;

ALTER TABLE `volunteer`
MODIFY `volunteerID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;

ALTER TABLE `member`
ADD CONSTRAINT `member_ibfk_1` FOREIGN KEY (`type`) REFERENCES `type` (`typeID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `volunteer`
ADD CONSTRAINT `volunteer_ibfk_1` FOREIGN KEY (`type`) REFERENCES `type` (`typeID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
