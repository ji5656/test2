<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: cart.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: event post .


---------------------------------------------------------

Updates: 

comment section

Kris Kingston

--------------------------------------------------------->
<?php
$page = "eventpost";
include '../includes/connect.php';
include '../includes/header.php';
include '../includes/nav.php';

?>
<?php

$memberID = $_SESSION ['user'];
?>
<div class="container">
	<div class="row box box-pink">
		<div class="col-md-10 ">
			<hr>
			<h1>
				OUR <strong>EVENTS </strong>

			</h1>
			<hr>
	
	
	

 <?php
	$eventID = $_GET ['eventID'];
	$sql = "SELECT * FROM events WHERE eventID =$eventID";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
	$row = mysqli_fetch_array ( $result );
	$pageTitle = $row ['title'];
	$content = $row ['content'];
	$venue = $row ['venue'];
	$eventdate = $row ['eventdate'];
	$eventimg = $row ['eventimg'];
	
	?>
</div>

		<div class="col-md-8">
			<div class='img-responsive'>
			     <?php
								
echo "<img src='../img/" . $eventimg . "'" . "style='width:300px; height:400px;' background-size='cover'>";
								?>
			</div>
			<div class="col-md-pull-1">
				<span class="eventdatebtn"><?php
				$source = $eventdate;
				$date = new DateTime ( $source );
				echo $date->format ( 'd M' );
				echo "</button>";
				
				?>
			
			
			</div>
			<div class='col-md-push-3'>
				<h1><?php echo $pageTitle?></h1>
				<h4><?php echo $eventdate?></h4>

			</div>
			
<?php

if (isset ( $_SESSION ['member'] )) {
	$eventID = $_GET ['eventID'];
	$sql = "SELECT memberattendee.* FROM memberattendee  WHERE memberID = $memberID AND eventID =$eventID";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	$row_count = mysqli_num_rows ( $result );
	if ($row_count >= 1) {
		echo "<div class='bs-callout bs-callout-success'>
  <h4>You already applyed this event</h4>
 
</div>";
	} else {
		echo '<a href="memberapply.php?eventID=' . $eventID . '" type="button"
 					class="btn btn-default btn-lg pull-right postbtn"> <span
 					class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span><strong>Paticipate Event</strong></a>';
	}
} else {
	echo '<a href="volapply.php?eventID=' . $eventID . '" type="button"
 					class="btn btn-default btn-lg pull-right postbtn"> <span
 					class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span><strong>JOIN
 					US</strong></a>';
}
?>
 		
 		<a href="donation.php?eventID=<?php echo $eventID ?>" type="submit"
				class="btn btn-default btn-lg pull-right postbtn"> <span
				class="glyphicon glyphicon-triangle-right" aria-hidden="true"></span><strong>Donation</strong></a>
			<br>
			<div class='col-md-pull-8 card'>
				<h3><?php echo $content  ?></h3>
				<br />
				<h3><?php echo $venue  ?></h3>



			</div>
		</div>

<?php
$sql = "SELECT * FROM events ORDER BY eventID DESC LIMIT 3";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
$row = mysqli_fetch_array ( $result );
echo '<div class="col-md-4 text-center"><div class="makedonation" style="border: 5px solid #003169;"><h3>MAKE A DONATION</h3></div>';
while ( $row = mysqli_fetch_array ( $result ) ) {
	$eventID = $row ['eventID'];
	echo '<div class="row card"><div class="row " style="background-color: #022C37; margin: 10px 0;">
		<div class="eventcardtxt1 pull-left text-center">';
	echo '<h5>' . $row ['title'] . '</h5>';
	echo '<hr>
		<h5>' . $row ['eventdate'] . '</h5>';
	echo '<p>' . $row ['venue'] . '</p>
		<a href="eventpost.php?eventID=' . $eventID . '" type="button"
				class="btn btn-primary-outline event"><i>More details</i></a>
				</div>
				<div class="pull-right">';
	echo "<img src='../img/" . ($row ['eventimg']) . "'" . " style='width: 150px; height: 200px;'
						background-size='cover'>
						</div></div></div>
						";
}

?>
	
</div>




	<div class="row col-md-4 ">
		<span class="eventdatebtn1">venue</span>
		<h3><?php echo $venue?></h3>
		<iframe id="map" width="100%" height="90%" frameborder="0"
			scrolling="no" marginwidth="0" marginheight="0"
			src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&ie=UTF8&iwloc=A&output=embed&z=6&q=brisbane"></iframe>
	</div>
</div>

<h3> Comments </h3>

<?php 
    
     $commentsql = "SELECT c.commentID, c.comment, c.parentCommentID, c.memberID, m.username, m.image
     FROM (comments c) JOIN member m ON c.memberID = m.memberID WHERE c.parentCommentID IS NULL AND c.eventID = '"
     . $_GET['eventID'] . "'ORDER BY c.commentID ASC";
     $commentresult = mysqli_query($con, $commentsql) or die(mysqli_error($con));
    
     while ($commentoutput = mysqli_fetch_assoc($commentresult)){
         echo '<h3>'. $commentoutput['username'].'</h3><hr>';
         echo '<p>' . $commentoutput['comment'].'</p>';
         $commentcycle = $commentoutput['commentID'];
         echo '<input type = "submit" name="replyToComment" value ="reply" class ="replybuttons"> &#8226 <hr>';
         
          $replysql = "SELECT c.commentID, c.comment, c.memberID, c.eventID, m.username as comUsername, m.image, r.commentID as replyCommentID, r.comment as reply, r.memberID as replyUserID, 
          m2.username as replyUsername, m2.image as replyImage
          FROM (comments c)
          JOIN member m ON c.memberID = m.memberID
          LEFT JOIN comments r ON c.commentID = r.parentCommentID
          JOIN member m2 ON r.memberID = m2.memberID
          WHERE c.eventID = '" . $_GET['eventID'] . "' AND c.commentID ='$commentcycle' ORDER BY replyCommentID ASC" ;
          $replyresult = mysqli_query($con, $replysql) or die(mysqli_error($con));
         
         while ($replyoutput = mysqli_fetch_array($replyresult))
             {
             echo '<div class = "replies"><h3>'. $replyoutput['replyUsername'].'</h3><hr>';
             echo '<p> +' .$replyoutput['comUsername'] . ' '. $replyoutput['reply'].'</p>';
             echo'<form action ="eventpostreply.php method = "post">';
             echo '<textarea cols="50" rows="20" name="text" id="text_id" class="form-control" style="resize:vertical;display:none"></textarea>';
             echo '<input type ="hidden" name="eventIDreply" value="<?php echo '.$_GET['eventID'].'">';
             echo '<input type="hidden" name="memberIDreply" value="<?php echo '.$_SESSION['user'].' ?>">';
             echo '<input type="hidden" name="parentCommentIDreply">';
             echo '<p><input type = "submit" name="replyToComment" value ="reply" class="replybuttons" style:"display:none;">&#8226<hr></div>';
             echo '</form>';
             }
     }
     
?> 
    
            
    <div class = "col-md-pull-1">
    <h3>Add your comment! </h3>
    <img src='' class="profilecommentpic" >
    <form action="eventpostcomment.php" method="post">   
        <textarea type="text" name="comments" id="comments" placeholder="Make a comment" class = "commentboxevent" required></textarea>
        <input type="submit" name="post" value="Post">
        <?php
        /*
        <button onclick="myFunction()" name="post">Post</button>
        echo '<a href="eventpost.php?eventID='. $_GET['eventID'] .'" name ="postbtn" type="submit" class="btn btn-primary-outline event postbutton" >Post</a> ' */ 
        ?>
        <input type ="hidden" name="eventID" value="<?php echo $_GET['eventID']; ?>">
        <input type="hidden" name="memberID" value="<?php echo $_SESSION['user']; ?>">
        <input name ="cancelbutton" type="button" value="Cancel" id ="cancelbutton" class="btn btn-primary-outline event cancelbutton">
    </form>
    </div> 



<?php
include "../includes/footer.php";
?>
