<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: volregiprocessing.php
File Version: 1.01 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: process for registration for volunteer
--------------------------------------------------------->
<?php

$page = "Registration";
include '../includes/connect.php';
include '../includes/header.php'; // session_start(); included in header.php
include '../includes/nav.php';
?>
<div class="login">
	<div id="regform">

		<hr>
		<h2>Become a volunteer</h2>
		<hr>
<?php

$username = mysqli_real_escape_string ( $con, $_POST ['username'] ); // prevent SQL injection
$password = mysqli_real_escape_string ( $con, $_POST ['password'] );
$firstName = mysqli_real_escape_string ( $con, $_POST ['firstname'] );
$lastName = mysqli_real_escape_string ( $con, $_POST ['lastname'] );
$phone = mysqli_real_escape_string ( $con, $_POST ['phone'] );
$mobile = mysqli_real_escape_string ( $con, $_POST ['mobile'] );
$email = mysqli_real_escape_string ( $con, $_POST ['email'] );
$gender = mysqli_real_escape_string ( $con, $_POST ['gender'] );
if (strlen ( $password ) < 8) // check if the password is a minimum of 8 characters long
{
	$_SESSION ['error'] = 'Password must be 8 characters or more.'; // if password is less than 8 characters intialise a session called 'error' with a msg
	header ( "location:registration.php" ); // redirect to registration.php
	exit ();
}

$salt = md5 ( uniqid ( rand (), true ) ); // create a random salt value
$password = hash ( 'sha256', $password . $salt ); // generate the hashed password with the salt value

$sql = "(SELECT username FROM volunteer WHERE volunteer.username='$username') UNION
(SELECT username FROM member WHERE member.username='$username')"; // check if the username is taken in the volunteer table or the admin table as the username must be unique
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
$numrow = mysqli_num_rows ( $result ); // count how many rows are returned

if ($numrow > 0) // if count greater than 0
{
	// if an username is taken intialise a session called 'error' with a msg
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('error')
 		 window.location.href='volregi.php'
 		</SCRIPT>");
} elseif ($username == "" || $password == "" || $firstName == "" || $lastName == "" || $email == "" || $gender == "") // check if all required fields have data

{

	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('All * fields are required.')
 		 window.location.href='volregi.php'
 		</SCRIPT>");
} elseif (! filter_var ( $email, FILTER_VALIDATE_EMAIL )) // check if email is valid
{

	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Please enter a valid email address.')
 		 window.location.href='volregi.php'
 		</SCRIPT>");
} else {
	$sql = "INSERT INTO volunteer (username, password, salt, firstname, lastname, phone, mobile, email, gender, date,
 type) VALUES ('$username', '$password', '$salt', '$firstName',
'$lastName', '$phone','$mobile', '$email', '$gender', NOW(), '2')";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('successfully new sing up')
 		 window.location.href='volregi.php'
 		</SCRIPT>");
 }
?>