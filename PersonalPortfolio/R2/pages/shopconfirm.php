<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: shopconfirm.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page:cart confirm page 
--------------------------------------------------------->
<?php
$page = "confirm";
include '../includes/connect.php';
include '../includes/header.php';
include '../includes/functions.php';
include '../includes/nav.php';
include '../includes/loginmembercheck.php';
?>

<body>

<?php
if (isset ( $_POST ['submit'] )) {
	
	if (isset ( $_REQUEST ['command'] ) && $_REQUEST ['command'] == '') { // if the $_REQUEST variable has a value of 'nothing' do the following
		$total = (number_format ( (get_order_total ()), 2, '.', '' ));
		$memberID = $_SESSION ['user'];
		$sql = "INSERT INTO invoice (memberID, dateTime,total) VALUES('$memberID', NOW(),'$total')";
		// insert data into the invoice table
		$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
		$invoiceID = mysqli_insert_id ( $con ); // retrieve the last generated automatic ID
		
		$max = count ( $_SESSION ['cart'] );
		for($i = 0; $i < $max; $i ++) {
			
			$pid = $_SESSION ['cart'] [$i] ['productID']; // for each product retrieve the productID
			$q = $_SESSION ['cart'] [$i] ['qty']; // for each product retrieve the quantity
			$sql = "INSERT into product_invoice (invoiceID, productID, quantitiy ) VALUES('$invoiceID', '$pid', '$q')"; // insert data into the product_invoice table
			$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
			// run the query
		}
		unset ( $_SESSION ['cart'] ); // unset the 'cart' session when the order is completed
	}
	$sql = "SELECT * FROM invoice WHERE invoiceID =" . $invoiceID; // query to select the invoiceID
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	$row = mysqli_fetch_array ( $result ); // store the result in the variable $row and in the code below display the invoiceID that is stored in the $row variable
	?>
	<div class=container>
		<div class="col-md-6 box" style="margin: 2% 25%;">
			<h2>Your Order Invoice #
	 <?php
	echo $row ['invoiceID']?> 
	 Has Been Processed!</h2>
			<p>Your order has been successfully processed. You will receive your
				product within10 working days.</p>
			<p>
				<em>Thank you for shopping with us online!</em>
			</p>
		</div>
	</div>
	</div>
	</div>


<?php

} else {
	?>

	<div class="container ">

		<div class="row box box-bgreen">
			<div class="col-md-12">
				<h1>Checkout</h1>
				<hr>


				<h2>Order Total</h2>
				<p>Please confirm your order details</p>
<?php
	$memberID = $_SESSION ['user'];
	$sql = "SELECT * FROM member WHERE memberID = '$memberID'";
	// retrieve the details for the logged in user
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	$row = mysqli_fetch_array ( $result ); // save the result in the $row variable
	echo "<h3> Order for: <strong>" . $row ['firstname'] . " " . $row ['lastname'] . "</strong></h3>"; // display the user name
	
	echo "<h4> Address: <strong>" . $row ['streetnum'] . " " . $row ['streetname'] . " " . $row ['suburb'] . " " . $row ['state'] . $row ['postcode'] . "</strong></h4>";
	?>

<table class="table">
					<thead>
						<tr>
							<th>Image</th>
							<th>Product Name</th>
							<th>Price</th>
							<th>Qty</th>
							<th>Subtotal</th>


						</tr>
					</thead>
					<tbody>
						<!-- the code could be improved by removing all the inline styling and placing it in
your external CSS file -->
 <?php
	if (isset ( $_SESSION ['cart'] )) {
		
		$max = count ( $_SESSION ['cart'] );
		for($i = 0; $i < $max; $i ++) { // for each product in the cart get the following
			$pid = $_SESSION ['cart'] [$i] ['productID']; // productID
			$q = $_SESSION ['cart'] [$i] ['qty']; // quantity
			$pname = get_product_Name ( $pid ); // product name
			if ($q == 0)
				continue;
			?>
 <tr>
							<td style="padding: 10px"><?php
			
			echo "<img src='../img/shop/" . (get_product_Image ( $pid )) . "'" . " width=100 height=100 alt='product'" . " />"?></td>
							<td><?php echo $pname ?></td>
							<td>$ <?php
			
			echo (number_format ( (get_price ( $pid )), 2, '.', '' ))?></td>
							<td><?php echo $q ?></td>
							<td>$ <?php
			
			echo (number_format ( (get_price ( $pid ) * $q), 2, '.', '' ))?></td>
 <?php
		}
		?>
				
						
						
						<tr>
							<td colspan="2"><strong>Order Total:  <?php
		$total = (number_format ( (get_order_total ()), 2, '.', '' ));
		echo "$" . $total;
		?></strong></td>
							<td colspan="5" style="text-align: right; padding: 10px;"></td>
						</tr>
 <?php
	} else {
		echo "<tr><td>There are no items in your shopping cart!</td>";
	}
	?>

				
				</table>

				<form class="form-horizontal">
					<fieldset>

						<!-- Form Name -->
						<legend></legend>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="Card">Card Number</label>
							<div class="col-md-5">
								<input id="Card" name="Card" type="text" placeholder=""
									class="form-control input-md" required=""> <span
									class="help-block">Enter 16 digit card number.</span>
							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="fullname">Full Name</label>
							<div class="col-md-2">
								<input id="firstname" name="first" type="text"
									placeholder="First Name" class="form-control input-md"
									required=""> <span class="help-block">Exactly as it appears on
									the card.</span>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-2">
									<input id="lastname" name="last" type="text"
										placeholder="Last Name" class="form-control input-md"
										required=""> <span class="help-block"></span>
								</div>
							</div>

							<!-- Select Basic -->
							<div class="form-group">
								<label class="col-md-4 control-label" for="Exp Date">Expiration
									Date</label>
								<div class="col-md-2">
									<select id="Exp Date" name="Exp Date" class="form-control">
										<option value="Month">Month</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
								</div style="float:left;">

								<!-- Select Basic -->
								<div class="form-group">
									<div class="col-md-2">
										<select id="YY" name="Year" class="form-control">
											<option value="Year">Year</option>
											<option value="2016">2016</option>
											<option value="2017">2017</option>
											<option value="2018">2018</option>
											<option value="2019">2019</option>
											<option value="2020">2020</option>
											<option value="2021">2021</option>
											<option value="2022">2022</option>
											<option value="2023">2023</option>
											<option value="2024">2024</option>
											<option value="2025">2025</option>
											<option value="2026">2026</option>
											<option value="2027">2027</option>
											<option value="2028">2028</option>
										</select>
									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="cvv">Security Code</label>
									<div class="col-md-1">
										<input id="" name="CVV" type="text" placeholder=""
											class="form-control input-md" required="">
									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="">Street Address 1</label>
									<div class="col-md-5">
										<input id="" name="Address 1" type="text" placeholder=""
											class="form-control input-md" required="">
									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="">Street Address 2</label>
									<div class="col-md-5">
										<input id="" name="Address 2" type="text"
											placeholder="Apartment, suite, etc."
											class="form-control input-md" required="">
									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="">City</label>
									<div class="col-md-2">
										<input id="" name="City" type="text" placeholder=""
											class="form-control input-md" required="">
									</div>
								</div>


								<!-- Select Basic -->
								<div class="form-group">
									<label class="col-md-4 control-label" for="country">Country</label>
									<div class="col-md-2">
										<select id="country" name="country" class="form-control">
											<select id="country" name="country"></select>
									
									</div>
								</div>

								<!-- Select Basic -->
								<div class="form-group">
									<label class="col-md-4 control-label" for="state">State</label>
									<div class="col-md-2">
										<select id="state" name="state" class="form-control">
											<select id="Select State" name="state"></select>

											<script language="javascript">
            populateCountries("country", "state");
            populateCountries("country2");
        </script>
									
									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="">Zip or postal code</label>
									<div class="col-md-2">
										<input id="" name="Zip" type="text" placeholder=""
											class="form-control input-md" required="">

									</div>
								</div>

								<!-- Text input-->
								<div class="form-group">
									<label class="col-md-4 control-label" for="">Contact Email</label>
									<div class="col-md-3">
										<input id="" name="Email" type="text" placeholder=""
											class="form-control input-md" required="">


									</div>
								</div>
					
					</fieldset>
				</form>

				<form action="" method="post">

					<input type="hidden" name="memberID"
						value="<?php echo $memberID; ?>"> <input type="hidden"
						name="command" /> <input type="button" value="Return to Cart"
						class="btn btn-success pull-right"
						onclick="window.location='cart.php'"> <input type="hidden"
						name="<?php $total;?>" /> <input type="submit"
						class="btn btn-danger pull-right" name="submit"
						value="Confirm Order" />
				</form> 
                                <?php }?></div></div>
<?php
include '../includes/footer.php';
?>