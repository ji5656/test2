<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: donation.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page:donation event page
--------------------------------------------------------->

<?php
$page = "eventpost";
include '../includes/connect.php';
include '../includes/header.php';
include '../includes/nav.php';

?>
<?php 
 if(!isset($_SESSION ['member'])){
 	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Please loggin')
        window.location.href='login.php'
        </SCRIPT>");
 	
 }
	if (isset ( $_POST ['submit'] )) {
		$eventID = $_GET ['eventID'];
		$memberID = $_POST ['memberID'];
		$donationMoney = $_POST ['donationMoney'];
		
		
		$sql = "INSERT INTO fundedmoney (eventID,memberID,donationMoney,donationdate) VALUES ('$eventID', '$memberID','$donationMoney', NOW())";
		$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); 
			echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('succeesfully donated money')
        window.location.href='memberlanding.php'
        </SCRIPT>");
		} 
	
		
	else {
		?>
	
<?php
		
		$memberID = $_SESSION ['user'];
		
		$eventID = $_GET ['eventID'];
		$sql = "SELECT * FROM events WHERE eventID =$eventID";
		$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
		$row = mysqli_fetch_array ( $result );
		$pageTitle = $row ['title'];
		$content = $row ['content'];
		$venue = $row ['venue'];
		$eventdate = $row ['eventdate'];
		$eventimg = $row ['eventimg'];
		
		$sql = "SELECT * FROM member WHERE memberID = '$memberID'";
		$re = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
		$qul = mysqli_fetch_array ( $re );
		?>
<div class="container ">
<div class="row box  box-blue">
	<div class="col-md-12">
	<h1>Donation the Event </h1><hr>


	<h2 class="text-center"><?php echo $pageTitle?></h2>




	
	<form class="form-horizontal" method="post" enctype='multipart/form-data'>
<fieldset>

<!-- Form Name -->
<legend></legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Card">Card Number</label>  
  <div class="col-md-5">
  <input id="Card" name="card" type="text" placeholder="" class="form-control input-md" required="">
  <span class="help-block">Enter 16 digit card number.</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="fullname">Full Name</label>  
  <div class="col-md-2">
  <input id="firstname" name="first" type="text" placeholder="First Name" class="form-control input-md" required="">
  <span class="help-block">Exactly as it appears on the card.</span>  
 </div>
 
 <!-- Text input-->
<div class="form-group"> 
  <div class="col-md-2">
  <input id="lastname" name="last" type="text" placeholder="Last Name" class="form-control input-md" required="">
  <span class="help-block"></span>
  </div></div>
 
<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Exp Date">Expiration Date</label>
  <div class="col-md-2">
    <select id="Exp Date" name="Exp Date" class="form-control">
      <option value="Month">Month</option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
      <option value="6">6</option>
      <option value="7">7</option>
      <option value="8">8</option>
      <option value="9">9</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
    </select>
</div style="float:left;">

<!-- Select Basic -->
<div class="form-group">
  <div class="col-md-2">
    <select id="YY" name="Year" class="form-control">
      <option value="Year">Year</option>
      <option value="2016">2016</option>
      <option value="2017">2017</option>
      <option value="2018">2018</option>
      <option value="2019">2019</option>
      <option value="2020">2020</option>
      <option value="2021">2021</option>
      <option value="2022">2022</option>
      <option value="2023">2023</option>
      <option value="2024">2024</option>
      <option value="2025">2025</option>
      <option value="2026">2026</option>
      <option value="2027">2027</option>
      <option value="2028">2028</option>
    </select>
  </div></div>
  
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="cvv">Security Code</label>  
  <div class="col-md-1">
  <input id="" name="CVV" type="text" placeholder="" class="form-control input-md" required="">
  </div></div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="">Street Address 1</label>  
  <div class="col-md-5">
  <input id="" name="Address 1" type="text" placeholder="" class="form-control input-md" required="">
  </div></div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="">Street Address 2</label>  
  <div class="col-md-5">
  <input id="" name="Address 2" type="text" placeholder="Apartment, suite, etc." class="form-control input-md" required="">
  </div></div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="">City</label>  
  <div class="col-md-2">
  <input id="" name="City" type="text" placeholder="" class="form-control input-md" required=""></div></div>




<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="">Zip or postal code</label>  
  <div class="col-md-2">
  <input id="" name="Zip" type="text" placeholder="" class="form-control input-md" required="">
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="">Contact Email</label>  
  <div class="col-md-3">
  <input id="" name="Email" type="text" placeholder="" class="form-control input-md" required="">
  
    
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="">AMONUT</label>  
  <div class="col-md-3">
  <input id="" name=donationMoney type="text" placeholder="$AUD" class="form-control input-md" required="">
  
    
  </div>
</div>



</fieldset>

						
						<input type="hidden" name="memberID"value="<?php echo $memberID; ?>">
				
							<input type="hidden" name="command" />
							<input type="submit" class="btn btn-danger pull-right"name="submit" ><br>
						
						</form>  <?php }?></div></div></div>
<?php
 include '../includes/footer.php';
?>