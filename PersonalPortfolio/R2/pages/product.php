<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: product.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: shopping page for product information
--------------------------------------------------------->

<?php
$page = "product";
include '../includes/connect.php';
include '../includes/header.php';
include '../includes/nav.php';
include '../includes/functions.php'; // includes all the required PHP functions
?>

<body>
	<div class="container">
		<div class="row box box-mint">

			<div class="col-md-10">
				
				<h3>Product Information</h3>
				<hr>
			</div>
<?php
// if the $_REQUEST 'command' is 'add' than call the PHP addtocart function
// $_REQUEST is similar to $_GET and $_POST, but it contains the contents of both
if (isset ( $_REQUEST ['command'] ) && $_REQUEST ['command'] == 'add' && $_REQUEST ['productID'] > 0) {
	$pid = $_REQUEST ['productID'];
	addtocart ( $pid, 1 );
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('Succesfully added to cart')
        window.location.href='cart.php'
        </SCRIPT>");
	//header("location:cart.php");
	exit ();
}
?>

<!-- JavaScript that creates the 'add' command if the 'Add to Cart' button is clicked -->
			<script>
 function addtocart(pid){
 document.form1.productID.value=pid;
 document.form1.command.value='add';
 document.form1.submit();
 }
</script>
			<!-- form with hidden fields to send productID and the $_REQUEST 'command' to the
next page -->

			<form name="form1">
				<input type="hidden" name="productID" /> <input type="hidden"
					name="command" />
			</form>

			<div class="col-md-12">

<?php
$sql = "SELECT * FROM product"; // sql query
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) // display the results
	
	?>



<?php

$productID = mysqli_real_escape_string ( $con, $_GET ['productID'] );
$sql = "SELECT * FROM product WHERE productID =$productID";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
while ( $row = mysqli_fetch_array ( $result ) ) {
	echo "<div class='col-md-6'>";
	echo "<img src='../img/shop/" . ($row ['productImage']) . "'" . "  class ='img-responsive' alt='product'" . " />"; // display the image stored inside the images subfolder in another subfolder named shop (in your database store the image name e.g., image.jpg in the image column of your product table) >
	echo "</div >";
	echo "<div class='col-md-6'>";
	echo "<div class='row'>";
	echo "<h3>" . $row ['productName'] . "</h3>";
	echo "<h4>" . $row ['productDescription'] . "</h4><br>";
	echo "<h4><strong>Price: $" . $row ['productPrice'] . "</strong></h4>";
	echo "<p><input type='button' class='btn btn-danger pull-right ' value='Add to Cart' onclick='addtocart(" . $row['productID' ] . ")' /></p>"; // 'Add to Cart' button
			echo "</div>";
	
	echo "</div>";
}

?>

</div>

			</div>
		</div>
	</div>



<?php
include "../includes/footer.php";
?>