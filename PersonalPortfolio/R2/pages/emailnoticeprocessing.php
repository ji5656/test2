<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: eventnotice.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: Function to sending mail to member about events information.
--------------------------------------------------------->
<?php
session_start ();
include '../includes/connect.php';

?>
<?php

require 'PHPMailerAutoload.php';
$mail = new PHPMailer ();
$mail->CharSet = "utf-8";
$mail->IsSMTP ();
// enable SMTP authentication
$mail->SMTPAuth = true;
// GMAIL username
$mail->Username = "community82.ifb299@gmail.com";
// GMAIL password
$mail->Password = "adminpassword123";
$mail->SMTPSecure = "ssl";
// sets GMAIL as the SMTP server
$mail->Host = "smtp.gmail.com";
// set the SMTP port for the GMAIL server
$mail->Port = "465";
$mail->From = 'community82.ifb299@gmail.com';
$mail->FromName = 'community';

?>
<?php

$eventID = $_POST ['eventID'];
$sql = "SELECT * FROM member, events WHERE eventID = $eventID";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
$row = mysqli_fetch_array ( $result ); // create a variable called '$row' to store the results

while ( $row = mysqli_fetch_array ( $result ) ) {
	
	foreach ( $result as $row ) {
		
		$title = $row ['title'];
		$contents = $row ['content'];
		$eventdate = $row ['eventdate'];
		$venue = $row ['venue'];
		$img = $row ['eventimg'];
		$user = $row ['username'];
		$mail->AddEmbeddedImage ( '../img/' . ($row ['eventimg']), 'eventimg' );
		$mail->Subject = "community82 upcoming event notice";
		$body = "<h1>Hello $user</h1>";
		$body .= "<h2>Upcoming Event</h2>";
		$body .= "<h2>$title</h2>";
		$body .= "<h5>$contents</h5>";
		$body .= "<h4>$venue</h4>";
		$body .= "<h4>$eventdate</h4>";
		$body .= "<h4><a href ='http://54.206.25.253'>More detail visit website</a></h4>";
		$mail->msgHTML ( $body );
		$mail->addAddress ( $row ['email'], $row ['firstname'] );
		
		if (! $mail->send ()) {
			
			// echo "Message sent to :" . $row ['firstname'] . ' (' . str_replace ( "@", "&#64;", $row ['email'] ) . ')<br />';
			
			header ( 'location: ../admin/emailnotice.php' );
		} elseif ($mail->send ()) {
			
			header ( 'location: ../admin/emailnotice.php' );
			$_SESSION ['msg'] = 'Email sent successfully.';
		}
	}
}

?>
