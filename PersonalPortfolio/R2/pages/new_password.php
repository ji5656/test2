<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: new_password.php
File Version: 1.0.1(Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: new password sending email to member
--------------------------------------------------------->
<?php
session_start ();
include '../includes/connect.php';

?>
<?php

$email = mysqli_real_escape_string ( $con, $_POST ['email'] );
$username = mysqli_real_escape_string ( $con, $_POST ['username'] );

$sql = "SELECT * FROM member WHERE member.email='$email' and member.username='$username'  ";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query

$row = mysqli_fetch_array ( $result ); // create a variable called '$row' to store the results

$count = mysqli_num_rows ( $result ); // count the number of rows returned by the query
function random_password($length = 8) {
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$temp_pw = substr ( str_shuffle ( $chars ), 0, $length );
	return $temp_pw;
}

if ($count == 1) {
	
	$temp_pw = random_password ( 8 );
	
	require 'PHPMailerAutoload.php';
	$mail = new PHPMailer ();
	$mail->CharSet = "utf-8";
	$mail->IsSMTP ();
	// enable SMTP authentication
	$mail->SMTPAuth = true;
	// GMAIL username
	$mail->Username = "community82.ifb299@gmail.com";
	// GMAIL password
	$mail->Password = "adminpassword123";
	$mail->SMTPSecure = "ssl";
	// sets GMAIL as the SMTP server
	$mail->Host = "smtp.gmail.com";
	// set the SMTP port for the GMAIL server
	$mail->Port = "465";
	$mail->From = 'community82.ifb299@gmail.com';
	$mail->FromName = 'community';
	$mail->AddAddress ( $email, $username );
	$mail->Subject = 'Reset Password at community82';
	$mail->IsHTML ( true );
	$mail->Body = 'new password is <strong>' . $temp_pw . '</strong><br> Please login again.';
	
	if ($mail->Send ()) {
		
		$salt = md5 ( uniqid ( rand (), true ) ); // create a random salt value
		$password = hash ( 'sha256', $temp_pw . $salt );
		$sql = "UPDATE member SET password='$password', salt='$salt' WHERE
			email='$email'";
	}
	$_SESSION ['msg'] = ' new password was sent to your email.please login again and change your password</span>';
	header ( 'location: index.php' );
} else {
	$_SESSION ['msg'] = 'Error. Fail to matach username and email.</span>'; // register a session with an error message
	header ( 'location: login.php' );
	// redirect to account.php
}

?>
