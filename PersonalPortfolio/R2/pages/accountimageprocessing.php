<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: volmanage.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------


Description of the page: Page for update member image.
--------------------------------------------------------->
<?php
session_start ();
include "../includes/connect.php";
?>
<?php

$memberID = $_POST ['memberID'];
if ($_FILES ['image'] ['name']) {
	$image = $_FILES ['image'] ['name'];
	$randomDigit = rand ( 0000, 9999 );
	$newImageName = strtolower ( $randomDigit . "_" . $image );
	$allowedExts = array (
			'jpg',
			'jpeg',
			'gif',
			'png' 
	);
	$tmp = explode ( '.', $_FILES ['image'] ['name'] );
	$extension = end ( $tmp );
	
	if ($_FILES ['image'] ['size'] > 512000) {
		$_SESSION ['error'] = 'Your file size exceeds maximum of 500kb.'; // if file exceeds max size initialise a session called 'error' with a msg
		header ( "location:registration.php" );
		exit ();
	} elseif (($_FILES ['image'] ['type'] == 'image/jpg') || ($_FILES ['image'] ['type'] == 'image/jpeg') || ($_FILES ['image'] ['type'] == 'image/gif') || ($_FILES ['image'] ['type'] == 'image/png') && in_array ( $extension, $allowedExts )) {
		move_uploaded_file ( $_FILES ['image'] ['tmp_name'], $target );
	} else {
		$_SESSION ['error'] = 'Only JPG and PNG files allowed.'; // if file uses an invalid extension initialise a session called 'error' with a msg
		header ( "location:registration.php" ); // redirect to registration.php
		exit ();
	}
}

$sql = "UPDATE member SET image='$newImageName' WHERE memberID='$memberID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query

$_SESSION ['success'] = 'Image updated successfully'; // if registration is successful initialise a session called 'success' with a msg
header ( "location:account.php" );
?>
