<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: donation.php
File Version: 1.0.1(Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page:member registration page
--------------------------------------------------------->
<?php
$page = "Registration";
include '../includes/connect.php';
include '../includes/header.php'; // session_start(); included in header.php
include '../includes/nav.php';
?>
	<?php
	
if (isset ( $_SESSION ['member'] )) {
		echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('You already Loggined')
        window.location.href='memberlanding.php'
        </SCRIPT>");
	} else {
		
		?>
<div class="container">
	<div class="row box">
		<div class="col-md-12">
			<hr>
			<h2>Become a member</h2>
			<hr>

<?php
		// user messages
		if (isset ( $_SESSION ['error'] )) {
			echo '<div class="error">';
			echo '<p>' . $_SESSION ['error'] . '</p>';
			echo '</div>';
			unset ( $_SESSION ['error'] );
		}
		?><div class="col-md-6 col-md-offset-3">
				<form action="registrationprocessing.php" method="post"
					enctype="multipart/form-data">
					<!-- the multipart/form-data is essential for file upload functionality -->
					<p>Complete the details below to sign up for a new account.</p>
					<p>Passwords must have a minimum of 8 characters.</p>
					<label>Username*</label> <input type="text" name="username"
						class="form-control" required /><br /> <label>Password*</label> <input
						type="password" name="password" class="form-control" required
						pattern=".{8,}" title="Password must be 8 characters or more" /><br />
					<label>First Name*</label> <input type="text" name="firstname"
						class="form-control" required /><br /> <label>Last Name*</label> <input
						type="text" name="lastname" class="form-control" required /><br />
					<label>Street Number</label> <input type="text" name="streetnum"
						class="form-control" /><br /> <label>Street Name</label> <input
						type="text" name="streetname" class="form-control" /><br /> <label>Suburb</label>
					<input type="text" name="suburb" class="form-control" /><br /> <label>State</label>
 <?php
		
		$tableName = 'member';
		
		$colState = 'state';
		function getEnumState($tableName, $colState) {
			global $con; // enable database connection in the function
			$sql = "SHOW COLUMNS FROM $tableName WHERE field='$colState'"; // retrieve enum column
			
			$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
			
			$row = mysqli_fetch_array ( $result ); // store the results in a variable named $row
			
			$type = preg_replace ( '/(^enum\()/i', '', $row ['Type'] ); // regular expression to replace the enum syntax with blank space
			
			$enumValues = substr ( $type, 0, - 1 ); // return the enum string
			
			$enumExplode = explode ( ',', $enumValues ); // split the enum string into individual values
			return $enumExplode; // return all the enum individual values
		}
		$enumValues = getEnumState ( 'member', 'state' );
		echo '<select name="state">';
		
		echo "<option value=''>Please select</option>";
		foreach ( $enumValues as $value ) {
			echo '<option value="' . $removeQuotes = str_replace ( "'", "", $value ) . '">' . $removeQuotes = str_replace ( "'", "", $value ) . '</option>'; // remove the quotes from the enum values
		}
		echo '</select><br />';
		?>
			<p>&nbsp;</p>
					<label>Postcode*</label> <input type="text" class="form-control"
						name="postcode" /><br /> <label>Country*</label> <input
						type="text" class="form-control" name="country" required /><br />
					<label>Phone</label> <input type="text" class="form-control"
						name="phone" /><br /> <label>Mobile</label> <input type="text"
						class="form-control" name="mobile" /><br /> <label>Email*</label>
					<input type="email" class="form-control" name="email" required /><br />
					<label>Gender*</label>
 <?php
		// generate drop-down list for gender using enum data type and values from database
		$tableName = 'member';
		$colGender = 'gender';
		function getEnumGender($tableName, $colGender) {
			global $con; // enable database connection in the function
			$sql = "SHOW COLUMNS FROM $tableName WHERE field='$colGender'"; // retrieve enum column
			
			$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
			
			$row = mysqli_fetch_array ( $result ); // store the results in a variable named $row
			
			$type = preg_replace ( '/(^enum\()/i', '', $row ['Type'] ); // regular expression to replace the enum syntax with blank space
			
			$enumValues = substr ( $type, 0, - 1 ); // return the enum string
			
			$enumExplode = explode ( ',', $enumValues ); // split the enum string into individual values
			return $enumExplode; // return all the enum individual values
		}
		
		$enumValues = getEnumGender ( 'member', 'gender' );
		echo '<select name="gender">';
		
		echo "<option value=''>Please select</option>";
		
		foreach ( $enumValues as $value ) {
			echo '<option class="form-control" value="' . $removeQuotes = str_replace ( "'", "", $value ) . '">' . $removeQuotes = str_replace ( "'", "", $value ) . '</option>';
		}
		echo '</select>';
		?>
	<br />
					<h3>Subscribe to weekly email newsletter?</h3>

					<label>Yes</label><input type="radio" name="newsletter" value="Y"
						checked><br /> <label>No</label><input type="radio"
						name="newsletter" value="N"><br /> <label>Image</label><input
						class="form-control" type="file" name="image" />

					<h4>Accepted files are JPG, GIF or PNG. Maximum size is 500kb.</h4>

					<p>
						<input type="submit" name="registration" class="form-control"
							value="Create New Account" />
					</p>
				</form>
			</div>
		</div>
		<!-- end content -->
<?php }?>
<?php
	include '../includes/footer.php';
?>
