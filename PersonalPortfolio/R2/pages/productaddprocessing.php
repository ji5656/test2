<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: productaddprocessing.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page:processing for adding new product 
--------------------------------------------------------->
<?php
include "../includes/connect.php";
?>
<?php



$productName = mysqli_real_escape_string ( $con, $_POST ['productName'] ); // prevent SQL injection
$productDescription = mysqli_real_escape_string ( $con, $_POST ['productDescription'] );
$productPrice = mysqli_real_escape_string ( $con, $_POST ['productPrice'] );

if ($_FILES ['image'] ['name']) // if an image has been uploaded
{
	$image = $_FILES ['image'] ['name']; // the PHP file upload variable for a file
	$randomDigit = rand ( 0000, 9999 ); // generate a random numerical digit <= 4 characters
	$newImageName = strtolower ( $randomDigit . "_" . $image ); // attach the random digit to the front of uploaded images to prevent overriding files with the same name in the images folder and enhance security
	$target = "../img/shop/" . $newImageName; // the target for uploaded images
	$allowedExts = array (
			'jpg',
			'jpeg',
			'gif',
			'png'
	); // create an array with the allowed file extensions
	$tmp = explode ( '.', $_FILES ['image'] ['name'] ); // split the file name from the file extension
	$extension = end ( $tmp ); // retrieve the extension of the photo e.g., png

	if ($_FILES ['image'] ['size'] > 512000) // image maximum size is 500kb
	{
		$_SESSION ['msg'] = 'Your file size exceeds maximum of 500kb.'; // if file exceeds max size initialise a session called 'error' with a msg
		header ( "location:product.php" ); // redirect to registration.php
		exit ();
	} elseif (($_FILES ['image'] ['type'] == 'image/jpg') || ($_FILES ['image'] ['type'] == 'image/jpeg') || ($_FILES ['image'] ['type'] == 'image/gif') || ($_FILES ['image'] ['type'] == 'image/png') && in_array ( $extension, $allowedExts )) {
		move_uploaded_file ( $_FILES ['image'] ['tmp_name'], $target ); // move the image to images folder
	} else {
		$_SESSION ['msg'] = 'Only JPG, GIF and PNG files allowed.'; // if file uses an invalid extension initialise a session called 'error' with a msg
		header ( "location:product.php" ); // redirect to registration.php
		exit ();
	}
}

$sql = "INSERT INTO product (productName, productDescription, productPrice, productImage
) VALUES (' $productName', '$productDescription', $productPrice, '$newImageName')";

$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query

echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('add event successfully.')
 		 window.location.href='shop.php'
 		</SCRIPT>");

?>