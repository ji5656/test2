<!-------------------------------------------------------

Subject: IFB299		Group: Group 82
Webpage: adminlogin.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion) 
Author: Ji-Young Choi


---------------------------------------------------------
				Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)

Formatting page.

---------------------------------------------------------

Description of the page: admin login page.
--------------------------------------------------------->

<?php
 $page = "Login";
 include '../includes/connect.php';
 include '../includes/header.php';
 include '../includes/nav.php';
?>

<div class="container">
<div class="row box box-pink">
<div class="col-md-6 col-md-offset-2">
<h2>Admin LOGIN</h2><hr>


<?php

 if(isset($_SESSION['error'])){
 	echo '<div class="error">';
 	echo '<p>' . $_SESSION['error'] . '</p>';
 	echo "</div>";
 	unset($_SESSION['error']); 
 }

 if(isset($_SESSION['success'])){
 	echo "<p class = 'success'>" . $_SESSION['success'] . "</p>"; //echo  the message
 	unset($_SESSION['success']); //unset the session
 }
?>

<form  action="../pages/loginprocessing.php" method="post">
<h3>Admin / President / Committee Login</h3>
<p>for the test : admin(id:admintest pw:12345678) /president(id:president pw:12345678 )/ committee(id:com2 pw:12345678)</p>
	
		<label for="username">Username : </label>
		<input class="form-control"type="text" name="username" id="username" placeholder="Enter your username" required/>
	
	
		<label for="password">Password  : </label>
		<input type="password" class="form-control"name="password" id="password" placeholder="Enter your password" required />
	
 		<input type="hidden" value="<?php echo $_GET['eventID']; ?>" name="eventID"/>
		<p><input type="submit" class="form-control"name="login" value="Login" /></p>
</form>
</div>
<?php
 include '../includes/footer.php';
?>
