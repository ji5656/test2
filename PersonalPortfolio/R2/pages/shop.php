<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: shop.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page:main shop page for listing products
--------------------------------------------------------->

<?php
$page = "shop";
include '../includes/connect.php';
include '../includes/header.php';
include '../includes/nav.php';

?>

<div class="container ">

	<div class="row box box-bgreen">
		<div class="col-md-12">



			<h1>
				<strong>Shop</strong>
			</h1>
			<hr>
	



	
			
<?php
$sql = "SELECT * FROM product"; // sql query
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) // display the results
{
	
	echo "<div class='col-md-3 col-md2'>";
	echo "<div class='mainproduct'>";
	echo "<a href='product.php?productID=" . $row ['productID'] . "'>";
	echo "<img src='../img/shop/" . ($row ['productImage']) . "'" . " class='img-responsive '  alt='product'" . " width:/>";
	echo "</a>";
	echo "<h3 class='text-center'>" . $row ['productName'] . "</h3>";
	echo "<h4 class='text-center'>$" . $row ['productPrice'] . "</h4>";
	
	echo "</div>";
	echo "</div>";
}

?>       
						
 

      </div>

	</div>


<?php
include "../includes/footer.php";
?>
