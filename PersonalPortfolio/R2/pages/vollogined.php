<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpagevollogined.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: landing page for volunteer
--------------------------------------------------------->
<?php
$page = "volunteeraccount";
include '../includes/connect.php';
include '../includes/header.php';
include '../includes/nav.php';
include "../includes/logincheckvol.php";
?>

<div class="container ">

	<div class="tabbable-panel row">
		<div class="tabstyle belowline">
			<ul class="nav nav-tabs">


				<li><a class="active" data-toggle="tab" href="#profile">Profile</a></li>
				<li><a data-toggle="tab" href="#updateprofile">Update Profile</a></li>

			</ul>

			<div class="tab-content">
				<div id="profile" class="tab-pane fade in active">
					<div class="row">
						<div class="col-md-4 box">


							<h3>Profile</h3>
							<hr>
					
   <?php
			$volunteerID = $_SESSION ['volunteer'];
			
			$sql = "SELECT * FROM volunteer where username='$volunteerID'";
			$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
			while ( $row = mysqli_fetch_array ( $result ) ) 

			{
				$volID = $row ['volunteerID'];
				$fname = $row ['firstname'];
				$lname = $row ['lastname'];
				$email = $row ['email'];
				$mobile = $row ['mobile'];
				
				echo "<li class='list-group-item'><h4><i class='glyphicon glyphicon-user'></i> $fname $lname </h4></li>";
				echo "<li class='list-group-item'><h4><i class='glyphicon glyphicon-envelope'> $email </i></h4></li>";
				echo "<li class='list-group-item'><h4><i class='glyphicon glyphicon-phone'> $mobile </i></h4></li>";
				
				$sql = "SELECT vol_attendee.*, events.* FROM  vol_attendee join events using (eventID) WHERE volID = '$volID' ORDER BY events.eventdate ASC";
				$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
				$row_count = mysqli_num_rows ( $result );
				
				while ( $row = mysqli_fetch_array ( $result ) ) {
				}
			}
			?></div>
						<div class="col-md-6 box box-mint">
							<h3>Recent Event History</h3>
							<table class="table">
								<thred>
								<th>title</th>
								<th>date</th>
								<th>venue</th>
								<th>position</th>
								</thred>





<?php
$sql = "SELECT * FROM volunteer WHERE username = '$volunteerID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
$row_count = mysqli_num_rows ( $result );

while ( $row = mysqli_fetch_array ( $result ) ) {
	$volID = $row ['volunteerID'];
	
	$sql = "SELECT vol_attendee.*, events.* FROM  vol_attendee join events using (eventID) WHERE volID = '$volID' ORDER BY events.eventdate ASC";
	
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
	while ( $row = mysqli_fetch_array ( $result ) ) 

	{
		
		echo '<tr>';
		echo '<td>' . $row ['title'] . '</td>';
		echo '<td>' . date ( "Y-m-d", strtotime ( $row ['eventdate'] ) ) . '</h5>';
		echo '<td>' . $row ['venue'] . '</td>';
		echo '<td>' . $row ['position'] . '</td>';
		echo '</tr>';
	}
}
?></table>
						</div>

						<div class="col-md-6 col-md-offset-4 box box-pink">
							<script src="https://code.highcharts.com/highcharts.js"></script>
							<script src="https://code.highcharts.com/highcharts-more.js"></script>
							<script src="https://code.highcharts.com/modules/exporting.js"></script>
							<div id="container"
								style="min-width: 400px; max-width: 600px; height: 400px; margin: 0 auto">
								<script>
$(function () {

    $('#container').highcharts({

        chart: {
            polar: true,
            type: 'line'
        },

        title: {
            text: 'skills',
            x: -80
        },

        pane: {
            size: '80%'
        },

        xAxis: {
            categories: ['communication skill', 'Technical skill', 'Experience', 'Management','Responsibility','other'],
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polargon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },

        series: [ {
            name: 'skill point',
            data: [40, 90, 40, 40, 60, 50],
            pointPlacement: 'on'
        }]

    });
});
</script>
							</div>
						</div>
					</div>
				</div>

				<div id="updateprofile" class="tab-pane fade ">
					<div class="row">
						<div class="col-md-6 box box-bgreen">
<?php
$username = $_SESSION ['volunteer'];

$sql = "SELECT * FROM volunteer where username='$username'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) 

{
	$volID = $row ['volunteerID'];
	
	?>

<hr>
							<h2>My Account</h2>
							<hr>
							<p>Update your account details.</p>

							<form action="volaccountprocessing.php" method="post">

								<label>Username*</label> <input type="text" name="username"
									required value="<?php echo $row['username'] ?>"
									class="form-control" readonly /><br /> <label>First Name*</label>
								<input type="text" class="form-control" name="firstname"
									required value="<?php echo $row['firstname'] ?>" /><br /> <label>Last
									Name*</label> <input type="text" class="form-control"
									name="lastname" required value="<?php echo $row['lastname'] ?>" /><br />




								<label>Phone</label> <input type="text" class="form-control"
									name="phone" value="<?php
	
echo $row ['phone']?>" /><br /> <label>Mobile</label> <input type="text"
									class="form-control" name="mobile"
									value="<?php
	
echo $row ['mobile']?>" /><br /> <label>Email*</label> <input type="email"
									class="form-control" name="email" required
									value="<?php echo $row['email'] ?>" /><br /> <label>Gender*</label>
<?php
	// generate drop-down list for gender using enum data type and values from database
	$tableName = 'volunteer';
	$colGender = 'gender';
	function getEnumGender($tableName, $colGender) {
		global $con; // enable database connection in the function
		$sql = "SHOW COLUMNS FROM $tableName WHERE field='$colGender'";
		// retrieve enum column
		$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
		// run the query
		$row = mysqli_fetch_array ( $result ); // store the results in a variable named $row
		$type = preg_replace ( '/(^enum\()/i', '', $row ['Type'] ); // regular expression to replace the enum syntax with blank space
		$enumValues = substr ( $type, 0, - 1 ); // return the enum string
		$enumExplode = explode ( ',', $enumValues ); // split the enum string into individual values
		return $enumExplode; // return all the enum individual values
	}
	$enumValues = getEnumGender ( 'volunteer', 'gender' );
	echo '<select name="gender">';
	
	echo "<option value=" . $row ['gender'] . ">" . $row ['gender'] . "</option>"; // display the selected enum value
	
	foreach ( $enumValues as $value ) {
		echo '<option value="' . $removeQuotes = str_replace ( "'", "", $value ) . '">' . $removeQuotes = str_replace ( "'", "", $value ) . '</option>';
	}
	echo '</select>';
	?>

<input type="hidden" name="volunteerID" value="<?php echo $username; ?>">
								<input type="submit" class="form-control" name="accountupdate"
									value="Update Account" />
							</form>
						</div>
						<div class="col-md-4 box box-blue">
							<h3>Update Password</h3>
							<p>Passwords must have a minimum of 8 characters.</p>

							<form action="volaccountpasswordprocessing.php" method="post">
								<label>New Password*</label> <input class="form-control"
									type="password" name="password" pattern=".{8,}"
									title="Password must be 8 characters or more" required /><br />
								<input type="hidden" name="username"
									value="<?php echo $username; ?>">
								<p>
									<input type="submit" class="form-control name="
										passwordupdate" value="Update Password" <?php }?> />
								</p>
							</form>
						</div>


						<div class="col-md-4 box box-mint">
							<h3>Delete My Account</h3>
							<p>
								We're sorry to hear you'd like to delete your account.<br /> By
								clicking the button below you will be applied to delete your
								accout to admin. when your account delete, confirm mail will be
								sent
							</p>
							<form action="volaccountprocessing.php" method="post">
								<input type="hidden" name="username"
									value="<?php echo $username; ?>">
								<div class="form-group">
									<input type="submit" name="delete" value="Delete My Account"
										onclick="return
confirm('Are you sure you wish to permanently delete your account?');">
								</div>

							</form>
						</div>

					</div>
				</div>
			</div>
<?php
include "../includes/footer.php";
?>