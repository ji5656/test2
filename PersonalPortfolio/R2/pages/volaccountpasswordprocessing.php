<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage:volaccountpasswardprocessing.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page:volunteer change the password
--------------------------------------------------------->
<?php
session_start ();
include '../includes/connect.php';
?>
<?php

$username = $_POST ['username'];
$password = mysqli_real_escape_string ( $con, $_POST ['password'] );

if (strlen ( $password ) < 8) {
	$_SESSION ['error'] = 'Password must be 8 characters or more.'; // if password is less than 8 characters initialise a session called 'regoerror1' to have a value of the error msg
	header ( "location:volaccount.php" ); // redirect to registration.php
	exit ();
} else {
	
	$salt = md5 ( uniqid ( rand (), true ) ); // create a random salt value
	
	$password = hash ( 'sha256', $password . $salt ); // generate the hashed password with the salt value
	
	$sql = "UPDATE volunteer SET password='$password', salt='$salt' WHERE username = '$username'";
	
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
}

if ($result) {
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('succeesfully updated')
        window.location.href='vollogined.php'
        </SCRIPT>");
} else {
	echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('error: fail to update')
        window.location.href='vollogined.php'
        </SCRIPT>");
}
?>
