<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: memberlanding.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: Laning page for member
--------------------------------------------------------->


<?php
$page = "My Account";
include '../includes/connect.php';
include '../includes/header.php';
include '../includes/nav.php';
include "../includes/loginmembercheck.php";
?>

<div class="container ">

	<div class="tabbable-panel row">
		<div class="tabstyle belowline">
			<ul class="nav nav-tabs">

				<li class="active"><a class="active" data-toggle="tab" href="#home">Event</a></li>
				<li><a class="tab" data-toggle="tab" href="#profile">Profile</a></li>
				<li><a data-toggle="tab" href="#newitem">Selling Item</a></li>
				<li><a data-toggle="tab" href="#updateprofile">Update Profile</a></li>

			</ul>

			<div class="tab-content">



				<div id="home" class="tab-pane fade in active box box-mint"
					style='margin: 2% 10%; border-top-color: #FFCC5C;'>
					<div class="row">

						<h2 class="text-center">Upcoming your event schedule</h2>
						<hr>


						<div class="col-md-4 " style='margin-left: 5%;'>

					
<?php
$memberID = $_SESSION ['user'];
$sql = "SELECT memberattendee.*, events.* FROM memberattendee join events using (eventID) WHERE memberID = '$memberID' ORDER BY events.eventdate ASC LIMIT 3";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) 

{
	
	echo '<div class="row "style="background-color:#022C37; margin:10px 0;">';
	echo '<div class="eventcardtxt1 pull-left text-center">';
	echo '<h5>' . $row ['title'] . '</h5><hr>';
	echo '<h5>' . date ( "Y-m-d H:i", strtotime ( $row ['eventdate'] ) ) . '</h5>';
	
	echo '<p>' . $row ['venue'] . '</p>';
	echo '<a href="eventpost.php?eventID=' . $row ['eventID'] . '" type="button" class="btn btn-primary-outline event"><i>More details</i></a>';
	echo '</div>';
	
	echo "<div class='pull-right'><img src='../img/" . ($row ['eventimg']) . "'" . "style='width:150px; height:200px;' background-size='cover'>";
	echo '</div></div>';
}
?>
</div>

						<div class="col-md-6 push-right">

							<h3>Your Recent Contribution</h3>
							<table class="table ">
								<thred>
								<th>donationTitle</th>
								<th>goalMoney</th>
								<th>donation money</th>
								<th>donation date</th>
								</thred>
<?php
$memberID = $_SESSION ['user'];
$sql = "SELECT `fundedmoney`.*, `events`.* FROM `fundedmoney` join `events` using (`eventID`) WHERE `memberID` ='$memberID' ORDER BY 
donationdate DESC LIMIT 0,4";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) 

{
	echo '<tr>';
	echo '<td>' . $row ['title'] . '</td>';
	echo '<td>' . $row ['eventGoal'] . '</td>';
	echo '<td>' . $row ['donationMoney'] . '</td>';
	echo '<td>' . $row ['donationdate'] . '</td>';
	echo '</tr>';
}
?>
 </table>
						</div>
<?php
$sql = "select sum(donationMoney)\n" . "FROM fundedmoney,events where memberID =$memberID ";

$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
$row = mysqli_fetch_array ( $result );
$total = $row ['sum(donationMoney)'];

$sql = "select AVG(donationMoney) FROM fundedmoney ";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
$ss = mysqli_fetch_array ( $result );
$avg = $ss ['AVG(donationMoney)'];

?>	
<div class="col-md-4">
							<h3>Total Contribution</h3>
							<div id="bar-example"></div>
						</div>

					</div>

				</div>



				<script>
Morris.Bar({
	  element: 'bar-example',
	  parseTime: false,
	  barColors: [
		   
		    '#ff4400', '#0BA462'
		    
		  ],
	  data: [
	    { y: 'Total Contribution',a: <?php echo $total ?>, b: <?php echo $avg ?>},
	    
	   
	  
	  ],
	  xkey: 'y',
	  ykeys: ['a', 'b'],
	  labels: ['You', 'Member Average']
	});
</script>




				<div id="profile" class="tab-pane fade ">
					<div class="col-md-4 box box-mint">
						<h3>Profile</h3>
					
   <?php
			$memberID = $_SESSION ['user'];
			$sql = "SELECT * FROM member where memberID=$memberID";
			$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
			while ( $row = mysqli_fetch_array ( $result ) ) 

			{
				$fname = $row ['firstname'];
				$lname = $row ['lastname'];
				$email = $row ['email'];
				$mobile = $row ['mobile'];
				echo "<ul class='list-group'><li class='list-group-item'><img src='../img/" . ($row ['image']) . "'" . "style='width:100px; height:100px;' background-size='cover' border:'3px solid #d2d6de;' class='img-circle img-responsive '></li>";
				echo "<li class='list-group-item'><h4><i class='glyphicon glyphicon-user'></i> $fname $lname </h4></li>";
				echo "<li class='list-group-item'><h4><i class='glyphicon glyphicon-envelope'> $email </i></h4></li>";
				echo "<li class='list-group-item'><h4><i class='glyphicon glyphicon-phone'> $mobile </i></h4></li>";
			}
			?></div>
					<div class="col-md-6 box box-mint">
						<table class="table ">
							<h2>Recent Order</h2>
							<tbody>
								<th>No.</th>
								<th>date</th>
								<th>total</th>
			<?php
			$memberID = $_SESSION ['user'];
			$sql = "SELECT * FROM invoice where memberID=$memberID GROUP BY invoiceID DESC LIMIT 2";
			$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
			$a = 1;
			while ( $row = mysqli_fetch_array ( $result ) ) 

			{
				echo "<tr><td>" . $a ++ . "</td>";
				echo "<td>" . $row ['dateTime'] . "</td>";
				echo "<td>" . $row ['total'] . "</td></tr>";
			}
			?>
			</tbody></table>
  
</div>

					</div>		
								<div id="newitem" class="tab-pane fade ">

									<div class="row ">
										<div class="col-md-6 box box-pink " style="margin-left: 20%;">

											<h2>Selling New Product For Charity</h2>
											<hr>
											<form action="productaddprocessing.php" method="post"
												enctype="multipart/form-data">

												<div class="form-group">
													<label>Title*</label> <input class="form-control"
														type="text" name="productName" required /><br />
												</div>

												<div class="form-group">
													<label>Description*</label>
													<textarea rows="10" cols="60%" name="productDescription"
														class="form-control"></textarea>
												</div>
												<br />
												<div class="form-group">
													<label>Price</label> <input type="text" name="productPrice"
														class="form-control" required /><br />
												</div>

												<div class="form-group">
													<label>Image</label> <input type="file" name="image"
														class="form-control" /><br />
												</div>
												<p>Accepted files are JPG, GIF or PNG. Maximum size is
													500kb.</p>
												<input type="hidden" name="productID"
													value="<?php echo $productID; ?>">

												<div class="form-group">
													<input type="submit" class="form-control" name="newproduct"
														value="Add New product" />
												</div>

											</form>
										</div>
									</div>
								</div>






								<div id="updateprofile" class="tab-pane fade ">
									<div class="row ">
										<div class="col-md-6 box box-mint">
<?php

$sql = "SELECT * FROM member WHERE memberID = '$memberID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
$row = mysqli_fetch_array ( $result );
?>



							
								
								<h2>My Account</h2>
											<hr>
											<p>Update your account details.</p>

											<form action="accountprocessing.php" method="post">
												<div class="form-group">
													<label>Username*</label> <input type="text"
														class="form-control" name="username" required
														value="<?php echo $row['username'] ?>" readonly /><br />
												</div>

												<div class="form-group">
													<label>First Name*</label> <input type="text"
														class="form-control" class="form-control" name="firstname"
														required value="<?php echo $row['firstname'] ?>" /><br />
												</div>

												<div class="form-group">
													<label>Last Name*</label> <input type="text"
														class="form-control" name="lastname" required
														value="<?php echo $row['lastname'] ?>" /><br />
												</div>

												<div class="form-group">
													<label>Street number</label> <input type="text"
														class="form-control" name="streetnum"
														value="<?php echo $row ['streetnum']?>" /><br /> <label>Street
														name</label> <input type="text" name="streetname"
														value="<?php
														
														echo $row ['streetname']?>" /><br />
												</div>

												<div class="form-group">
													<label>Suburb</label> <input type="text"
														class="form-control" name="suburb"
														value="<?php echo $row ['suburb']?>" /><br />
												</div>

												<div class="form-group">
													<label>State</label>
<?php

$tableName = 'member';
$colState = 'state';
function getEnumState($tableName, $colState) {
	global $con;
	$sql = "SHOW COLUMNS FROM $tableName WHERE field='$colState'";
	// retrieve enum column
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
	$row = mysqli_fetch_array ( $result );
	$type = preg_replace ( '/(^enum\()/i', '', $row ['Type'] ); // regular expression to replace the enum syntax with blank space
	$enumValues = substr ( $type, 0, - 1 );
	$enumExplode = explode ( ',', $enumValues );
	return $enumExplode;
}
$enumValues = getEnumState ( 'member', 'state' );
echo '<select name="state">';

if ((is_null ( $row ['state'] )) || (empty ( $row ['state'] ))) // if the state field is NULL or empty
{
	echo "<option value=''>Please select</option>";
} else {
	echo "<option value=" . $row ['state'] . ">" . $row ['state'] . "</option>"; // display the selected enum value
}

foreach ( $enumValues as $value ) {
	echo '<option value="' . $removeQuotes = str_replace ( "'", "", $value ) . '">' . $removeQuotes = str_replace ( "'", "", $value ) . '</option>'; // remove the quotes from the enum values
}
echo '</select><br />';
?>

<p>&nbsp;</p>
												</div>

												<div class="form-group">
													<label>Postcode*</label> <input type="text"
														class="form-control" name="postcode" required
														value="<?php echo $row['postcode'] ?>" /><br />
												</div>

												<div class="form-group">
													<label>Country*</label> <input type="text" name="country"
														class="form-control" required
														value="<?php echo $row['country'] ?>" /><br />
												</div>

												<div class="form-group">
													<label>Phone</label> <input type="text" name="phone"
														class="form-control"
														value="<?php
														
														echo $row ['phone']?>" /><br />
												</div>


												<div class="form-group">
													<label>Mobile</label> <input type="text" name="mobile"
														class="form-control"
														value="<?php
														echo $row ['mobile']?>" /><br />
												</div>


												<div class="form-group">
													<label>Email*</label> <input type="email" name="email"
														class="form-control" required
														value="<?php echo $row['email'] ?>" /><br />
												</div>

												<div class="form-group">
													<label>Gender*</label>
<?php
// generate drop-down list for gender using enum data type and values from database
$tableName = 'member';
$colGender = 'gender';
function getEnumGender($tableName, $colGender) {
	global $con; // enable database connection in the function
	$sql = "SHOW COLUMNS FROM $tableName WHERE field='$colGender'";
	// retrieve enum column
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
	// run the query
	$row = mysqli_fetch_array ( $result ); // store the results in a variable named $row
	$type = preg_replace ( '/(^enum\()/i', '', $row ['Type'] ); // regular expression to replace the enum syntax with blank space
	$enumValues = substr ( $type, 0, - 1 ); // return the enum string
	$enumExplode = explode ( ',', $enumValues ); // split the enum string into individual values
	return $enumExplode; // return all the enum individual values
}
$enumValues = getEnumGender ( 'member', 'gender' );
echo '<select name="gender" class="form-control">';

echo "<option value=" . $row ['gender'] . ">" . $row ['gender'] . "</option>"; // display the selected enum value

foreach ( $enumValues as $value ) {
	echo '<option value="' . $removeQuotes = str_replace ( "'", "", $value ) . '">' . $removeQuotes = str_replace ( "'", "", $value ) . '</option>';
}
echo '</select>';
?></div>

												<input type="hidden" name="memberID"
													value="<?php echo $memberID; ?>"> <input type="submit"
													name="accountupdate" value="Update Account" />
											</form>

										</div>





										<div class="col-md-4 box box-mint">
											<h3>Update Image</h3>
<?php
if ((is_null ( $row ['image'] )) || (empty ( $row ['image'] ))) // if the photo field is NULL or empty
{
	echo "<p><img src='../img/member.png' width=150 height=150
alt='default photo'  /></p>";
} else 

{
	echo "<p><img src='../img/" . ($row ['image']) . "'" . '
width=150 height=150 alt="contact photo"' . "/></p>";
}
?>

<form action="accountimageprocessing.php" method="post"
												enctype="multipart/form-data">

												<input type="hidden" name="memberID"
													value="<?php echo $memberID; ?>">

												<div class="form-group">
													<label>New Image</label> <input type="file" name="image"
														class="form-control" /><br />
													<p>Accepted files are JPG, GIF or PNG. Maximum size is
														500kb.</p>
												</div>

												<div class="form-group">
													<input type="submit" name="imageupdate"
														class="form-control" value="Update Image" />
											
											</form>


										</div>
									</div>

									<div class="col-md-4 box box-mint">
										<h3>Update Password</h3>
										<p>Passwords must have a minimum of 8 characters.</p>
										<form action="accountpasswordprocessing.php" method="post">
											<label>New Password*</label> <input type="password"
												name="password" pattern=".{8,}"
												title="Password must be 8 characters or more" required /><br />
											<input type="hidden" name="memberID"
												value="<?php echo $memberID; ?>">
											<p>
											
											
											<div class="form-group">
												<input type="submit" name="passwordupdate"
													value="Update Password" />
											</div>
											
										</form>
									</div>
									<div class="col-md-4 box box-mint">
										<h3>Delete My Account</h3>
										<p>
											We're sorry to hear you'd like to delete your account.<br />
											By clicking the button below you will be applied to delete
											your accout to admin. when your account delete, confirm mail
											will be sent
										</p>
										<form action="accountprocessing.php" method="post">

											<div class="form-group">
												<input type="submit" name="delete" value="Delete My Account"
													onclick="return
confirm('Are you sure you wish to permanently delete your account?');">
											</div>
											<input type="hidden" name="memberID"
												value="<?php echo $memberID; ?>">
										</form>
									</div>
								</div>

								</div>
								</div>


								</div>
								</div>
								</div>
								</div>

<?php
include "../includes/footer.php";
?>
