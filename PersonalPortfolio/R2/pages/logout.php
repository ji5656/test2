<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: donation.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page:logout process
--------------------------------------------------------->
<?php
 session_start();
 session_destroy();
 header("location:index.php"); 
?>
