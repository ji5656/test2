<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: footer.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page:footer login link administrators
--------------------------------------------------------->

<footer class="footer navbar-fixed-bottom">

	<div class="col-md-4 ">

		<img src="../img/COMMUNITYLOGO.png" width="300px">
	</div>
	<div class="col-md-4">
		<ul>

			<li><a class="foot" href="adminlogin.php">ADMIN LOGIN</a></li>
			<li><a class="foot" href="login.php">MEMBER/VOLUNTEER LOGIN</a></li>
			<li><a class="foot" href="regi.php">RESISTRATION</a></li>
		</ul>
	</div>
	<div class="icon">
		<i class="fa fa-map-marker"></i>
		<p>
			<span>12 ABC Street</span>BRISBANE, AUSTRALIA
		</p>


		<div>
			<i class="fa fa-phone"></i>
			<p>+64 1234 56789</p>
		</div>

		<div>
			<i class="fa fa-envelope"></i>
			<p>community@admin.com</p>
		</div>
	</div>
	<h5>&copy; copyright&nbsp;&#9615; WEBSITE BY community82 2016</h5>
</footer>
</body>


</html>
