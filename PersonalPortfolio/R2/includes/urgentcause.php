<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: ugentcause.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: main page for notice events 
--------------------------------------------------------->
<div class="container">

	<div class="row box ">


		<div class="col-md-offset-1 col-md-2  ">
			<h3 class="ugent">
				<u style="color: #FF5733;"><span style="color: #000;"><strong>U</strong></span></u>rgent
				Cause
			</h3>
			<img src="../img/urgent1.jpg" alt="urgent" style="width: 300px;"
				class="img-responsive">
		</div>

		<div class="col-md-4" style="margin-top: 25px;">

          <?php
										$sql = "SELECT * FROM donation ORDER BY donationID DESC LIMIT 1";
										$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
										while ( $row = mysqli_fetch_array ( $result ) ) {
											$donationID = $row ['donationID'];
											
										
										}	$sql = "SELECT *  FROM events  WHERE donationID =' $donationID '";
											$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
											$row = mysqli_fetch_array ( $result );
											while ( $row = mysqli_fetch_array ( $result ) ) 

											{
												$eventID = $row ['eventID'];
												
												echo '<h4><strong ">' . $row ['title'] . '</strong></h4>';
												echo '<span style="color:#FF5733;font-size:2rem;">' . (substr ( ($row ['content']), 0, 50 )) . '...</span>';
											}
										?>
        <div class="table-responsive">
				<table class="table">
					<thead>
					
					
					<tbody>
  <?php
		$sql = "SELECT SUM(donationMoney) , goalMoney FROM fundedmoney, donation WHERE fundedmoney.donationID= $donationID";
		$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
		
		while ( $row = mysqli_fetch_assoc ( $result ) ) {
			
			$percent = $row ['SUM(donationMoney)'] / $row ['goalMoney'] * 100;
			
			echo "<tr><th> Donaters - </th><td>3</td></tr>";
			echo "<tr><th> Goal -</th><td> $" . $row ['goalMoney'] . '</td></tr>';
			echo "<tr><th> Raised -</th><td> $" . $row ['SUM(donationMoney)'] . '</td></tr> </thead></tbody></table>
       </div>';
			
			echo '<div class="rating"><div class="graphcont"><div class="graph"><strong class="bar" style="width:' . round ( $percent ) . '%;"><span class="one">' . round ( $percent ) . '%</strong></span></div></div></div>';
		}
		
		?>


<div class="col-md-6">
							<a href="donation.php?eventID=<?php echo $eventID;?>">
								<button type="button" class="btn btn-default boxbtn"
									style="background-color: #0066FF; border: 3px solid #0066FF; color: #fff; margin-top: 5px;"
									name="donate">DONATION</button>
							</a>
						</div>
						<div class="col-md-6">
							<a href="eventpost.php?eventID=<?php  echo $eventID?>">
								<button type="button" class="btn btn-default boxbtn"
									style="border: 3px solid #0066FF; color: #0066FF; margin-top: 5px;">READ
									MORE</button>
							</a>
						</div>



					</tbody>
				</table>
			</div>


			<div class="col-md-4  ">
    <?php
				$sql = "SELECT * FROM events WHERE eventID =4";
				$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
				$row = mysqli_fetch_array ( $result );
				$pageTitle = $row ['title'];
				$content = $row ['content'];
				$venue = $row ['venue'];
				$eventdate = $row ['eventdate'];
				$eventimg = $row ['eventimg'];
				
				$sql = "SELECT * FROM events WHERE eventID =5";
				$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
				$row = mysqli_fetch_array ( $result );
				$pageTitle1 = $row ['title'];
				$content1 = $row ['content'];
				$venue1 = $row ['venue'];
				$eventdate1 = $row ['eventdate'];
				$eventimg1 = $row ['eventimg'];
				
				?>    
    
  <h3 class="ugent">
					<u style="color: #FF5733;"><span style="color: #000;"> <strong>U</strong></span></u>pcoming
					events
				</h3>
				<div class="media">
					<div class="media-left media-middle">
						<a href="eventpost.php?eventID=4">   <?php echo "<img class='media-object'  src=  '../img/" . $eventimg. "'" . " width='100px'height='66px' >";?></a>
					</div>
					<div class="media-body">
						<h5 class="media-heading ">
							<strong><?php echo $pageTitle;?></strong> <span
								class="pull-right eventdatebtn2"><?php
								$source = $eventdate;
								$date = new DateTime ( $source );
								echo $date->format ( 'd M' );
								echo "</button>";
								
								?>
						
						
						
						</h5>
						</span>
						<h5><?php echo $venue;?></h5>
					</div>
				</div>
				<hr margin-top='2px'>






				<div class="media">
					<div class="media-left media-middle">
						<a href="eventpost.php?eventID=5">   <?php echo "<img class='media-object'  src=  '../img/" . $eventimg1. "'" . " width='100px'height='66px' >";?></a>
					</div>
					<div class="media-body">
						<h5 class="media-heading ">
							<strong><?php echo $pageTitle1;?></strong> <span
								class="pull-right eventdatebtn2"><?php
								$source = $eventdate1;
								$date = new DateTime ( $source );
								echo $date->format ( 'd M' );
								echo "</button>";
								
								?>
						
						
						
						</h5>
						</span>
						<h5><?php echo $venue1;?></h5>
					</div>
				</div>
				<hr margin-top='2px'>

			</div>
		</div>
		<br>



		<div class="container text-center bg-grey ">
			<div class='row'>
				<div class="col-md-4 ">


					<div class="thumbnail">
						<p class=" text-center">
							<strong><span style="font-size: 1.8em;">Donation</span></strong><br>
							WE MAKE A LIVING BY WHAT WE GET,<br> BUT WE MAKE A LIFE BY WHAT
							WE GIVE.
						</p>
						<img src="../img/donation1.jpg" alt="Paris" w>

					</div>
					<a href="event.php"><button type="button"
							class="btn btn-default boxbtn">
							<span class="glyphicon glyphicon-triangle-right"
								aria-hidden="true"></span>DONATE NOW
						</button></a>
				</div>
				<div class="col-md-4 ">

					<div class="thumbnail" style="background-color: #FF5733;">
						<p>
							<strong><span style="font-size: 1.8em;">Volunteer</span></strong><br>
							"ALONE WE CAN DO SO LITTE;<br>TOGETHER WE CAN DO SO MUCH"
						</p>
						<img src="../img/donation1.jpg" alt="Paris">

					</div>
					<a href="volregi.php"><button type="button"
							class="btn btn-default boxbtn"
							style="background-color: #000; border: #000; color: #fff;">
							<span class="glyphicon glyphicon-triangle-right"
								aria-hidden="true"></span>JOIN NOW
						</button> </a>
				</div>
				<div class="col-md-4">

					<div class="thumbnail">
						<p>
							<strong><span style="font-size: 1.8em;">Event</span></strong><br>
							CHECK UPCOMING<br>EVENTS
						</p>
						<img src="../img/donation1.jpg" alt="Paris">

					</div>
					<a href="event.php"><button type="button"
							class="btn btn-default boxbtn">
							<span class="glyphicon glyphicon-triangle-right"
								aria-hidden="true"></span>READ MORE
						</button></a>
				</div>

			</div>


		</div>