<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: donation.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Updates:

Link for notifications and navigation movement

---------------------------------------------------------

Description of the page:navigation for member/volunteer /visitor
--------------------------------------------------------->
<nav class="navbar navbar-fixed-top">

	<div class="container-fluid" style="padding-left: 0">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>


		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-left">

				<li class="joincolor" style="padding-left: 0"><a
					href="../pages/regichoice.php"> Join us</a></li>
				<li><a <?php if($page=='Home'){echo "class='active'";} ?>
					href="index.php">Home</a></li>
				<li><a <?php if($page=='About'){echo "class='current'";} ?> href="about.php">About</a></li>
				<li><a <?php if($page=='Event'){echo "class='current'";} ?>
					href="event.php">Events</a></li>
				<li><a <?php if($page=='Volunteer'){echo "class='current'";} ?>
					href="volregi.php">Volunteer</a></li>
				<li><a <?php if($page=='Shop'){echo "class='current'";} ?>
					href="shop.php">Shop</a></li>

				<li><a <?php if($page=='Contact'){echo "class='current'";} ?>
					href="contact.php">Contact</a></li>

			</ul>



<?php
if ((isset ( $_SESSION ['member'] ))) // check to see if a member or admin is logged in and, if so, display the logged in menu items
{
	?>
<ul class="nav navbar-nav navbar-right">

				<li><a <?php if($page=='memberaccount'){echo "class='current'";} ?>
					href="memberlanding.php">My Account</a></li>
                <li><a href="../pages/notifications.php">&#8226</a></li>
				<li><a href="logout.php">Logout</a></li>
				




<?php
} elseif ((isset ( $_SESSION ['volunteer'] ))) // if a member or admin is not logged in display the not logged in menu items
{
	?>
<ul class="nav navbar-nav navbar-right">

					<li><a
						<?php if($page=='volunteeraccount'){echo "class='current'";} ?>
						href="vollogined.php">My Account</a></li>
                    <li><a href="../pages/notifications.php">&#8226</a></li>
					<li><a href="logout.php">Logout</a></li>
				


					

<?php
}
?>
<ul class="nav navbar-nav navbar-right clander">

						<li><a href="#"><i class="fa fa-calendar fa-lg"
								style="color: #fff; float: left;"></i></a>
						
						<li>
					
					</ul>
				</ul>
    
            </ul>
		
		</div>
    </div>

</nav>

<script type ="text/javascript">
$(document).ready(function() {
	var stickyNavTop = $('nav').offset().top;

	var stickyNav = function(){
	var scrollTop = $(window).scrollTop();

	if (scrollTop > stickyNavTop) { 
	    $('nav').addClass('sticky');
	} else {
	    $('nav').removeClass('sticky'); 
	}
	};

	stickyNav();

	$(window).scroll(function() {
	    stickyNav();
	});
	});
</script>
