<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: imageslide.php
File Version: 1.01 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: pcarousel slide in index page
--------------------------------------------------------->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		<li data-target="#myCarousel" data-slide-to="1"></li>
	</ol>
    <?php
				$sql = "SELECT * FROM events WHERE eventID =6";
				$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
				$row = mysqli_fetch_array ( $result );
				$pageTitle = $row ['title'];
				
				?>
    <!-- Wrapper for slides -->
	<div class="carousel-inner" role="listbox">
		<div class="item active">
			<img src="../img/ca1.jpg" alt="Image" width="800" height="400">
			<div class="carousel-caption">
				<div class="jumbotron text-left">
					<h1>Don't turn away</h1>
					<p>
						DONATION NEEDED:<BR>Infrastructure & Water Facilities to Poor
						Children in India
					</p>
					<div class="donatebtn">
						<a class="btn btn-default" href="../pages/event.php" role="button">DONATE
							TODAY</a>
					</div>
				</div>
			</div>
		</div>

		<div class="item">
			<img src=" ../img/ca2.jpg" alt="Image">
			<div class="carousel-caption">
				<h3>Event</h3>
				<p></p>
				<div class="jumbotron text-left">
					<h2><?php echo $pageTitle;?></h2>
					<p>
					
					
					<div class="clock"></div>
					<div class="donatebtn">
						<a class="btn btn-info" href="../pages/eventpost.php?eventID=6"
							role="button">Detail</a>
					</div>


					<script type="text/javascript">
var date = new Date("November 22, 2016 14:00:00"); //Month Days, Year HH:MM:SS
var now = new Date();
var diff = (date.getTime()/1000) - (now.getTime()/1000);

var clock = $('.clock').FlipClock(diff,{
    clockFace: 'DailyCounter',
    countdown: true
});

</script>


				</div>
			</div>
		</div>

		<!-- Left and right controls -->
		<a class="left carousel-control" href="#myCarousel" role="button"
			data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"
			aria-hidden="true"></span> <span class="sr-only">Previous</span>
		</a> <a class="right carousel-control" href="#myCarousel"
			role="button" data-slide="next"> <span
			class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	</div>


	<!-- Add the extra clearfix for only the required viewport -->
	<div class="clearfix visible-xs-block"></div>
</div>
