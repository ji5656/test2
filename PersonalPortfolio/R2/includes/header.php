<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: header.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page:header include link JS CSS chart JQuriy countdown JS fonts
--------------------------------------------------------->
<!DOCTYPE html>
<?php
session_start ()?>
<head>
<meta charset="utf-8">
<title><?php echo $page ?></title>

<!-- link to favicon -->
<link href="../images/favicon.ico" rel="shortcut icon" />
<!-- link to external CSS -->
<!-- bootstrap-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../css/graph.css">
<link rel="stylesheet" href="../css/main.css">

<!-- morris chart -->
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.1.min.js"></script>

<!-- flip clock for countdown -->
<link rel="stylesheet" href="../css/flipclock.css">

<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script src="../css/flipclock.js"></script>

<!-- enable HTML5 in IE 8 and below -->
<!--[if IE]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->
<!-- font -->
<link href='http://fonts.googleapis.com/css?family=Ubuntu'
	rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Roboto"
	rel="stylesheet">


</head>
<body>
	<div class="row">
		<div class=" col-md-4">
			<div class="socialgroup">
				<ul>
					<li><a href="https://www.facebook.com/community82/"><i
							class="fa fa-facebook"></i></a></li>
					<li><a href="https://twitter.com/community82qut"><i
							class="fa fa-twitter"></i></a></li>
					<li><a
						href="https://www.linkedin.com/in/community-qut-9b4816125?trk=hp-identity-name"><i
							class="fa fa-linkedin"></i></a></li>
					<li><a href="https://plus.google.com/107687660841293451632"><i
							class="fa fa-google-plus"></i> </a></li>
				</ul>

			</div>
		</div>
		<div class=" col-md-8">
			<div class="contactinfo">

				<ul>
					<li><span class="glyphicon glyphicon-phone"></span> 12345 6789</li>
					<li><span class="glyphicon glyphicon glyphicon-envelope"></span>community@admin.com</li>
  <?php
		if ((isset ( $_SESSION ['member'] ))) // check to see if a member or admin is logged in and, if so, display the logged in menu items
{
			
			echo "<span class='name'><li>" . "Welcome&nbsp&nbsp" . ($_SESSION ['member']) . " </li></span>";
			
			echo "<li><a href='../pages/cart.php'>cart</a></li>";
		} 

		else {
			if ((isset ( $_SESSION ['volunteer'] ))) {
				echo "<span class='name'><li>" . "Welcome&nbsp&nbsp" . ($_SESSION ['volunteer']) . " </li></span>";
			} else {
				echo "<li><a href='../pages/login.php'><span class='glyphicon glyphicon-user'></span>  Login</a></li>";
			}
		}
		?>
</ul>
			</div>
		</div>
	</div>