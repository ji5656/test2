<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: eventresult.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: Page for president to check event process or result.
--------------------------------------------------------->


<?php
session_start ();
?>

<?php
$page = " Update events";
include '../includes/connect.php';
include 'allheader.php'; // includes a session_start()

if ($_SESSION ['type'] == 4) {
	
	include 'prenav.php';
} else {
	
	include 'adminnav.php';
}
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Event Analytics</h1>
	<div class="row">

<?php
$eventID = $_GET ['eventID'];
$sql = "SELECT * FROM events, eventincome,eventexpense WHERE events.eventID = '$eventID' and eventincome.type = 1 and eventexpense.type=1 ";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) {
	$RaffleTickets = $row ['RaffleTickets'];
	$EntryFees = $row ['EntryFees'];
	$CashDonation = $row ['CashDonation'];
	$GoodsServiceSold = $row ['GoodsServiceSold'];
	$sumincome = $RaffleTickets + $EntryFees + $CashDonation + $GoodsServiceSold;
	$budget = $row ['budget'];
	$goal = $row ['eventGoal'];
	$sumincomeresult = $RaffleTickets + $EntryFees + $CashDonation + $GoodsServiceSold;
	$title = $row ['title'];
	$date = $row ['eventdate'];
}

$sql = "SELECT * FROM eventexpense WHERE eventID = '$eventID' and type=1";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) {
	$venue1 = $row ['venuecost'];
	$FoodCatering1 = $row ['FoodCatering'];
	$AudioVisual1 = $row ['AudioVisual'];
	$ThirdPartyVendors1 = $row ['ThirdPartyVendors'];
	$EventRentals1 = $row ['EventRentals'];
	$marketingRegistration1 = $row ['marketingRegistration'];
	$planningOrganization1 = $row ['planningOrganization'];
	$administrativeExpenses1 = $row ['administrativeExpenses'];
	$totalexpeneresult = $venue1 + $FoodCatering1 + $AudioVisual1 + $ThirdPartyVendors1 + $EventRentals1 + $marketingRegistration1 + $planningOrganization1 + $administrativeExpenses1;
}

$sql = "SELECT * FROM eventincome join events using (eventID) WHERE eventID = '$eventID' and type=0";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) {
	$RaffleTickets = $row ['RaffleTickets'];
	$EntryFees = $row ['EntryFees'];
	$CashDonation = $row ['CashDonation'];
	$GoodsServiceSold = $row ['GoodsServiceSold'];
	$sumincome = $RaffleTickets + $EntryFees + $CashDonation + $GoodsServiceSold;
}

$sql = "SELECT * FROM eventincome WHERE eventID = '$eventID' and type=1";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) {
	$RaffleTickets1 = $row ['RaffleTickets'];
	$EntryFees1 = $row ['EntryFees'];
	$CashDonation1 = $row ['CashDonation'];
	$GoodsServiceSold1 = $row ['GoodsServiceSold'];
	$sumincome1 = $RaffleTickets1 + $EntryFees1 + $CashDonation1 + $GoodsServiceSold1;
}

$sql = "SELECT * FROM eventexpense join events using (eventID) WHERE eventID = '$eventID' and type=0";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) {
	$venue = $row ['venuecost'];
	$FoodCatering = $row ['FoodCatering'];
	$AudioVisual = $row ['AudioVisual'];
	$ThirdPartyVendors = $row ['ThirdPartyVendors'];
	$EventRentals = $row ['EventRentals'];
	$marketingRegistration = $row ['marketingRegistration'];
	$planningOrganization = $row ['planningOrganization'];
	$administrativeExpenses = $row ['administrativeExpenses'];
	$totalexpene = $venue + $FoodCatering + $AudioVisual + $ThirdPartyVendors + $EventRentals + $marketingRegistration + $planningOrganization + $administrativeExpenses;
}

$sql = "SELECT memberattendee.*,member.* FROM memberattendee ,member
	WHERE member.memberID= memberattendee.memberID AND memberattendee.eventID =$eventID";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
$total_member = mysqli_num_rows ( $result );

$todate = date ( "Y-m-d", time () );
$ddy = (strtotime ( $date ) - strtotime ( $todate )) / 86400;
$temp = floor ( $ddy );
echo "<h3>D-Day :$temp day(s) left </h3>";

echo "<div class= 'col-md-12'>";

echo "<h2 class='text-center'>$title</h2></div>";

echo "<div class='row'><div class='col-md-4'> <div class='panel panel-success'><div class='panel-heading'>
	<div class='panel-body'> <h4>Event Goal $$goal</h4></div></div></div>";

echo "<table class ='table'>";

echo "<tr><td>Budget</td> <td> $$budget</td></tr>";

echo "<tr><td >TOTAL INCOME</td> <td> $$sumincomeresult</td></tr>";
echo "<tr><td>TOTAL EXPENDITURES</td> <td> $$totalexpeneresult</td></tr>";
$profit = $sumincomeresult - $totalexpeneresult;
echo "<tr><td>Profit</td> <td> $$profit</td></tr></table></div>";

echo '"<div class="col-md-2">"<h4>Total Attendee :  ' . $total_member . '</h4>';
echo '<h4>Total volunteer :  ' . $total_member . '</h4></div>';
echo '<div class="col-md-6"><div id="bar-example"></div>';
?>
</div>



</div>
<div class="row">


	<div class="col-md-4">

		<div class="panel panel-info">
			<div class="panel-heading">
				<h3>Estimated income plan</h3>
			</div>
			<div class="panel-body">
				<h3>Target: $ <?php echo "$sumincome"; ?></h3>
				<div id="incomeplan"></div>
			</div>
		</div>

	</div>


	<div class="col-md-8">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3>Compare Income Plan and Result</h3>
			</div>
			<div class="panel-body">
				<div id="line-example"></div>
			</div>
		</div>
	</div>
</div>


<div class="row">
	<div class="col-md-4">

		<div class="panel panel-warning">
			<div class="panel-heading">
				<h3>Estimated expense plan</h3>
			</div>
			<div class="panel-body">
				<h3>Total: $ <?php echo "$totalexpeneresult"; ?></h3>
				<div id="expenseplan"></div>
			</div>
		</div>


	</div>

	<div class="col-md-8">
		<div class="panel panel-warning">
			<div class="panel-heading">
				<h3>Compare Expense Plan and Result</h3>
			</div>

			<div id="expenseResult"></div>
		</div>
	</div>
</div>



<script>

Morris.Donut({
  element: 'expenseplan',
 
  data: [
	 
    {label: "venue", value: <?php echo $venue ?>},
    {label: "FoodCatering", value: <?php echo $FoodCatering?>},
    {label: "Audio Visual", value: <?php echo $AudioVisual ?>},
    {label: "Third PartyVendors", value: <?php echo $ThirdPartyVendors ?>},
    {label: "Event Rentals", value: <?php echo $EventRentals ?>},
    {label: "Administrative Expenses", value: <?php echo $administrativeExpenses ?>},
    {label: "Planning Organization", value: <?php echo $planningOrganization ?>},
    {label: "marketing /Registration", value: <?php echo $marketingRegistration ?>},
  

    
  ]
});
Morris.Line({
	  element: 'expenseResult',
	  parseTime: false,
	  lineColors: ['#ff4400', '#22aa22'],
	  data: [
	    { y: 'venue', a: <?php echo $venue ?>, b: <?php echo $venue1 ?> },
	    { y: 'FoodCatering', a: <?php echo $FoodCatering?>,  b:<?php echo $FoodCatering1 ?> },
	    { y: 'Audio Visual', a:  <?php echo $AudioVisual ?>,  b: <?php echo $AudioVisual1 ?> },
	    { y: 'Third PartyVendors', a: <?php echo $EventRentals ?>,  b:<?php echo $EventRentals1 ?> },
	    {y: "Administrative Expenses",a: <?php echo $administrativeExpenses ?>, b:<?php echo $administrativeExpenses1 ?>},
	    {y: "Planning Organization", a: <?php echo $planningOrganization ?>, b: <?php echo $planningOrganization1 ?>},
	    {y: "marketing /Registration", a: <?php echo $marketingRegistration ?>, b: <?php echo $marketingRegistration1 ?>},
	
	  ],
	  xkey: 'y',
	  ykeys: ['a', 'b'],
	  labels: ['PLAN', 'RESULT']
	 
	});

		Morris.Donut({
			
			  element: 'incomeplan',
			  colors: [
				    '#0BA462',
				    '#39B580',
				    '#67C69D',
				    '#95D7BB'
				  ],
			  data: [
				 
			    {label: "RaffleTickets", value: <?php echo $RaffleTickets ?>},
			    {label: "EntryFees", value: <?php echo $EntryFees?>},
			    {label: "CashDonation", value: <?php echo $CashDonation ?>},
			    {label: "Goods/Service sales", value: <?php echo $GoodsServiceSold ?>},
			   

			    
			  ] 
			});

		

		Morris.Line({
		  element: 'line-example',
		  parseTime: false,
		  lineColors: ['#ff4400', '#22aa22'],
		  data: [
		    { y: 'RaffleTickets', a: <?php echo $RaffleTickets ?>, b: <?php echo $RaffleTickets1 ?> },
		    { y: 'EntryFees', a: <?php echo $EntryFees?>,  b:<?php echo $EntryFees1 ?> },
		    { y: 'CashDonation', a: <?php echo $CashDonation ?>,  b:<?php echo $CashDonation1 ?> },
		    { y: 'GoodsServiceSold', a: <?php echo $GoodsServiceSold ?>,  b: <?php echo $GoodsServiceSold1 ?> },
		
		  ],
		  xkey: 'y',
		  ykeys: ['a', 'b'],
		  labels: ['PLAN', 'RESULT']
		 
		});
		
		 Morris.Bar({
			  element: 'bar-example',
			  parseTime: false,
			  barColors: [
				   
				    '#ff4400', '#0BA462'
				    
				  ],
			  data: [
			    { y: 'GOAL',a: <?php echo $goal ?>, b: <?php echo $profit ?>},
			    { y: 'INCOME', a: <?php echo $sumincome ?>,  b:<?php echo $sumincome1 ?> },
			    { y: 'EXPENDITURES', a: <?php echo $totalexpene ?>,  b:<?php echo $totalexpeneresult ?> },
			   
			  
			  ],
			  xkey: 'y',
			  ykeys: ['a', 'b'],
			  labels: ['Plan', 'Result']
			});
</script>





</div>

