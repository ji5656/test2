<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: index.php(admin directory)
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------
Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Formatting page.

---------------------------------------------------------

Description of the page: Index page for admin.
--------------------------------------------------------->

<?php
session_start ();
$page = "president- Home";
include '../includes/connect.php';
include 'allheader.php';
include 'prenav.php';
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Dashboard</h1>
			<?php
			$sql = "SELECT (SELECT COUNT(*) FROM events) AS 'totalevents', (SELECT
SUM(donationMoney) FROM fundedmoney) AS 'totalfunding', (SELECT COUNT(*) FROM member) AS
'totalmembers' ";
			$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
			
			if ($row = mysqli_fetch_array ( $result )) {
				
				echo '<div class="row">
					<div class="col-md-6">

						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">Total Events</h3>';
				echo '<h3>' . $row ['totalevents'] . '</h3></div></div></div>';
				
				echo '<div class="col-md-6">
	        
						<div class="panel panel-info">
							<div class="panel-heading">
								<h3 class="panel-title">Total MEMBER</h3>';
				echo '<h3>' . $row ['totalmembers'] . '</h3></div></div></div>
				
			
		</div>';
				
				echo '<div class="row">
					<div class="col-md-6">
	    
						<div class="panel panel-danger">
							<div class="panel-heading">
								<h3 class="panel-title">Total funding</h3>';
				echo '<h3>' . $row ['totalfunding'] . '</h3></div></div></div>';
				
				echo '					<div class="col-md-6">
	    
						<div class="panel panel-success">
							<div class="panel-heading">
								<h3 class="panel-title">Total funding</h3>';
				echo '<h3>' . $row ['totalfunding'] . '</h3></div></div></div></div>
		';
			}
			
			?>
</div>



