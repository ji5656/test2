<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: eventmemberlist.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: Page for admin to manage event cost.
--------------------------------------------------------->
<?php
session_start ();
?>

<?php
$page = " Update events";
include '../includes/connect.php';
include 'allheader.php'; // includes a session_start()

if ($_SESSION ['type'] == 4) {
	
	include 'prenav.php';
} else {
	
	include 'adminnav.php';
}
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Event Attendee List</h1>
	
<?php
$eventID = $_GET ['eventID'];
$sql = "SELECT * FROM events WHERE eventID = '$eventID' ";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) {
	$title = $row ['title'];
	
	echo "<h1 class='text-info'> $title</h1>";
}
?>
	<table class="table">
		<thead>
			<tr>
				<th>userID</th>
				<th>username</th>
				<th>firstname</th>
				<th>lastname</th>
				<th>contactnumber</th>
				<th>email</th>

			</tr>
		</thead>


		<tbody>
<?php
$eventID = $_GET ['eventID'];
$sql = "SELECT memberattendee.*,member.* FROM memberattendee ,member 
 WHERE member.memberID= memberattendee.memberID AND memberattendee.eventID =$eventID";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
$row_count = mysqli_num_rows ( $result );
echo '<h3>Total Attendee :  ' . $row_count . '</3>';
while ( $row = mysqli_fetch_array ( $result ) ) {
	
	echo "<td>" . $row ['memberID'] . "</td><td>" . $row ['username'] . " </td>";
	echo "<td>" . $row ['firstname'] . "</td><td>" . $row ['lastname'] . "</td>";
	echo "<td>" . $row ['mobile'] . "</td>";
	echo "<td>" . $row ['email'] . "</td>";
	echo "</tr>";
}

?>

	
	</table>