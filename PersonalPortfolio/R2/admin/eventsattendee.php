<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: membermanage.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: Page for admin to manage event attending member.
--------------------------------------------------------->
<?php
session_start ();
?>

<?php
$page = "eventanalytic";
include '../includes/connect.php';
include 'allheader.php'; // includes a session_start()

if ($_SESSION ['type'] == 4) {
	
	include 'prenav.php';
} else {
	
	include 'adminnav.php';
}
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">EVENTS</h1>
	<table class="table">
		<thead>
			<tr>
				<th>EvnetID</th>
				<th>Title</th>
				<th>Description</th>
				<th>Date</th>
				<th>Venue</th>
				<th>Budget</th>
				<th>attendee</th>
									<?php
									if ($_SESSION ['type'] == 4) {
										
										echo "<th>Result(president)</th>";
									}
									?>
								</tr>
		</thead>
							
	
	<?php
	$sql = "SELECT events.* From events ";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
	$numrow = mysqli_num_rows ( $result ); // retrieve the number of rows
	echo "<p>There are currently <strong>" . $numrow . "</strong>
	events.</p>";
	
	while ( $row = mysqli_fetch_array ( $result ) ) {
		$eventID = $row ['eventID'];
		echo "<tr>";
		echo "<td>" . $row ['eventID'] . "</td>";
		echo "<td>" . $row ['title'] . "</td>";
		echo "<td>" . (substr ( ($row ['content']), 0, 50 )) . "...</td>";
		echo "<td>" . date ( "d/m/y H", strtotime ( $row ['eventdate'] ) ) . "</td>";
		echo "<td>" . $row ['venue'] . "</td>";
		echo "<td><a href=\"eventcost.php?eventID={$row['eventID']}\">View</a></td>";
		
		echo "<td><a href=\"eventmemberlist.php?eventID={$row['eventID']}\">View</a></td> ";
		
		if ($_SESSION ['type'] == 4) {
			
			echo "<td><a href=\"eventresult.php?eventID={$row['eventID']}\">View</a></td> ";
		}
		
		echo "</tr>";
	}
	
	?>
</table>



	<div class="modal fade myModal" tabindex="-1" role="dialog"
		aria-labelledby="myLargeModalLabel">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div class="row">
						<div id="donut-example"></div>

					</div>
					<h4 class="modal-title">Modal title</h4>
				</div>
				<div class="modal-body">
					<p>One fine body&hellip;</p>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Save changes</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<script>
 Morris.Donut({
   element: 'donut-example',
   data: [
     {label: "Download Sales", value: 12},
     {label: "In-Store Sales", value: 30},
     {label: "Mail-Order Sales", value: 20}
   ]
 });

    	  </script> 



<?php
// user messages
if (isset ( $_SESSION ['msg'] )) // if session error is set
{
	echo '<div class="msg">';
	echo '<h3 class ="text-danger">' . $_SESSION ['msg'] . '</h3>'; // display error message
	echo '</div>';
	unset ( $_SESSION ['msg'] ); // unset session error
}

?>


	







