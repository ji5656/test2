<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: memberupdateprocessing.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------


Description of the page: Page for admin to manage member such as update or delete processing.
--------------------------------------------------------->
<?php
session_start ();
include "../includes/connect.php";
?>
<?php

$memberID = $_POST ['memberID']; // retrieve reviewID from hidden form field

$contactnumber = mysqli_real_escape_string ( $con, $_POST ['contactnumber'] ); // prevent SQL injection
$email = mysqli_real_escape_string ( $con, $_POST ['email'] );

$sql = "UPDATE member SET mobile='$contactnumber', email='$email' WHERE memberID ='$memberID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
if ($result == 1) {
	$_SESSION ['msg'] = 'Member ID ' . $memberID . ' updated successfully'; // if member update is successful intialise a session called 'success' with a msg
	header ( "location:membermanage.php" ); // redirect
} else {
	$_SESSION ['msg'] = 'member ID ' . $memberID . ' updated failed';
}
?>
