<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: memberupdate.php
File Version: 1.0.3 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------
Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)
Version: 1.0.3 (Ji-Young Choi)

Formatting page.

---------------------------------------------------------

Description of the page: Page for admin to Update member's information:contact number, email
--------------------------------------------------------->
<?php
session_start ();
?>
<?php

$page = "update member";
include '../includes/connect.php';
include '../admin/allheader.php';
include 'adminnav.php';

?>

<?php
$memberID = $_GET ['memberID']; // retrieve reviewID from URL
$sql = "SELECT * FROM member WHERE memberID = '$memberID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
$row = mysqli_fetch_array ( $result );
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1>Update member</h1>
	<div class="row ">
		<div class="col-md-4  offset-md-6 ">
			<form action="memberupdateprocessing.php" method="post">
				<div class="form-group">
					<label>Username*</label> <input class="form-control" type="text"
						name="username" required value="<?php echo $row['username'] ?>"
						readonly />
				</div>

				<div class="form-group">
					<label>First Name*</label> <input class="form-control" type="text"
						name="firstname" required value="<?php echo $row['firstname'] ?>"
						readonly /><br />
				</div>

				<div class="form-group">
					<label>Last Name*</label> <input class="form-control" type="text"
						name="lastname" required value="<?php echo $row['lastname'] ?>"
						readonly /><br />
				</div>

				<div class="form-group">
					<label>contact number</label> <input class="form-control"
						type="text" name="contactnumber" required
						value="<?php echo $row['mobile'] ?>" /><br />
				</div>

				<div class="form-group">
					<label>email</label> <input class="form-control" type="text"
						name="email" required value="<?php echo $row['email'] ?>" /><br />
				</div>

				<div class="form-group">
					<input type="hidden" name="memberID" class="form-control"
						value="<?php echo $memberID; ?>">
					<p>
						<input type="submit" name="memberupdate" value="Update member"
							class="form-control" />
					</p>
				</div>
			</form>
		</div>
	</div>
	<!-- end content -->
</div>


