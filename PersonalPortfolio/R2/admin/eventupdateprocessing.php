<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: eventupdateprocessing.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------
Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)
Version: 1.0.3 (Ji-Young Choi)

Formatting page.

---------------------------------------------------------

Description of the page: function to update events.
--------------------------------------------------------->

<?php
 session_start();
 include "../includes/connect.php";
?>

<?php
 $eventID = $_POST['eventID']; //retrieve reviewID from hidden form field

 $title = mysqli_real_escape_string($con, $_POST['title']); //prevent SQL injection
 $content = mysqli_real_escape_string($con, $_POST['content']);
 $venue = mysqli_real_escape_string($con, $_POST['venue']);
 $eventdate = mysqli_real_escape_string($con, $_POST['eventdate']);
 $adminID = mysqli_real_escape_string($con, $_POST['adminID']);
 $donationID = mysqli_real_escape_string($con, $_POST['donationID']);
 $budget = mysqli_real_escape_string($con, $_POST['budget']);
 


 
 if($_FILES['image']['name']) //if an image has been uploaded
 {
 	$image = $_FILES['image']['name']; //the PHP file upload variable for a file
 	$randomDigit = rand(0000,9999); //generate a random numerical digit <= 4 characters
 	$newImageName = strtolower($randomDigit . "_" . $image); //attach the random digit to the front of uploaded images to prevent overriding files with the same name in the images folder and enhance security
 	$target = "../img/" . $newImageName; //the target for uploaded images
 	$allowedExts = array('jpg', 'jpeg', 'gif', 'png'); //create an array with the allowed file extensions
 	$tmp = explode('.', $_FILES['image']['name']); //split the file name from the file extension
 	$extension = end($tmp); //retrieve the extension of the photo e.g., png
 
 	if($_FILES['image']['size'] > 512000) //image maximum size is 500kb
 	{
 		$_SESSION['error'] = 'Your file size exceeds maximum of 500kb.'; //if file exceeds max size initialise a session called 'error' with a msg
 		header("location:eventupdate.php?eventID=" . $reviewID); //redirect to reviewupdate.php
 		exit();
 	}
 	elseif(($_FILES['image']['type'] == 'image/jpg') || ($_FILES['image']['type']
 			== 'image/jpeg') || ($_FILES['image']['type'] == 'image/gif') ||
 			($_FILES['image']['type'] == 'image/png') && in_array($extension, $allowedExts))
 	{
 		move_uploaded_file($_FILES['image']['tmp_name'], $target); //move the image to images folder
 	}
 	else
 	{
 		$_SESSION['msg'] = 'Only JPG and PNG files allowed.'; //if file uses an invalid extension initialise a session called 'error' with a msg
 		header("location:eventupdate.php?eventID=" . $reviewID); //redirect to reviewupdate.php
 		exit();
 	}
 }
 
 $sql="UPDATE events SET title='$title', content='$content',venue='$venue',eventdate='$eventdate', donationID='$donationID' ,adminID= '$adminID', budget= '$budget' , eventimg='$newImageName' WHERE eventID ='$eventID'";
 $result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query

 echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('update successfully.')
 		 window.location.href='eventmanage.php'
 		</SCRIPT>");
?>
