<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: eventcost.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

Description of the page: Page for admin to manage event cost.
--------------------------------------------------------->
<?php
session_start();
?>

<?php
$page = " Update events";
include '../includes/connect.php';
include 'allheader.php'; // includes a session_start()

if ($_SESSION ['type'] == 4) {
	
	include 'prenav.php';
} else {
	
	include 'adminnav.php';
}
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Event Cost Plan</h1>
 

<?php
$eventID = $_GET ['eventID'];
$sql = "SELECT * FROM events WHERE eventID = '$eventID' ";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) {
	$budget = $row ['budget'];
	$title = $row ['title'];
	echo "<h2>$title</h2>";
	echo "<h1 class=''>Budget: $$budget</h1>";
}
?>
<?php

$sql = "SELECT * FROM eventexpense join events using (eventID) WHERE eventID = '$eventID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) {
	$venue = $row ['venuecost'];
	$FoodCatering = $row ['FoodCatering'];
	$AudioVisual = $row ['AudioVisual'];
	$ThirdPartyVendors = $row ['ThirdPartyVendors'];
	$EventRentals = $row ['EventRentals'];
	$marketingRegistration = $row ['marketingRegistration'];
	$planningOrganization = $row ['planningOrganization'];
	$administrativeExpenses = $row ['administrativeExpenses'];
	$totalexpene = $venue + $FoodCatering + $AudioVisual + $ThirdPartyVendors + $EventRentals + $marketingRegistration + $planningOrganization + $administrativeExpenses;
}
?>

<div class="col-md-6">
		<h3>Total: $ <?php echo "$totalexpene"; ?></h3>
		<div class="panel panel-warning">
			<div class="panel-heading">
				<h3>Estimated expense plan</h3>
			</div>
			<div class="panel-body">
				<div id="expenseplan"></div>
			</div>
		</div>

	</div>
<?php
$sql = "SELECT * FROM eventincome join events using (eventID) WHERE eventID = '$eventID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
while ( $row = mysqli_fetch_array ( $result ) ) {
	$RaffleTickets = $row ['RaffleTickets'];
	$EntryFees = $row ['EntryFees'];
	$CashDonation = $row ['CashDonation'];
	$GoodsServiceSold = $row ['GoodsServiceSold'];
	$sumincome = $RaffleTickets + $EntryFees + $CashDonation + $GoodsServiceSold;
}
?>

<div class="col-md-6">
		<h3>Target: $ <?php echo "$sumincome"; ?></h3>
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3>Estimated income plan</h3>
			</div>
			<div class="panel-body">
				<div id="incomeplan"></div>
			</div>
		</div>






		<script>
Morris.Donut({
  element: 'expenseplan',
 
  data: [
	 
    {label: "venue", value: <?php echo $venue ?>},
    {label: "FoodCatering", value: <?php echo $FoodCatering?>},
    {label: "Audio Visual", value: <?php echo $AudioVisual ?>},
    {label: "Third PartyVendors", value: <?php echo $ThirdPartyVendors ?>},
    {label: "Event Rentals", value: <?php echo $EventRentals ?>},
    {label: "Administrative Expenses", value: <?php echo $administrativeExpenses ?>},
    {label: "Planning Organization", value: <?php echo $planningOrganization ?>},
    {label: "marketing /Registration", value: <?php echo $marketingRegistration ?>},
  

    
  ]
});

		Morris.Donut({
			
			  element: 'incomeplan',
			  colors: [
				    '#0BA462',
				    '#39B580',
				    '#67C69D',
				    '#95D7BB'
				  ],
			  data: [
				 
			    {label: "RaffleTickets", value: <?php echo $RaffleTickets ?>},
			    {label: "EntryFees", value: <?php echo $EntryFees?>},
			    {label: "CashDonation", value: <?php echo $CashDonation ?>},
			    {label: "Goods/Service sales", value: <?php echo $GoodsServiceSold ?>},
			   

			    
			  ] 
			});

</script>