<!-------------------------------------------------------

Subject: IFB299		Group: Group 82
Webpage: eventcontents.php
File Version: 1.0.3 (Release.ConfirmedVersion.CurrentVersion) 
Author: Ji-Young Choi


---------------------------------------------------------
				Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)
Version: 1.0.3 (Ji-Young Choi)
Formatting page.

---------------------------------------------------------

Description of the page: Page to manage the Event for committee member.
--------------------------------------------------------->

<?php
session_start ();

$page = "eventcontents";
include '../includes/connect.php';
include 'allheader.php';
include 'prenav.php';
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Event Contents</h1>
	<table class="table">
		<thead>
			<tr>
				<th>EvnetID</th>
				<th>Title</th>
				<th>Date</th>
				<th>Venue</th>
				<th>Committee name</th>
				<th>Contact info</th>
				<th>attendee</th>
			</tr>
		</thead>
							
<?php
// user messages
if (isset ( $_SESSION ['msg'] )) // if session error is set
{
	echo '<div class="msg">';
	echo '<h3 class ="text-danger">' . $_SESSION ['msg'] . '</h3>'; // display error message
	echo '</div>';
	unset ( $_SESSION ['msg'] ); // unset session error
}
?>

<?php
$sql = "SELECT events.*, admin.firstname, admin.email FROM events JOIN admin USING (adminID)  WHERE events.adminID = admin.adminID";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
$numrow = mysqli_num_rows ( $result ); // retrieve the number of rows
echo "<p>There are currently <strong>" . $numrow . "</strong>
	events.</p>";

while ( $row = mysqli_fetch_array ( $result ) ) {
	echo "<tr>";
	echo "<td>" . $row ['eventID'] . "</td>";
	echo "<td>" . $row ['title'] . "</td>";
	echo "<td>" . (substr ( ($row ['content']), 0, 50 )) . "...</td>";
	echo "<td>" . date ( "d/m/y H", strtotime ( $row ['eventdate'] ) ) . "</td>";
	echo "<td>" . $row ['venue'] . "</td>";
	echo "<td>" . $row ['firstname'] . "</td>";
	echo "<td>" . $row ['email'] . "</td>";
	echo "<td></td>";
	echo "<td></td>";
	echo "<td><a href=\"../eventupdate.php?eventID={$row['eventID']}\">Update</a>  <a
		href=\"../eventsdelete.php?eventID={$row['eventID']}\" onclick=\"return confirm('Are you
		sure you want to delete this event?')\">Delete</a></td>";
	echo "</tr>";
}
echo "</table>";
?>
			</div>
		</div>
		</div>

<?php
// user messages
if (isset ( $_SESSION ['msg'] )) // if session error is set
{
	echo '<div class="msg">';
	echo '<h3 class ="text-danger">' . $_SESSION ['msg'] . '</h3>'; // display error message
	echo '</div>';
	unset ( $_SESSION ['msg'] ); // unset session error
}

?>

