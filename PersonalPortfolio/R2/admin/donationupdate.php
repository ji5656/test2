<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: donationupdate.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------

---------------------------------------------------------

Description of the page: Page to update event by committee member only.
--------------------------------------------------------->
<?php session_start()
?>

<?php
  $page = "Update events";
  include '../includes/connect.php';
  include 'allheader.php'; //includes a session_start()
  include '../admin/comnav.php';
  
?>
<?php 
	if (isset ( $_POST ['submit'] )) {
		$donationID = $_POST['donationID']; //retrieve reviewID from hidden form field
		
		$donationTitle = mysqli_real_escape_string($con, $_POST['donationTitle']); //prevent SQL injection
		$goalMoney = mysqli_real_escape_string($con, $_POST['goalMoney']);
		
		
		$sql="UPDATE donation SET donationTitle='$donationTitle', goalMoney='$goalMoney'WHERE donationID ='$donationID'";
		$result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query
	
		echo ("<SCRIPT LANGUAGE='JavaScript'>window.alert('succeesfully update donation infomation')
        window.location.href='donation.php'
        </SCRIPT>");
		}
	
	
		
	else {
		?>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Donation Detail</h1>
				
<?php
$sql = "SELECT *,(SELECT COUNT(*) FROM member) AS 'totalmember' FROM fundedmoney,donation,member WHERE donation.donationID =" . $_GET['donationID'];
  $result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query
  $row = mysqli_fetch_array($result);
 
?>
  <div class="container">
   
    <div class="row">
      <div class ="col-md-6">
        <form action="" method="post">

          <div class="form-group">
            <label>Title*</label> <input  class="form-control" type="text" name="donationTitle" required value="<?php
            echo $row['donationTitle'] ?>" /><br />
          </div>

         <div class="form-group">
            <label>Goal Money*</label> <input  class="form-control" type="text" name="goalMoney" required value="<?php
            echo $row['goalMoney'] ?>" /><br />
          </div>


          <div class="form-group">
            <input type="hidden" name="donationID" value="<?php echo $row['donationID']; ?>">
          </div>

            <input type="submit"  class="form-control" name="submit" />

        </form>
      </div>

        <div class="col-md-6">
          <h2>current Funding</h2>
          <table class="table">
							<thead>
								<tr>
								<th>fundedMoney</th>
								<th>pecentage</th>
								<th>total Member</th>
								<th>needed each member contribution</th>
								</tr>
							</thead>
		
				<?php 
				$percent = $row['donationMoney']/$row['goalMoney']*100 ;
				$per =( $row['goalMoney']-$row['donationMoney'])/$row['totalmember'] ;
				
			echo "<td> $" . $row['donationMoney'] . "</td>";	
			echo "<td>" . round($percent) . "%</td>";
			
			echo "<td>" .$row['totalmember'] . "</td>";
			echo "<td> $ " .round($per) . "/ per</td>";
			?>		
					</table>
      </div>
    </div>
  </div></div>
<?php }?>
