<!-------------------------------------------------------

Subject: IFB299		Group: Group 82
Webpage: commitindex.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion) 
Author: Ji-Young Choi


---------------------------------------------------------
				Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)

Formatting page.

---------------------------------------------------------

Description of the page: Page to donation.
--------------------------------------------------------->
<?php session_start()
?>
<?php
 $page = "donationmanage";
 include '../includes/connect.php';
 include 'allheader.php';
 include 'comnav.php';
?>
	
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header">Donation</h1>
						<table class="table">
							<thead>
								<tr>
									<th>dontionID</th>
									<th>Title</th>
									<th>Goalmoney</th>
									<th>Date</th>
									
									<th>Detail</th>
								</tr>
							</thead>
							


<?php
	$sql = "SELECT donation.*,  events.title ,events.eventdate FROM donation JOIN events USING (eventID)  WHERE events.eventID = donation.eventID";
	$result = mysqli_query($con, $sql) or die(mysqli_error($con));
	$numrow = mysqli_num_rows($result); //retrieve the number of rows
	echo "<p>There are currently <strong>" . $numrow . "</strong>
	events.</p>";

 	while ($row = mysqli_fetch_array($result)){
 		echo "<tr>";
 	
 		echo "<td>" . $row['donationID'] . "</td>";
 		echo "<td>" . $row['title'] . "</td>";
 		echo "<td>" . $row['goalMoney']   . "</td>";
 		echo "<td>" . date("d/m/y H",strtotime($row['eventdate'])) . "</td>";
 	
 	
 		
 		
 		echo "<td><a href=\"donationupdate.php?donationID={$row['donationID']}\">Modify</a> ";
 		echo "</tr>";
 	}
 	
 ?>
 			</table>

<?php
//user messages
	if(isset($_SESSION['msg'])) //if session error is set
	{
		echo '<div class="msg">';
		echo '<h3 class ="text-danger">' . $_SESSION['msg'] . '</h3>'; //display error message
		echo '</div>';
		unset($_SESSION['msg']); //unset session error
	}

?>


