<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: volmanage.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------


---------------------------------------------------------

Description of the page: Page for admin to manage volunteer such as update or delete.
--------------------------------------------------------->
<?php
session_start ();
?>

<?php
$page = "volmember";
include '../includes/connect.php';
include 'allheader.php'; // includes a session_start()

if ($_SESSION ['type'] == 4) {
	
	include 'prenav.php';
} else {
	
	include 'adminnav.php';
}
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Volunteer administraion</h1>


	<table class="table">
		<thead>
			<tr>
				<th>userID</th>
				<th>username</th>
				<th>firstname</th>
				<th>lastname</th>
				<th>contactnumber</th>
				<th>joindate</th>
				<th>email</th>
				<th>modifying</th>
				<th>Applied delete accoout</th>
			</tr>
		</thead>


		<tbody>
 						<?php
							$sql = "SELECT * FROM volunteer ";
							$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
							$row_count = mysqli_num_rows ( $result );
							echo 'Total Volunteer :  ' . $row_count;
							
							while ( $row = mysqli_fetch_array ( $result ) ) {
								if ($row ['voldelete'] == 1) {
									$memberdelete = 'yes';
								} else {
									$memberdelete = 'no';
								}
								
								echo "<tr >";
								echo "<td>" . $row ['volunteerID'] . "</td><td>" . $row ['username'] . " </td>";
								echo "<td>" . $row ['firstname'] . "</td><td>" . $row ['lastname'] . "</td>";
								echo "<td>" . $row ['mobile'] . "</td><td>" . date ( "Y-m-d ", strtotime ( $row ['date'] ) ) . "</td>";
								echo "<td>" . $row ['email'] . "</td>";
								echo "<td><a href=\"voldelete.php?volunteerID={$row['volunteerID']}\" onclick=\"return confirm('Are you sure you want to delete this volunteer?')\">Delete</a></td>";
								echo "<td>" . $memberdelete . "</td>";
								echo "</tr>";
							}
							?>
 					</tbody>
	</table>

				<?php
				// user messages
				if (isset ( $_SESSION ['msg'] )) // if session error is set
{
					echo '<div class="msg">';
					echo '<h3 class ="text-danger">' . $_SESSION ['msg'] . '</h3>'; // display error message
					echo '</div>';
					unset ( $_SESSION ['msg'] ); // unset session error
				}
				
				?>
			</div>

