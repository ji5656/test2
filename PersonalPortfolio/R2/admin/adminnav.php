<!-------------------------------------------------------

Subject: IFB299		Group: Group 82
Webpage: adminnav.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi


---------------------------------------------------------
				Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)

Formatting page.

---------------------------------------------------------

Description of the page: Navigation bar for admin only.
--------------------------------------------------------->

<div class="container-fluid">
	<div class="row">
		<div class="col-sm-3 col-md-2 sidebar">
			<div class="user-panel name ">
      
       	<?php
								if ($_SESSION ['admin']) {
									{
										$username = $_SESSION ['admin'];
										$sql = "SELECT * FROM admin  WHERE username= '$username'";
										$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
										$row = mysqli_fetch_array ( $result );
										// check to see if a member or admin is logged in and, if so, display the logged in menu items
											
										echo "<h2>" . $row ['firstname'] . " </h2>";
										
										echo '<h4>ADMIN</h4>';
									}
									?>	
									
		
          
      
      </div>

			<ul class="nav nav-sidebar ">
				<li><a href="membermanage.php" <?php if($page=='managemember'){echo "class='current1'";} ?>>Members</a></li>
				<li><a href="volmanage.php" <?php if($page=='volmember'){echo "class='current1'";} ?>>Volunteer</a></li>
				<li><a href="eventsattendee.php" <?php if($page=='eventanalytic'){echo "class='current1'";} ?>>Events Info</a></li>
			</ul>
		
<?php }?></div>