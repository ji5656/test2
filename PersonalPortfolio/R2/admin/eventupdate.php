<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: eventupdate.php
File Version: 1.0.3 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------
Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)
Version: 1.0.3 (Ji-Young Choi)

Formatting page.

---------------------------------------------------------

Description of the page: Page to update event by committee member /president only.
--------------------------------------------------------->
<?php
session_start ();
?>

<?php
$page = " Update events";
include '../includes/connect.php';
include 'allheader.php';

if ($_SESSION ['type'] == 4) {
	
	include 'prenav.php';
} else {
	
	include 'comnav.php';
}
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">UPDATING EVENT INFO</h1>
				
<?php
$eventID = $_GET ['eventID'];
$sql = "SELECT events.* ,admin.firstname FROM events JOIN admin USING (adminID)  WHERE eventID = '$eventID'";
$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
$row = mysqli_fetch_array ( $result );
?>

		<h1>Update event</h1>
	<div class="row">
		<div class="col-md-6">
			<form action="eventupdateprocessing.php" method="post"
				enctype='multipart/form-data'>

				<div class="form-group">
					<label>Title*</label> <input class="form-control" type="text"
						name="title" required value="<?php echo $row ['title']?>" /><br />
				</div>

				<div class="form-group">
					<label>Content*</label>
					<textarea rows="10" cols="60%" class="form-control" name="content"
						required> <?php echo $row ['content']?></textarea>
					<br />
				</div>

				<div class="form-group">
					<label>venue</label> <input type="text" name="venue"
						class="form-control" required
						value="<?php
						echo $row ['venue']?>" /><br />
				</div>
				<div class="form-group">
					<label>Budget</label> <input class="form-control" type="text"
						name="budget" required value="<?php echo $row ['budget']?>" /><br />
				</div>

				<div class="form-group">
					<label>date</label> <input type="text" name="eventdate"
						class="form-control" required
						value="<?php
						echo $row ['eventdate']?>" /><br />
				</div>

				<div class="form-group">
					<label>Author*</label> <select name='adminID' class="form-control"
						required>
						<option value="<?php echo $row['adminID'] ?>"><?php echo $row ['firstname']?>
					            </option>

            <?php
												$sql = "SELECT * FROM admin where type=3";
												$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) );
												while ( $row = mysqli_fetch_array ( $result ) ) {
													echo "<option value=" . $row ['adminID'] . ">" . $row ['firstname'] . " " . "</option>";
												}
												?>
            </select><br />
				</div>


				<div class="form-group">
					<label>Donation</label>
					<!-- create a drop-down list populated by the admin details stored in the database -->
					<select name='donationID' class="form-control">
						<option value="donationID">Please select</option>
 
 <?php
	$sql = "SELECT * FROM donation ";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	while ( $row = mysqli_fetch_array ( $result ) ) {
		echo "<option value=" . $row ['donationID'] . ">" . $row ['donationTitle'] . "</option>";
	}
	?>
 
 </select><br />
				</div>
		
		</div>

		<div class="col-md-6">

			<h2>Update Image</h2>
          <?php
										$sql = "SELECT events.* FROM events WHERE eventID ='$eventID'";
										$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
										$row = mysqli_fetch_array ( $result );
										
										if ((is_null ( $row ['eventimg'] )) || (empty ( $row ['eventimg'] ))) // if the photo field is NULL or empty
{
											echo "<p><img src='../img/default.png' width=620 height=300 alt='default photo'/></p>"; // display the default photo
										} else {
											echo "<img src='../img/" . ($row ['eventimg']) . "'" . "style='width:150px; height:200px;' background-size='cover'>";
										}
										?> <div class="form-group">
				<label>New Image</label> <input class="form-control" type="file"
					name="image"><br />
				<p>Accepted files are JPG, GIF or PNG. Maximum size is 500kb.</p>
			</div>


			<input type="hidden" name="eventID" value="<?php echo $eventID; ?>">


			<input type="submit" class="form-control" name="Update event"
				value="Update event" />
			</form>
		</div>
	</div>
</div>








