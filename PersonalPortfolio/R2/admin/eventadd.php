<!-------------------------------------------------------

Subject: IFB299		Group: Group 82
Webpage: eventadd.php
File Version: 1.0.1 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi


---------------------------------------------------------
Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)

Formatting page.

---------------------------------------------------------

Description of the page: Page to add event .
--------------------------------------------------------->
<?php
session_start ();
?>
<?php

$page = "eventadd";
include '../includes/connect.php';
include 'allheader.php';

if ($_SESSION ['type'] == 4) {
	
	include 'prenav.php';
} else {
	
	include 'comnav.php';
}
?>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<h1 class="page-header">Add New Event</h1>

	<div class="container">

		<div class="row">
			<div class="col-md-6">
				<form action="eventaddprocessing.php" method="post"
					enctype='multipart/form-data'>

					<div class="form-group">
						<label>Title*</label> <input class="form-control" type="text"
							name="title" required><br />
					</div>

					<div class="form-group">
						<label>Content*</label>
						<textarea rows="10" cols="60%" class="form-control" name="content"
							required></textarea>
						<br />
					</div>

					<div class="form-group">
						<label>venue</label> <input type="text" name="venue"
							class="form-control" /><br />
					</div>

					<div class="form-group">
						<label>date</label> <input type="text" name="eventdate"
							placeholder="yyyy-mm-dd 00:00:00" class="form-control" /><br />
					</div>

					<div class="form-group">
						<label>Budget</label> <input class="form-control" type="text"
							name="budget" placeholder=" $AUD " required><br />
					</div>
					<div class="form-group">
						<label>Admin</label>
						<!-- create a drop-down list populated by the admin details stored in the database -->
						<select name='adminID' class="form-control" required>
							<option value="">Please select</option>
 
 <?php
	$sql = "SELECT * FROM admin where type=3";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	while ( $row = mysqli_fetch_array ( $result ) ) {
		echo "<option value=" . $row ['adminID'] . ">" . $row ['firstname'] . " " . $row ['lastname'] . "</option>";
	}
	?>
 
 </select><br />
					</div>

					<div class="form-group">
						<label>Donation</label>
						<!-- create a drop-down list populated by the admin details stored in the database -->
						<select name='donationID' class="form-control">
							<option value="">Please select</option>
 
 <?php
	$sql = "SELECT * FROM donation ";
	$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
	while ( $row = mysqli_fetch_array ( $result ) ) {
		echo "<option value=" . $row ['donationTitle'] . ">" . $row ['donationTitle'] . "</option>";
	}
	?>
 
 </select><br />
					</div>
					<div class="form-group">

						<label>New Image</label> <input class="form-control" type="file"
							name="image"><br />
						<p>Accepted files are JPG, GIF or PNG. Maximum size is 500kb.</p>
					</div>



					<input type="hidden" name="eventID" value="<?php echo $eventID; ?>">


					<input type="submit" class="form-control" name="Adding new event"
						value="Adding new event" />
			
			</div>
			</form>


		</div>
	</div>
</div>

<?php
if (isset ( $_SESSION ['msg'] )) // if session error is set
{
	echo '<div class="msg">';
	echo '<h3 class ="text-danger">' . $_SESSION ['msg'] . '</h3>'; // display error message
	echo '</div>';
	unset ( $_SESSION ['msg'] ); //unset session error
	}

?>

				
				
				
				