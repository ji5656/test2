<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: emailnotice.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------
Updates

---------------------------------------------------------

Description of the page: Page for admin to sening email to member about event info.
--------------------------------------------------------->
<?php
session_start ();
?>

<?php
$page = "emailnotice";
include '../includes/connect.php';
include 'allheader.php';

 
if($_SESSION ['president'] ){
	
	include 'prenav.php';

}
else{
	
	include 'adminnav.php';
	
}
?>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="row">
		<div class="col-md-12">
			<hr>
			<h2>Send Email Notification</h2>
			<hr>
			<?php
			
				if (isset ( $_SESSION ['msg'] )) // if session error is set
				{
					echo '<script language="javascript">';
					echo 'alert("'. $_SESSION ['msg'].'")';
					echo '</script>';
					unset ( $_SESSION ['msg'] );
					
				}
				
				?>
			<table class="table">
				<thead>
					<tr>
						<th>EvnetID</th>
						<th>Title</th>
						<th>Venue</th>
						<th>Eventdate</th>

					</tr>
				</thead>


				<tbody>
 						<?php
							$sql = "SELECT * FROM events ";
							$result = mysqli_query ( $con, $sql ) or die ( mysqli_error ( $con ) ); // run the query
							$row_count = mysqli_num_rows ( $result );
							echo 'Total event :  ' . $row_count;
							
							while ( $row = mysqli_fetch_array ( $result ) ) {
								
								echo "<tr >";
								echo "<td>" . $row ['eventID'] . "</td>";
								echo "<td>" . $row ['title'] . "</td>";
								echo "<td>" . $row ['venue'] . "</td>";
								echo "<td>" . $row ['eventdate'] . "</td>";
								echo '<td><form action="../pages/emailnoticeprocessing.php" method="post">';
								echo '<input type="hidden" name="eventID" value="' . $row ['eventID'] . '">';
								echo '<input type="submit" class="btn btn-primary" 
    							onclick="return confirm(\'are you sure to send event ' . $row ['title'] . ' information to all member?\')" value ="Sending Email">';
								echo "</tr >";
							}
							?>
				
			
			</table>

				
			</div>
	</div>
</div>

<?php
