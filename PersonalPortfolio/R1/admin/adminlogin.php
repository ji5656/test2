<!-------------------------------------------------------

Subject: IFB299		Group: Group 82
Webpage: adminlogin.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion) 
Author: Ji-Young Choi


---------------------------------------------------------
				Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)

Formatting page.

---------------------------------------------------------

Description of the page: admin login page.
--------------------------------------------------------->

<?php
 $page = "Login";
 include '../includes/connect.php';
 include '../includes/header.php';
 include '../includes/nav.php';
?>

<div class="login">
	<hr><h2>Admin LOGIN</h2><hr>


<?php

 if(isset($_SESSION['error'])){
 	echo '<div class="error">';
 	echo '<p>' . $_SESSION['error'] . '</p>';
 	echo "</div>";
 	unset($_SESSION['error']); 
 }

 if(isset($_SESSION['success'])){
 	echo "<p class = 'success'>" . $_SESSION['success'] . "</p>"; //echo  the message
 	unset($_SESSION['success']); //unset the session
 }
?>

<form  action="../pages/loginprocessing.php" method="post">
	<p>Admin Login</p>
	<div>
		<label for="username">Username : </label>
		<input type="text" name="username" id="username" placeholder="Enter your username" required/>
	</div>
	<div>
		<label for="password">Password  : </label>
		<input type="password" name="password" id="password" placeholder="Enter your password" required />
	</div>

 		<input type="hidden" value="<?php echo $_GET['eventID']; ?>" name="eventID"/>
		<p><input type="submit" name="login" value="Login" /></p>
</form>

<?php
 include '../includes/footer.php';
?>
