<!-------------------------------------------------------

Subject: IFB299 Group: Group 82
Webpage: eventsdelete.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion)
Author: Ji-Young Choi

---------------------------------------------------------
Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)

Formatting page.

---------------------------------------------------------

Description of the page: Functions to delete event.
--------------------------------------------------------->

<?php
	session_start();
	include"../includes/connect.php";
?>

<?php
	$eventID=$_GET['eventID'];
	$sql="DELETE events.* FROM events WHERE eventID = '$eventID'";
	$result=mysqli_query($con,$sql)or die(($con));
	$_SESSION['msg']='event deleted successfully.';
	header('location: commitindex.php');
//register a session witha success message
?>
