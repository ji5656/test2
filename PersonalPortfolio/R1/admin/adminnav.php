<!-------------------------------------------------------

Subject: IFB299		Group: Group 82
Webpage: adminnav.php
File Version: 1.0.2 (Release.ConfirmedVersion.CurrentVersion) 
Author: Ji-Young Choi


---------------------------------------------------------
				Updates
Version: 1.0.1 (Ji-Young Choi)

Intial Issue

Version: 1.0.2 (Se Jun Ahn)

Formatting page.

---------------------------------------------------------

Description of the page: Navigation bar for admin only.
--------------------------------------------------------->

<nav class="navbar navbar-fixed-top">

  	<div class="container-fluid" style="padding-left:0";>
	    <div class="navbar-header">
      		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
        		<span class="icon-bar"></span>
      		</button>

	<div class="collapse navbar-collapse" id="myNavbar">
		<ul class="nav navbar-nav ">
			<li>
				<a <?php if($page=='Home'){echo "class='current joinedcolor '";} ?>
				href="index.php">Dashboard /</a>
			</li>

	 		<li>
 				<a <?php if($page=='memberadmin'){echo "class='current'";} ?>
 				href="membermanage.php">member infomation /</a></li>
		</ul>
	
		<ul class="nav navbar-nav  navbar-right">
  			<li>
	  			<a href="../pages/logout.php">Logout/ </a>
  			</li>
  		
  			<li>
	  			<a href="../pages/index.php">View Site /</a>
  			</li>
  		<?php echo  "<span class='name text'>
			<li>"."Welcome&nbsp&nbsp" . $_SESSION['admin'] ." </li></span>";?>
  		</ul>

</nav>
