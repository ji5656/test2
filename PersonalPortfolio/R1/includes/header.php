
<!DOCTYPE html>
<?php
session_start()?>
<head>
 <meta charset="utf-8">
 <title><?php echo $page ?></title>

 <!-- link to favicon -->
 <link href="../images/favicon.ico" rel="shortcut icon" />
 <!-- link to external CSS -->
<!-- bootstrap-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../css/graph.css">
<link rel="stylesheet" href="../css/main.css">
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
 <!-- enable HTML5 in IE 8 and below -->
 <!--[if IE]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
 <!-- TinyMCE editor -->
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
 tinymce.init({
 selector: "textarea",
 menubar: false,
 plugins: "link"
 });

</script>


</head>
<body>
  <div class="row">
    <div class=" col-md-4">
    <div class="socialgroup">
  <ul>
  <li><a href="https://www.facebook.com/community82/"><i class="fa fa-facebook"></i></a></li>
  <li><a href="https://twitter.com/community82qut"><i class="fa fa-twitter"></i></a></li>
  <li><a href="https://www.linkedin.com/in/community-qut-9b4816125?trk=hp-identity-name"><i class="fa fa-linkedin"></i></a></li>
  <li><a href="https://plus.google.com/107687660841293451632"><i class="fa fa-google-plus"></i> </a></li>
  </ul>

  </div>
</div>
  <div class=" col-md-8">
  <div class="contactinfo" >

    <ul>
  <li><span class="glyphicon glyphicon-phone"></span> 12345 6789  </li>
  <li><span class="glyphicon glyphicon glyphicon-envelope"></span>community@admin.com</li>
<li><a href="login.php" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-user"></span>  Login</a></li>
</ul>
</div>
</div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    	  <div class="modal-dialog">
				<div class="loginmodal-container">
          <hr><h1>LOGIN</h1><hr>
          <h4>member  or volunteer</h4>
           <?php
           if(isset($_SESSION['msg'])) //if session error is set
           {
           echo '<div class="msg">';
           echo '<h3 class ="text-danger">' . $_SESSION['msg'] . '</h3>'; //display error message
           echo '</div>';
           unset($_SESSION['msg']); //unset session error
         ?>
           <script type="text/javascript">


           $(function() {
             $('#myModal').modal('show');

           });
           $('#myModal').on('hidden.bs.modal', function () {
       $(this).find("input,textarea,select").val('').end();

   });


         </script>
         <?php }?>
          <div class="modal-body">
          <form  action="../pages/loginprocessing.php" method="post">

           <div>
           <label for="username">Username : </label>
           <input type="text" name="username" id="username" placeholder="Enter your username" required/>
           </div>
           <div>
           <label for="password">Password  : </label>
           <input type="password" name="password" id="password" placeholder="Enter your password" required />
           </div>

           <input type="hidden" value="" name="reviewID"/>
           <p><input type="submit" name="login" value="Login" /></p></br />
            <h4>Don't have an account yet? Please <a href="../pages /regichoice.php">sign up</a>.</h4>
              <h4><a href="index.php" data-toggle="modal" data-target="#newpwmodal">Fogot password?</a></h4>
           </form>
          </div>
				</div>
			</div>
	</div>

  <div class="modal fade" id="newpwmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      	  <div class="modal-dialog">
  				<div class="loginmodal-container">
            <?php
            if(isset($_SESSION['msg'])) //if session error is set
            {
            echo '<div class="msg">';
            echo '<h3 class ="text-danger">' . $_SESSION['msg'] . '</h3>'; //display error message
            echo '</div>';
            unset($_SESSION['msg']); //unset session error
          ?>
             <script type="text/javascript">

             $(function() {
               $('#newpwmodal').modal('show');

             });

             $('#newpwmodal').on('hidden.bs.modal', function () {
         $(this).find("input,textarea,select").val('').end();

     });
     </script>
   <?php } ?>
<div class="modal-body">
            <form method="post" action="new_password.php">
                <h2>forgot password??</h2>
              <p>Enter username </p>
                <input type="text" name="username">
                  <p>Enter Email Address registered </p>
                <input type="text" name="email">

                <input type="submit" name="submit_email">
            </form>
          </div>
        </div>
</div></div>
