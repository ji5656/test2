-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 16-09-05 15:12
-- 서버 버전: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `community`
--

-- --------------------------------------------------------

--
-- 테이블 구조 `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`adminID` int(10) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `email` varchar(60) NOT NULL,
  `joindate` datetime NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- 테이블의 덤프 데이터 `admin`
--

INSERT INTO `admin` (`adminID`, `username`, `password`, `salt`, `firstname`, `lastname`, `email`, `joindate`, `type`) VALUES
(2, 'adminkt', '6b3a55e0261b0304143f805a24924d0c', '15cec5205c9e34355287acbb4d096a08', 'Kristi ', 'Turman', 'kristi@gmail.com', '2014-01-01 09:00:00', 0),
(3, 'adminst', '10e39d44af853554767db9b1e7396bea8c97681eb56d85d8818addbb7e3b7053', 'abd1018f0e08056ff208df5de039809e', 'Scott', ' Turman', 'turman@telstra.com.au', '2014-01-20 14:30:00', 0),
(4, 'adminrw', '8a35fd6de1e692dfa8277c405f93bba34926176285c85a13634e54e051b576f3', '09d1fe391935b75f798053f866ee5052', 'Richard', ' Weathers', 'richo@gmail.com', '2014-01-20 16:00:00', 0),
(5, 'adminnc', 'd54dc8e24b12ba4805777d6b6eac977094b73962a15e04f227ec40eb6ec56432', '4d069acd30e4b0c6eb5e5f36c01d1482', 'Nicholas', ' Cutter', 'nicholas.cutter@gmail.com', '2014-01-22 13:00:00', 0),
(6, 'admintest', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', '', 'jiyou', 'ad', 'ff', '2016-09-06 00:00:00', 0);

-- --------------------------------------------------------

--
-- 테이블 구조 `category`
--

CREATE TABLE IF NOT EXISTS `category` (
`categoryID` int(11) NOT NULL,
  `category` varchar(40) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 테이블의 덤프 데이터 `category`
--

INSERT INTO `category` (`categoryID`, `category`, `description`) VALUES
(1, 'charity', 'charity'),
(2, 'activity', 'activity');

-- --------------------------------------------------------

--
-- 테이블 구조 `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
`commentID` int(11) NOT NULL,
  `datePosted` datetime NOT NULL,
  `comment` text NOT NULL,
  `eventID` int(11) NOT NULL,
  `memberID` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- 테이블의 덤프 데이터 `comment`
--

INSERT INTO `comment` (`commentID`, `datePosted`, `comment`, `eventID`, `memberID`) VALUES
(1, '2015-03-20 15:52:36', 'good', 1, 1),
(2, '2015-03-20 15:54:28', 'd', 1, 2),
(3, '2015-05-23 20:20:09', '', 1, 1),
(4, '2015-05-23 20:20:22', 'good', 1, 1),
(9, '2015-05-25 02:00:02', 'nice', 1, 1);

-- --------------------------------------------------------

--
-- 테이블 구조 `donation`
--

CREATE TABLE IF NOT EXISTS `donation` (
`donationID` int(11) NOT NULL,
  `donationTitle` varchar(40) NOT NULL,
  `donationDes` longtext NOT NULL,
  `goalMoney` int(11) NOT NULL,
  `img` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- 테이블의 덤프 데이터 `donation`
--

INSERT INTO `donation` (`donationID`, `donationTitle`, `donationDes`, `goalMoney`, `img`) VALUES
(1, 'CHARITY FOR EDUCATION', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', 45800, 'donation1');

-- --------------------------------------------------------

--
-- 테이블 구조 `events`
--

CREATE TABLE IF NOT EXISTS `events` (
`eventID` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `categoryID` int(10) NOT NULL,
  `adminID` int(10) NOT NULL,
  `date` datetime NOT NULL,
  `venue` varchar(40) NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `memberID` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- 테이블의 덤프 데이터 `events`
--

INSERT INTO `events` (`eventID`, `title`, `content`, `categoryID`, `adminID`, `date`, `venue`, `image`, `memberID`) VALUES
(3, 'event1', 'dfdf', 1, 2, '2016-09-30 00:00:00', 'QUT', NULL, 1),
(4, 'event2', 'dfdf', 2, 2, '2016-09-15 00:00:00', 'Botanic garden', NULL, 1),
(5, 'event3', 'dfdf', 1, 2, '2016-09-28 00:00:00', 'anzac squre', NULL, 1);

-- --------------------------------------------------------

--
-- 테이블 구조 `fundedmoney`
--

CREATE TABLE IF NOT EXISTS `fundedmoney` (
  `fundedID` int(11) NOT NULL,
  `donationID` int(11) NOT NULL,
  `memberID` int(11) NOT NULL,
  `donationMoney` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 테이블의 덤프 데이터 `fundedmoney`
--

INSERT INTO `fundedmoney` (`fundedID`, `donationID`, `memberID`, `donationMoney`) VALUES
(0, 1, 1, 1000),
(1, 1, 2, 2000);

-- --------------------------------------------------------

--
-- 테이블 구조 `member`
--

CREATE TABLE IF NOT EXISTS `member` (
`memberID` int(10) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `streetnum` int(11) DEFAULT NULL,
  `streetname` varchar(45) DEFAULT NULL,
  `suburb` varchar(20) DEFAULT NULL,
  `state` enum('ACT','QLD','WA','VIC','TAS','NSW') DEFAULT NULL,
  `postcode` varchar(45) DEFAULT NULL,
  `country` varchar(45) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `gender` enum('Female','Male') NOT NULL,
  `date` datetime NOT NULL,
  `newsletter` enum('YES','NO') DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `contribution` int(11) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 테이블의 덤프 데이터 `member`
--

INSERT INTO `member` (`memberID`, `username`, `password`, `salt`, `firstname`, `lastname`, `streetnum`, `streetname`, `suburb`, `state`, `postcode`, `country`, `phone`, `mobile`, `email`, `gender`, `date`, `newsletter`, `image`, `contribution`, `type`) VALUES
(1, 'natalie', '9d26a3c61c0e2ce5c980ad8c7fff13c08cf898b6f3786930c73b68e3b88ad779', '002640cb549ba08ee90cd79aee89cce7', 'member1adsfdf', 'choi12333', 4, 'stre                  ', 'rohhbb', 'ACT', '1234', 'lll', '123', '12355', 'ji5656@uuu.mmm', 'Female', '2014-01-16 14:00:00', '', 'nataliegoddard.png', NULL, 1),
(2, 'yvette', '19612ae3ed04b7c224ba12db07be5ce0915eed3de351ccbf60508f948b476e5a', '15cec5205c9e34355287acbb4d096a08', 'member1adsfdf', 'choi12333', 4, 'stre                  ', 'rohhbb', 'ACT', '1234', 'lll', '123', '12355', 'ji5656@uuu.mmm', 'Female', '2014-01-16 08:30:00', '', '', NULL, 1),
(3, 'admin', 'fd519f352e32ff3ac100a614f1c87e4bda60f99ccb0da7748c946d634b1270d0', '3919adddff059091db3868ca97d3dc3c', 'member1adsfdf', 'choi12333', 4, 'stre                  ', 'rohhbb', 'ACT', '1234', 'lll', '123', '12355', 'ji5656@uuu.mmm', 'Female', '2016-09-04 01:54:22', '', '', NULL, 1),
(4, 'member1', '4b374124e81c0f96572313a7e57d95d68c7ea2ac38c65c8b962a11db66608e51', '3c75ae7840aaf4da8cdf9e6443905034', 'member1adsfdf', 'choi12333', 4, 'stre                  ', 'rohhbb', 'ACT', '1234', 'lll', '123', '12355', 'ji5656@uuu.mmm', 'Female', '2016-09-05 19:33:22', '', '2693_girl-with-hat-in-sunglass_large.jpg', NULL, 1);

-- --------------------------------------------------------

--
-- 테이블 구조 `news`
--

CREATE TABLE IF NOT EXISTS `news` (
`newsID` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `categoryID` int(10) NOT NULL,
  `adminID` int(10) NOT NULL,
  `date` datetime NOT NULL,
  `image` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 테이블 구조 `type`
--

CREATE TABLE IF NOT EXISTS `type` (
  `typeID` tinyint(1) NOT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 테이블의 덤프 데이터 `type`
--

INSERT INTO `type` (`typeID`, `type`) VALUES
(0, 'admin'),
(1, 'member'),
(2, 'volunteer');

-- --------------------------------------------------------

--
-- 테이블 구조 `volunteer`
--

CREATE TABLE IF NOT EXISTS `volunteer` (
`volunteerID` int(10) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(32) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `streetnum` int(11) DEFAULT NULL,
  `streetname` varchar(45) DEFAULT NULL,
  `suburb` varchar(20) DEFAULT NULL,
  `state` enum('ACT','QLD','WA','VIC','TAS','NSW') DEFAULT NULL,
  `postcode` varchar(45) DEFAULT NULL,
  `country` varchar(45) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `email` varchar(200) NOT NULL,
  `gender` enum('Female','Male') NOT NULL,
  `date` datetime NOT NULL,
  `newsletter` enum('YES','NO') DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- 덤프된 테이블의 인덱스
--

--
-- 테이블의 인덱스 `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`adminID`), ADD UNIQUE KEY `adminID_UNIQUE` (`adminID`), ADD KEY `fk_admin_type1_idx` (`type`);

--
-- 테이블의 인덱스 `category`
--
ALTER TABLE `category`
 ADD PRIMARY KEY (`categoryID`), ADD UNIQUE KEY `categoryID_UNIQUE` (`categoryID`);

--
-- 테이블의 인덱스 `comment`
--
ALTER TABLE `comment`
 ADD PRIMARY KEY (`commentID`), ADD KEY `fk_comment_user1_idx` (`eventID`);

--
-- 테이블의 인덱스 `donation`
--
ALTER TABLE `donation`
 ADD PRIMARY KEY (`donationID`);

--
-- 테이블의 인덱스 `events`
--
ALTER TABLE `events`
 ADD PRIMARY KEY (`eventID`), ADD UNIQUE KEY `reviewID_UNIQUE` (`eventID`,`categoryID`), ADD KEY `fk_review_admin1_idx` (`adminID`), ADD KEY `fk_review_category1_idx` (`categoryID`);

--
-- 테이블의 인덱스 `fundedmoney`
--
ALTER TABLE `fundedmoney`
 ADD PRIMARY KEY (`fundedID`);

--
-- 테이블의 인덱스 `member`
--
ALTER TABLE `member`
 ADD PRIMARY KEY (`memberID`), ADD KEY `fk_member_type1_idx` (`type`);

--
-- 테이블의 인덱스 `news`
--
ALTER TABLE `news`
 ADD PRIMARY KEY (`newsID`), ADD UNIQUE KEY `reviewID_UNIQUE` (`newsID`,`categoryID`), ADD KEY `fk_review_admin1_idx` (`adminID`), ADD KEY `fk_review_category1_idx` (`categoryID`);

--
-- 테이블의 인덱스 `type`
--
ALTER TABLE `type`
 ADD PRIMARY KEY (`typeID`);

--
-- 테이블의 인덱스 `volunteer`
--
ALTER TABLE `volunteer`
 ADD PRIMARY KEY (`volunteerID`), ADD KEY `fk_member_type1_idx` (`type`);

--
-- 덤프된 테이블의 AUTO_INCREMENT
--

--
-- 테이블의 AUTO_INCREMENT `admin`
--
ALTER TABLE `admin`
MODIFY `adminID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- 테이블의 AUTO_INCREMENT `category`
--
ALTER TABLE `category`
MODIFY `categoryID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- 테이블의 AUTO_INCREMENT `comment`
--
ALTER TABLE `comment`
MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- 테이블의 AUTO_INCREMENT `donation`
--
ALTER TABLE `donation`
MODIFY `donationID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- 테이블의 AUTO_INCREMENT `events`
--
ALTER TABLE `events`
MODIFY `eventID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- 테이블의 AUTO_INCREMENT `member`
--
ALTER TABLE `member`
MODIFY `memberID` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- 테이블의 AUTO_INCREMENT `news`
--
ALTER TABLE `news`
MODIFY `newsID` int(11) NOT NULL AUTO_INCREMENT;
--
-- 테이블의 AUTO_INCREMENT `volunteer`
--
ALTER TABLE `volunteer`
MODIFY `volunteerID` int(10) NOT NULL AUTO_INCREMENT;
--
-- 덤프된 테이블의 제약사항
--

--
-- 테이블의 제약사항 `admin`
--
ALTER TABLE `admin`
ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`type`) REFERENCES `type` (`typeID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 테이블의 제약사항 `events`
--
ALTER TABLE `events`
ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`categoryID`) REFERENCES `category` (`categoryID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 테이블의 제약사항 `member`
--
ALTER TABLE `member`
ADD CONSTRAINT `member_ibfk_1` FOREIGN KEY (`type`) REFERENCES `type` (`typeID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 테이블의 제약사항 `news`
--
ALTER TABLE `news`
ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`adminID`) REFERENCES `admin` (`adminID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `news_ibfk_2` FOREIGN KEY (`categoryID`) REFERENCES `category` (`categoryID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 테이블의 제약사항 `volunteer`
--
ALTER TABLE `volunteer`
ADD CONSTRAINT `volunteer_ibfk_1` FOREIGN KEY (`type`) REFERENCES `type` (`typeID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
