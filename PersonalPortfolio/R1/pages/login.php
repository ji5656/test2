<?php
 $page = "Login";

 include '../includes/connect.php';
 include '../includes/header.php';
 include '../includes/nav.php';
?>
<div class="login">

 <hr><h2>LOGIN</h2><hr>


 <?php
 //user messages
 if(isset($_SESSION['error']))
 {

 echo '<div class="error">';
 echo '<p>' . $_SESSION['error'] . '</p>'; //
 echo "</div>";
 unset($_SESSION['error']);
 }

 if(isset($_SESSION['success'])){
 echo "<p class = 'success'>" . $_SESSION['success'] . "</p>";
 unset($_SESSION['success']);
 }
 ?>

<form  action="loginprocessing.php" method="post">
  <p> </p>
 <div>
 <label for="username">Username : </label>
 <input type="text" name="username" id="username" placeholder="Enter your username" required/>
 </div>
 <div>
 <label for="password">Password  : </label>
 <input type="password" name="password" id="password" placeholder="Enter your password" required />
 </div>

 <input type="hidden" value="<?php echo $_GET['reviewID']; ?>" name="reviewID"
/>
 <p><input type="submit" name="login" value="Login" /></p></br />
  <h3>Don't have an account yet? Please <a href="regichoice.php">sign up</a>.</h3>

 </form>



<?php
 include '../includes/footer.php';
?>
