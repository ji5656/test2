<?php
	$page = "Registration";
	include '../includes/connect.php';
	include '../includes/header.php'; //session_start(); included in header.php
  include '../includes/nav.php';
?>
	<div class="login">
	<div id="regform">
		<hr><h2>Become a volunteer</h2><hr>

<?php
	//user messages
	if (isset($_SESSION['error']))
	{
		echo'<div class="error">';
		echo'<p>' . $_SESSION['error'] . '</p>';
		echo'</div>';
		unset($_SESSION['error']);
	}
?>
	<form action="volregiprocessing.php" method="post" enctype="multipart/form-data"> <!-- the multipart/form-data is essential for file upload functionality -->
        <p>Complete the details below to sign up for a new volunteer account.</p>
		<p>Passwords must have a minimum of 8 characters.</p>
     <label>Username*</label> <input type="text" name="username" required /><br/>
	<label>Password*</label> <input type="password" name="password" required pattern=".{8,}" title= "Password must be 8 characters or more" /><br />
	<label>First Name*</label> <input type="text" name="firstname" required /><br />
	<label>Last Name*</label> <input type="text" name="lastname" required /><br/>
	<label>Phone</label> <input type="text" name="phone" /><br />
	<label>Mobile</label> <input type="text" name="mobile" /><br />
	<label>Email*</label> <input type="email" name="email" required /><br />
	<label>Gender*</label>
 <?php
		//generate drop-down list for gender using enum data type and values from database
		$tableName='volunteer';
		$colGender='gender';

		function getEnumGender($tableName, $colGender)
	{
		global $con; //enable database connection in the function
		$sql = "SHOW COLUMNS FROM $tableName WHERE field='$colGender'";//retrieve enum column

		$result = mysqli_query($con, $sql) or die(mysqli_error($con));//run the query

		$row = mysqli_fetch_array($result); //store the results in a variable named $row

		$type = preg_replace('/(^enum\()/i', '', $row['Type']); //regular expression to replace the enum syntax with blank space

		$enumValues = substr($type, 0, -1); //return the enum string

		$enumExplode = explode(',', $enumValues); //split the enum string into individual values
		return $enumExplode; //return all the enum individual values
	}

	$enumValues = getEnumGender('volunteer', 'gender');
		echo '<select name="gender">';

		echo "<option value=''>Please select</option>";

	foreach($enumValues as $value)
	{
		echo '<option value="' . $removeQuotes = str_replace("'", "",$value) . '">' . $removeQuotes = str_replace("'", "", $value) . '</option>';
	}
		echo '</select>';
 ?>
	<br />


		<p><input type="submit" name="registration" value="Create New Account"/></p>
	</form>
</div>
	</div> <!-- end content -->
