<?php
 $page = "Event";
 include '../includes/connect.php';
 include '../includes/header.php'; //session_start(); included in header.php
 include '../includes/nav.php';

?>
 <div class="con">

     <div class="row">
       <div class="col-md-10">
       <hr><h1 float="left" >OUR <strong>EVENTS </h1><hr></div>
         <div class="col-md-2" style='margin-top:50px; '>
           <a href="registration.php" type="button" class="btn btn-default btn-lg pull-right" style='border-radius:100px';> <span class="glyphicon glyphicon-triangle-right"

 aria-hidden="true"></span>JOIN US</strong></a>
</div>
</div>
</div>
<div class="container" id="eventbox">

    <div class="row" >
  <?php
$sql = "SELECT events.* FROM events ORDER BY events.eventdate ASC LIMIT 0,4 ";
$result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query
  while ($row = mysqli_fetch_array($result))

  {
      echo '<div class="col-md-6" style="margin:30px 0px;" >';
      echo '<div class="eventcardtxt text-center">';
      echo '<h3>'. $row['title'].'</h3><hr>';
      echo '<h4>'.date("Y-m-d H:i", strtotime($row['eventdate'])).'</h4>';

      echo '<p>'. $row['venue'].'</p>';
      echo '<a href="eventpost.php?eventID=' .$row['eventID'].'" type="button" class="btn btn-primary-outline event"><i>More details</i></a>';
      echo '</div>';

      echo "<img src='../img/" . ($row['eventimg']). "'" . "style='width:250px; height:250px;' background-size='cover'>";
      echo '</div>';

}
?>



</div>






</div>
