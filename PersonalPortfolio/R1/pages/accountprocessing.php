<?php
 session_start();
 include "../includes/connect.php";
?>
<?php
 $memberID = $_POST['memberID'];

 $firstname = mysqli_real_escape_string($con, $_POST['firstname']); //prevent SQL injection
 $lastname = mysqli_real_escape_string($con, $_POST['lastname']);
 $streetnum = mysqli_real_escape_string($con, $_POST['streetnum']);
$streetname= mysqli_real_escape_string($con, $_POST['streetname']);
 $suburb = mysqli_real_escape_string($con, $_POST['suburb']);
 $state = mysqli_real_escape_string($con, $_POST['state']);
 $postcode = mysqli_real_escape_string($con, $_POST['postcode']);
 $country = mysqli_real_escape_string($con, $_POST['country']);
 $phone = mysqli_real_escape_string($con, $_POST['phone']);
 $mobile = mysqli_real_escape_string($con, $_POST['mobile']);
 $email = mysqli_real_escape_string($con, $_POST['email']);
 $gender = mysqli_real_escape_string($con, $_POST['gender']);
 $newsletter = mysqli_real_escape_string($con, $_POST['newsletter']);


 if ($firstname == "" || $lastname == "" ) //check if all required fields have data
 {
 $_SESSION['error'] = 'error All * fields are required.'; //if an error occurs initialise a session called 'error' with a msg
 header("location:account.php"); //redirect to registration.php
 exit();
 }

 else
 {
 $sql="UPDATE member SET firstname='$firstname', lastname='$lastname',streetnum = '$streetnum',
 streetname ='$streetname ', suburb ='$suburb', state='$state', postcode='$postcode',
 country='$country', phone='$phone', mobile='$mobile', email='$email', gender='$gender',
 newsletter='$newsletter' ";
 $result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query
 $_SESSION['success'] = 'Account updated successfully'; //if registration is successful initialise a session called 'success' with a msg
 header("location:account.php"); //redirect to login.php
 }
?>
