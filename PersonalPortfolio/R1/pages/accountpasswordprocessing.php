<?php
session_start();
include '../includes/connect.php';
?>
<?php
 $memberID=$_POST['memberID'];
 $password = mysqli_real_escape_string($con, $_POST['password']);

 if (strlen($password) < 8)
 {
 $_SESSION['error'] = 'Password must be 8 characters or more.'; //if password is less than 8 characters initialise a session called 'regoerror1' to have a value of the error msg
 header("location:account.php"); //redirect to registration.php
 exit();
 }
 else
 {

 $salt = md5(uniqid(rand(), true)); //create a random salt value

 $password = hash('sha256', $password.$salt); //generate the hashed password with the salt value

 $sql = "UPDATE member SET password='$password', salt='$salt' WHERE
memberID='$memberID'";

 $result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query

 }

 if($result)
 {
 $_SESSION['success'] = 'Updating password successfully'; //register a session with a success message
 header("location:account.php"); //redirect to account.php
 }
 else
 {
 $_SESSION['error'] = 'Error. Fail to update password.'; //register a session with an error message
 header("location:account.php"); //redirect to account.php
 }
?>
