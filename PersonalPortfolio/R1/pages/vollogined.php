<?php
 $page = "volunteeraccount";
 include '../includes/connect.php';
 include '../includes/header.php';
 include '../includes/nav.php';
 include "../includes/logincheckvol.php";
?>
<div class="con">
<?php
 $volunteerID = $_SESSION['user'];


 $sql = "SELECT * FROM volunteer WHERE volunteerID = '$volunteerID'";
 $result = mysqli_query($con, $sql) or die(mysqli_error($con));
 $row = mysqli_fetch_array($result);
?>

<?php

 if(isset($_SESSION['error']))
 {
 echo '<div class="error">';
 echo '<p>' . $_SESSION['error'] . '</p>';
 echo '</div>';
 unset($_SESSION['error']);
 }
 elseif(isset($_SESSION['success']))
 {
 echo '<div class="success">';
 echo '<p>' . $_SESSION['success'] . '</p>';
 echo '</div>';
 unset($_SESSION['success']);
 }

?>
<div class="row">
<div class="col-md-12 " id = "account">

<hr><h2>My Profile</h1><hr>
  <div class="row">
<a href="volaccount.php" class="pull-right">update my details</a></div>
<div class="row">

<div class="col-md-7 ">

<h4>username : <?php echo $row['username'] ?></h2>
<h4><?php echo $row['firstname'];echo $row['lastname'] ?></h4>
<h4><?php echo $row['mobile'].'<br>';
echo $row['email'] ?></h4>

</br>


<div class="row col-md-pull-1"><span class="eventdatebtn3" >Event History</span></div>
<table class="table">
  <thred>
    <th>title</th>
    <th>date</th>
    <th>venue</th>
    <th>position</th>
  </thred>
  <?php
  $sql = "SELECT vol_attendee.*, events.* FROM  vol_attendee join events using (eventID) WHERE volID = '$volunteerID' ORDER BY events.eventdate ASC";


  $result = mysqli_query($con, $sql) or die(mysqli_error($con));
  while ($row = mysqli_fetch_array($result))

  {
    echo '<tr>';
    echo '<td>'. $row['title'].'</td>';
    echo '<td>'.date("Y-m-d", strtotime($row['eventdate'])).'</h5>';
    echo '<td>'. $row['venue'].'</td>';
    echo '<td>'. $row['position'].'</td>';
    echo '</tr>';

  }
  ?>

</table>

  </div>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/highcharts-more.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <div class="col-md-4">
<div id="container" style="min-width: 400px; max-width: 600px; height: 400px; margin: 0 auto">
<script>
$(function () {

    $('#container').highcharts({

        chart: {
            polar: true,
            type: 'line'
        },

        title: {
            text: 'skills',
            x: -80
        },

        pane: {
            size: '80%'
        },

        xAxis: {
            categories: ['communication skill', 'Technical skill', 'Experience', 'Management','Responsibility','other'],
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polargon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>${point.y:,.0f}</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },

        series: [ {
            name: 'skill point',
            data: [40, 90, 40, 40, 60, 50],
            pointPlacement: 'on'
        }]

    });
});
</script>
</div>
</div>
