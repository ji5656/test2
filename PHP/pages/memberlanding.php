<?php
 $page = "My Account";
 include '../includes/connect.php';
 include '../includes/header.php';
 include '../includes/nav.php';
 include "../includes/loginmembercheck.php";
?>
<div class = "container">
<div class="row">

  <h3>
      <hr><h1>Upcoming your event schedule</h1></hr>

    <a href="account.php" class="pull-right">update my details</a></div>

  <div class="col-md-4">

    <div class= "row">
<?php
 $memberID = $_SESSION['user'];
$sql = "SELECT memberattendee.*, events.* FROM memberattendee join events using (eventID) WHERE memberID = '$memberID' ORDER BY events.eventdate ASC";
$result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query
  while ($row = mysqli_fetch_array($result))

  {


echo '<div class="row "style="background-color:#022C37; margin:10px 0;">';
    echo '<div class="eventcardtxt1 pull-left text-center">';
    echo '<h5>'. $row['title'].'</h5><hr>';
    echo '<h5>'.date("Y-m-d H:i", strtotime($row['eventdate'])).'</h5>';

    echo '<p>'. $row['venue'].'</p>';
    echo '<a href="eventpost.php?eventID=' .$row['eventID'].'" type="button" class="btn btn-primary-outline event"><i>More details</i></a>';
    echo '</div>';

    echo "<div class='pull-right'><img src='../img/" . ($row['eventimg']). "'" . "style='width:150px; height:200px;' background-size='cover'>";
    echo '</div></div>';

}
?>
</div>
</div>
<div class="col-md-6 push-right" >
<h3>Your contribution</h3>
<table class="table ">
  <thred>
    <th>donationTitle</th>
    <th>goalMoney</th>
    <th>donation money</th>
    <th>donation date</th>
  </thred>
<?php
$memberID = $_SESSION['user'];
$sql = "SELECT `fundedmoney`.*, `donation`.* FROM `fundedmoney` join `donation` using (`donationID`) WHERE `memberID` ='$memberID'";
$result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query
 while ($row = mysqli_fetch_array($result))

 {
   echo '<tr>';
  	echo	'<td>'. $row['donationTitle'].'</td>';
    echo 	'<td>'.$row['goalMoney'].'</td>';
    echo 	'<td>'.$row['donationMoney'].'</td>';
    echo 	'<td>'.$row['donationdate'].'</td>';
     echo '</tr>';
 }?>
 </table>
 <script>
 $(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'your contribution '
        },

        xAxis: {
            categories: ['donation1', 'donation2', 'donation3', 'donation4'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Au (dollers)',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' millions'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'your contribution',
            data: [100, 500, 0, 0]
        }, {
            name: 'member average',
            data: [120, 100, 250, 100]
        }, {
            name: 'total funding',
            data: [1052, 954, 4250, 740]
        }]
    });
});
</script>
 <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
</div>
<?php
include "../includes/footer.php";
?>
