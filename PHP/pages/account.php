<?php
 $page = "My Account";
 include '../includes/connect.php';
 include '../includes/header.php';
 include '../includes/nav.php';
 include "../includes/loginmembercheck.php";
?>
<div class="login">
<?php
 $memberID = $_SESSION['user'];


 $sql = "SELECT * FROM member WHERE memberID = '$memberID'";
 $result = mysqli_query($con, $sql) or die(mysqli_error($con));
 $row = mysqli_fetch_array($result);
?>

<?php

 if(isset($_SESSION['error']))
 {
 echo '<div class="error">';
 echo '<p>' . $_SESSION['error'] . '</p>';
 echo '</div>';
 unset($_SESSION['error']);
 }
 elseif(isset($_SESSION['success']))
 {
 echo '<div class="success">';
 echo '<p>' . $_SESSION['success'] . '</p>';
 echo '</div>';
 unset($_SESSION['success']);
 }

?>
<div class="row">
  <div class="col-md-12" id = "account">
<hr><h2>My Account</h1><hr>
<p>Update your account details.</p>

<form action="accountprocessing.php" method="post">

<label>Username*</label> <input type="text" name="username" required
value="<?php echo $row['username'] ?>" readonly /><br />
<label>First Name*</label> <input type="text" name="firstname" required
value="<?php echo $row['firstname'] ?>" /><br />
<label>Last Name*</label> <input type="text" name="lastname" required
value="<?php echo $row['lastname'] ?>" /><br />
<label>Street number</label> <input type="text" name="streetnum" value="<?php echo
$row['streetnum'] ?>"/><br />
<label>Street name</label> <input type="text" name="streetname" value="<?php echo
$row['streetname'] ?>"/><br />
<label>Suburb</label> <input type="text" name="suburb" value="<?php echo
$row['suburb'] ?>" /><br />
<label>State</label>
<?php

$tableName='member';
$colState='state';
function getEnumState($tableName, $colState)
{
global $con;
$sql = "SHOW COLUMNS FROM $tableName WHERE field='$colState'";
//retrieve enum column
$result = mysqli_query($con, $sql) or die(mysqli_error($con));
$row = mysqli_fetch_array($result);
$type = preg_replace('/(^enum\()/i', '', $row['Type']); //regular expression to replace the enum syntax with blank space
$enumValues = substr($type, 0, -1);
$enumExplode = explode(',', $enumValues);
return $enumExplode;

}
$enumValues = getEnumState('member', 'state');
echo '<select name="state">';

if((is_null($row['state'])) || (empty($row['state']))) //if the state field is NULL or empty
{
echo "<option value=''>Please select</option>";
}
else
{
echo "<option value=" . $row['state'] . ">" . $row['state'] .
"</option>"; //display the selected enum value
}

foreach($enumValues as $value)
{
echo '<option value="' . $removeQuotes = str_replace("'", "",
$value) . '">' . $removeQuotes = str_replace("'", "", $value) . '</option>'; //remove the quotes from the enum values
}
echo '</select><br />';
?>

<p>&nbsp;</p>
<label>Postcode*</label> <input type="text" name="postcode" required
value="<?php echo $row['postcode'] ?>"/><br />
<label>Country*</label> <input type="text" name="country" required
value="<?php echo $row['country'] ?>"/><br />

<label>Phone</label> <input type="text" name="phone" value="<?php echo
$row['phone'] ?>"/><br />
<label>Mobile</label> <input type="text" name="mobile" value="<?php echo
$row['mobile'] ?>" /><br />
<label>Email*</label> <input type="email" name="email" required
value="<?php echo $row['email'] ?>" /><br />
<label>Gender*</label>
<?php
//generate drop-down list for gender using enum data type and values from database
$tableName='member';
$colGender='gender';
function getEnumGender($tableName, $colGender)
{
global $con; //enable database connection in the function
$sql = "SHOW COLUMNS FROM $tableName WHERE field='$colGender'";
//retrieve enum column
$result = mysqli_query($con, $sql) or die(mysqli_error($con));
//run the query
$row = mysqli_fetch_array($result); //store the results in a variable named $row
$type = preg_replace('/(^enum\()/i', '', $row['Type']); //regular expression to replace the enum syntax with blank space
$enumValues = substr($type, 0, -1); //return the enum string
$enumExplode = explode(',', $enumValues); //split the enum string into individual values
return $enumExplode; //return all the enum individual values
}
$enumValues = getEnumGender('member', 'gender');
echo '<select name="gender">';


echo "<option value=" . $row['gender'] . ">" . $row['gender'] .
"</option>"; //display the selected enum value

foreach($enumValues as $value)
{
echo '<option value="' . $removeQuotes = str_replace("'", "",
$value) . '">' . $removeQuotes = str_replace("'", "", $value) . '</option>';
}
echo '</select>';
?>

<input type="hidden" name="memberID" value="<?php echo $memberID; ?>">
<input type="submit" name="accountupdate" value="Update Account" />
</form>
<h3>Update Image</h3>
<?php
if((is_null($row['image'])) || (empty($row['image']))) //if the photo field is NULL or empty
{
echo "<p><img src='../img/member.png' width=150 height=150
alt='default photo' /></p>";
}
else

{
echo "<p><img src='../img/" . ($row['image']) . "'" . '
width=150 height=150 alt="contact photo"' . "/></p>";
}
?>

<form action="accountimageprocessing.php" method="post"
enctype="multipart/form-data">
<input type="hidden" name="memberID" value="<?php echo $memberID; ?>">
<label>New Image</label> <input type="file" name="image" /><br />
<p>Accepted files are JPG, GIF or PNG. Maximum size is 500kb.</p>
<p><input type="submit" name="imageupdate" value="Update Image" /></p>
</form>



<h3>Update Password</h3>
<p>Passwords must have a minimum of 8 characters.</p>
<form action="accountpasswordprocessing.php" method="post">
<label>New Password*</label> <input type="password" name="password"
pattern=".{8,}" title= "Password must be 8 characters or more" required /><br />
<input type="hidden" name="memberID" value="<?php echo $memberID; ?>">
<p><input type="submit" name="passwordupdate" value="Update Password"
/></p>
</form>

<h3>Delete My Account</h3>
 <p>We're sorry to hear you'd like to delete your account.<br/> By clicking the
button below you will be applied to delete your accout to admin. when your account  delete, confirm mail will be sent</p>
 <form action="accountdelete.php" method="post">
 <p><input type="submit" value="Delete My Account" onclick="return
confirm('Are you sure you wish to permanently delete your account?');" ></p> 

 <input type="hidden" name="memberID" value="<?php echo $memberID; ?>">
 </form>
</div> <!-- end #content -->
</div>
</div>
