<?php
 session_start();
 include "../includes/connect.php";
?>
<?php
 $memberID = $_POST['volunteerID'];

 $firstname = mysqli_real_escape_string($con, $_POST['firstname']); //prevent SQL injection
 $lastname = mysqli_real_escape_string($con, $_POST['lastname']);
 $phone = mysqli_real_escape_string($con, $_POST['phone']);
 $mobile = mysqli_real_escape_string($con, $_POST['mobile']);
 $email = mysqli_real_escape_string($con, $_POST['email']);
 $gender = mysqli_real_escape_string($con, $_POST['gender']);



 if ($firstname == "" || $lastname == "" ) //check if all required fields have data
 {
 $_SESSION['error'] = 'error All * fields are required.'; //if an error occurs initialise a session called 'error' with a msg
 header("location:volaccount.php"); //redirect to registration.php
 exit();
 }

 else
 {
 $sql="UPDATE volunteer SET firstname='$firstname', lastname='$lastname',
 phone='$phone', mobile='$mobile', email='$email', gender='$gender'";
 $result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query
 $_SESSION['success'] = 'Account updated successfully'; //if registration is successful initialise a session called 'success' with a msg
 header("location:volaccount.php"); //redirect to login.php
 }
?>
