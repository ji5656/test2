<?php
 $page = "My Account";
 include '../includes/connect.php';
 include '../includes/header.php';
 include '../includes/nav.php';
 include "../includes/logincheckvol.php";
?>
<div class="login">
<?php
 $volunteerID = $_SESSION['user'];


 $sql = "SELECT * FROM volunteer WHERE volunteerID = '$volunteerID'";
 $result = mysqli_query($con, $sql) or die(mysqli_error($con));
 $row = mysqli_fetch_array($result);
?>

<?php

 if(isset($_SESSION['error']))
 {
 echo '<div class="error">';
 echo '<p>' . $_SESSION['error'] . '</p>';
 echo '</div>';
 unset($_SESSION['error']);
 }
 elseif(isset($_SESSION['success']))
 {
 echo '<div class="success">';
 echo '<p>' . $_SESSION['success'] . '</p>';
 echo '</div>';
 unset($_SESSION['success']);
 }

?>
<div class="row">
  <div class="col-md-12" id = "account">
<hr><h2>My Account</h1><hr>
<p>Update your account details.</p>

<form action="volaccountprocessing.php" method="post">

<label>Username*</label> <input type="text" name="username" required
value="<?php echo $row['username'] ?>" readonly /><br />
<label>First Name*</label> <input type="text" name="firstname" required
value="<?php echo $row['firstname'] ?>" /><br />
<label>Last Name*</label> <input type="text" name="lastname" required
value="<?php echo $row['lastname'] ?>" /><br />




<label>Phone</label> <input type="text" name="phone" value="<?php echo
$row['phone'] ?>"/><br />
<label>Mobile</label> <input type="text" name="mobile" value="<?php echo
$row['mobile'] ?>" /><br />
<label>Email*</label> <input type="email" name="email" required
value="<?php echo $row['email'] ?>" /><br />
<label>Gender*</label>
<?php
//generate drop-down list for gender using enum data type and values from database
$tableName='volunteer';
$colGender='gender';
function getEnumGender($tableName, $colGender)
{
global $con; //enable database connection in the function
$sql = "SHOW COLUMNS FROM $tableName WHERE field='$colGender'";
//retrieve enum column
$result = mysqli_query($con, $sql) or die(mysqli_error($con));
//run the query
$row = mysqli_fetch_array($result); //store the results in a variable named $row
$type = preg_replace('/(^enum\()/i', '', $row['Type']); //regular expression to replace the enum syntax with blank space
$enumValues = substr($type, 0, -1); //return the enum string
$enumExplode = explode(',', $enumValues); //split the enum string into individual values
return $enumExplode; //return all the enum individual values
}
$enumValues = getEnumGender('volunteer', 'gender');
echo '<select name="gender">';


echo "<option value=" . $row['gender'] . ">" . $row['gender'] .
"</option>"; //display the selected enum value

foreach($enumValues as $value)
{
echo '<option value="' . $removeQuotes = str_replace("'", "",
$value) . '">' . $removeQuotes = str_replace("'", "", $value) . '</option>';
}
echo '</select>';
?>

<input type="hidden" name="volunteerID" value="<?php echo $volunteerID; ?>">
<input type="submit" name="accountupdate" value="Update Account" />
</form>

<h3>Update Password</h3>
<p>Passwords must have a minimum of 8 characters.</p>
<form action="volaccountpasswordprocessing.php" method="post">
<label>New Password*</label> <input type="password" name="password"
pattern=".{8,}" title= "Password must be 8 characters or more" required /><br />
<input type="hidden" name="volunteerID" value="<?php echo $volunteerID; ?>">
<p><input type="submit" name="passwordupdate" value="Update Password"
/></p>
</form>
</div>
</div>
