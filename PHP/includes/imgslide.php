<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="../img/ca1.jpg" alt="Image" width="800" height="400">
        <div class="carousel-caption">
          <div class="jumbotron text-left">
            <h1>Don't turn away</h1>
            <p>blablabla</p>
            <div class ="donatebtn"><a class="btn btn-default" href="#" role="button">DONATE TODAY</a></div>
          </div>
        </div>
      </div>

      <div class="item">
        <img src=" ../img/ca2.jpg" alt="Image">
        <div class="carousel-caption">
          <h3>EVENT</h3>
          <p>Lorem ipsum...</p>
          <div class="jumbotron text-left">
            <h1>EVENT</h1>
            <p>blablabla</p>

          </div>
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>


  <!-- Add the extra clearfix for only the required viewport -->
  <div class="clearfix visible-xs-block"></div>
</div>
