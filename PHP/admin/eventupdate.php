<?php

 $page = " Update events";
 include '../includes/connect.php';
 include '../admin/header.php'; //includes a session_start()
 include '../admin/comnav.php';
 include '../includes/logincheckcommittee.php'

?>
<?php
 $eventID = $_GET['eventID'];
 $sql= "SELECT events.* ,admin.firstname FROM events JOIN admin USING (adminID)  WHERE eventID = '$eventID'";

 $result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query
 $row = mysqli_fetch_array($result);
?>
<div class="container">
<h1>Update event</h1>
<div class="row">

  <div class ="col-md-6">



 <form action="eventupdateprocessing.php" method="post">

<div class="form-group">
 <label>Title*</label> <input  class="form-control" type="text" name="title" required value="<?php
echo $row['title'] ?>" /><br />
</div>

<div class="form-group">
 <label>Content*</label>
 <textarea rows="10" cols="60%"   class="form-control" name="content" required > <?php echo
$row['content'] ?></textarea><br />
  </div>

<div class="form-group">
<label>venue</label>
<input type="text" name="venue"  class="form-control" required value="<?php
echo $row['venue'] ?>" /><br />
  </div>

<div class="form-group">
<label>date</label>
<input type="text" name="eventdate"  class="form-control" required value="<?php
echo $row['eventdate'] ?>" /><br />
  </div>

<div class="form-group">
  <label>Author*</label>

<select name='adminID'>
<option value="<?php echo $row['adminID'] ?>"><?php echo $row['firstname']
 ?></option>

<?php
$sql="SELECT * FROM admin where type=3";
$result = mysqli_query($con, $sql) or die(mysqli_error($con));
while ($row = mysqli_fetch_array($result))
{
echo "<option value=" . $row['adminID'] . ">" . $row['firstname'] . " "
. "</option>";
}
?>


</select><br />
</div>
<div class="form-group">
 <input type="hidden" name="eventID" value="<?php echo $eventID; ?>">
  </div>

 <input type="submit"  class="form-control" name="Update event" value="Update event" />

 </form>
</div>

<div class="col-md-6">
  <h2>Update Image</h2>
  <?php
  $sql = "SELECT events.* FROM events WHERE eventID ='$eventID'";
  $result = mysqli_query($con, $sql) or die(mysqli_error($con)); //run the query
  $row = mysqli_fetch_array($result);
  if((is_null($row['eventimg'])) || (empty($row['eventimg']))) //if the photo field is NULL or empty
  {
  echo "<p><img src='../img/default.png' width=620 height=300 alt='default photo'/></p>"; //display the default photo
  }
  else
  {
    echo "<img src='../img/" . ($row['eventimg']). "'" . "style='width:150px; height:200px;' background-size='cover'>";
 }
  ?>

<form action="eventupdateimageprocessing.php" method="post"enctype="multipart/form-data">
 <input type="hidden" name="eventID" value="<?php echo $eventID; ?>">

 <label>New Image</label> <input class="form-control" type="file" name="image" /><br />
 <p>Accepted files are JPG, GIF or PNG. Maximum size is 500kb.</p>
 <input type="submit" name="imageupdate"  class="form-control" value="Update Image" />
 </form>
 </div>
</div>
</div>

<?php
 include '../includes/footer.php';
?>
