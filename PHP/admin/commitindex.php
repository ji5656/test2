<?php
 $page = "committee";
 include '../includes/connect.php';
 include '../admin/header.php';
include '../admin/comnav.php';
?>

<div class="con">
<div class="row">
  <div class="col-md-12">
<hr>
<h2>Event administraion</h2>
<hr>

<table class="table">

<thead>

<tr>
<th>EvnetID</th>
<th>Title</th>
<th>Description</th>
<th>Date</th>
<th>Venue</th>
<th>Committee name</th>
<th>Contact info</th>

<th>modifying</th>
</tr>
</thead>
<tbody>
  <?php
  //user messages
  if(isset($_SESSION['msg'])) //if session error is set
  {
  echo '<div class="msg">';
  echo '<h3 class ="text-danger">' . $_SESSION['msg'] . '</h3>'; //display error message

  echo '</div>';
  unset($_SESSION['msg']); //unset session error
  }
   ?>
 <?php

//$qury = "SELECT eventID,count(memberID) as memberattendee FROM `memberattendee` Group BY `memberattendee`.`eventID` ";
$sql = "SELECT events.*, admin.firstname, admin.email FROM events JOIN admin USING (adminID)  WHERE events.adminID = admin.adminID";
$result = mysqli_query($con, $sql) or die(mysqli_error($con));
$numrow = mysqli_num_rows($result); //retrieve the number of rows
echo "<p>There are currently <strong>" . $numrow . "</strong>
events.</p>";

 while ($row = mysqli_fetch_array($result))
 {
 echo "<tr>";
 echo "<td>" . $row['eventID'] . "</td>";
 echo "<td>" . $row['title'] . "</td>";
 echo "<td>" . (substr(($row['content']),0,50))  . "...</td>";
 echo "<td>" . date("d/m/y H",strtotime($row['eventdate'])) . "</td>";
 echo "<td>" . $row['venue'] . "</td>";
 echo "<td>" . $row['firstname'] . "</td>";
 echo "<td>" . $row['email'] . "</td>";

 echo "<td></td>";
 echo "<td></td>";
 echo "<td><a href=\"eventupdate.php?eventID={$row['eventID']}\">Update</a>  <a
href=\"eventsdelete.php?eventID={$row['eventID']}\" onclick=\"return confirm('Are you
sure you want to delete this event?')\">Delete</a></td>";
 echo "</tr>";
 }
 echo "</table>";

 ?>
</div>
</div>
</div>
<?php
//user messages
if(isset($_SESSION['msg'])) //if session error is set
{
echo '<div class="msg">';
echo '<h3 class ="text-danger">' . $_SESSION['msg'] . '</h3>'; //display error message
echo '</div>';
unset($_SESSION['msg']); //unset session error
}

?>

<?php
 include '../includes/footer.php';
?>
