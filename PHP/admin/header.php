
<!DOCTYPE html>
<?php
session_start();?>
<head>
 <meta charset="utf-8">
 <title><?php echo $page ?></title>

 <!-- link to favicon -->
 <link href="../images/favicon.ico" rel="shortcut icon" />
 <!-- link to external CSS -->
<!-- bootstrap-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../css/graph.css">
<link rel="stylesheet" href="../css/main.css">
<link rel="stylesheet" href="../css/adminmain.css">

 <!-- enable HTML5 in IE 8 and below -->
 <!--[if IE]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

<link href='http://fonts.googleapis.com/css?family=Fredericka+the+Great' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>

 <!-- TinyMCE editor -->
<script type="text/javascript" src="../js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
 tinymce.init({
 selector: "textarea",
 menubar: false,
 plugins: "link"
 });

</script>


</head>
<body>
  <div class="row">
  <div class="col-xs-12 col-sm-6 col-md-8">
    <div class="socialgroup">
  <ul>
  <li><a href="http://facebook.com/"><i class="fa fa-facebook"></i></a></li>
  <li><a href="http://twitter.com/"><i class="fa fa-twitter"></i></a></li>
  <li><a href="http://linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
  <li><a href="http://plus.google.com/"><i class="fa fa-google-plus"></i> </a></li>
  </ul>

  </div>
</div>
  <div class="contactinfo">
  <div class="col-xs-6 col-md-4">
    <ul>
  <li><span class="glyphicon glyphicon-phone"></span> 12345 6789  </li>
  <li><span class="glyphicon glyphicon glyphicon-envelope"></span>community@admin.com</li>
</ul>
</div>
  </div>
</div>
  <div class="logo">Dash Board</div>
